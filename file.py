##
## Acecool's Python Library - File Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.file' );

## Globals
from .__global__ import *;

##
from . import debug;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;

## ##
## ## File Functions
## ##
## ## class FileBase( ):
## ## 	pass;
## class File:
## 	##
## 	##
## 	##
## 	__name__ = 'File';



##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	##


	## _text += Acecool.Header( 'File.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += Acecool.Footer( ) + '\n';


	_text += debug.Header( 'File.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += Acecool.Header( 'File.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += Acecool.Footer( ) + '\n';

	return _text;


##
## Returns the proper path delimiter for the operating system...
##
def GetPathDelimiter( ):
	return System.GetPathDelimiter( );


##
##
##
def GetName( _path = '', _extended = False ):
	_file_base, _ext = os.path.splitext( os.path.basename( _path ) );

	return _file_base + _ext;

	## if ( not _extended ):
	## 	return _path.split( '\\' )[ - 1 ];
	## else:
	## 	return _path;


##
##
##
def SafeName( _name = '' ):
	return File.GetSafeName( _name );


##
## Helper - Converts the full-path to and including the file-name to a single-filename which can be used as a filename saved...
##
def GetSafeName( _name, _replace_dir = '_-_', _replace_blank = '_', _replace_banned = '' ):
	## Convert the filename to a safe-filename - will be using RegEx later..
	## _file_safe = String.Safe( _file ) + '.cache';

	## Same as above except not using RegEx...

	## Replace Directory Specific chars
	_name = _name.replace( '\\',	_replace_dir );
	_name = _name.replace( '/',		_replace_dir );

	## Replace bad chars - anythin
	_name = _name.replace( ' ',		_replace_blank );
	_name = _name.replace( '__',	_replace_blank );

	## Replace Banned ( Any char that can not be used in the name )..
	_name = _name.replace( ':',		_replace_banned );
	_name = _name.replace( '*',		_replace_banned );
	_name = _name.replace( '?',		_replace_banned );
	_name = _name.replace( '|',		_replace_banned );
	_name = _name.replace( '<',		_replace_banned );
	_name = _name.replace( '>',		_replace_banned );

	## Replace Excess
	_name = _name.replace( _replace_dir + _replace_dir,		_replace_dir );
	_name = _name.replace( _replace_blank + _replace_blank,	_replace_blank );

	## Return the finally "safe for saving" filename..
	return _name;


##
##
##
def GetExt( _file = '' ):
	## _data = _file.split( '.' );
	## return _data[ len( _data ) - 1 ] or '';
	_raw_ext, _ext = os.path.splitext( _file );
	if ( _ext[ 0 : 1 ] == '.' ):
		_ext = _ext[ 1 :  ];

	## ## _data = _file.split( '.' );
	## ## return _data[ len( _data ) - 1 ] or '';
	## _raw_ext, _ext = os.path.splitext( _file );
	## if ( _ext[ 0 : 1 ] == '.' ):
	## 	_ext = _ext[ 1 :  ];

	return _ext;
	## ## return String.GetFileExt( _name )


##
## Helper - Returns whether or not the file / folder exists by checking the timestamp...
##
def Exists( _path ):
	return File.GetTimeStamp( _path ) > -1;


##
##
##
def Create( _path, _data = '', _flags = 'w+' ):
	if File.Exists( _path ):
		return False;

	File.Write( _path, _data, _flags );


## ##
## ##
## ##
## def Read( _path, _flags = 'r' ):
## 	## Try to read the file data... This function should only be called if the file exists...
## 	try:
## 		## Open the file with readonly-permissions and proper encoding
## 		with io.open( _path, _flags, encoding = 'utf8' ) as _file:
## 			## Read the file and output it..
## 			return _file.read( );
## 	except:
## 		## Output the error to the output window...
## 		return 'ExceptionThrown: ' + str( sys.exc_info( )[ 0 ] ), -1;


##
##
##
def Read( _file, _char_new_line = CONST_NEW_LINE_UNIX ):
	_lines = None;

	try:
		with codecs.open( _file, "r", encoding = 'utf8' ) as _f:
			_lines = _f.read( ).split( _char_new_line );
	except Exception as _error:
		print( '[ Acecool > file > Read ] Error:', _error );
		_lines = ''.split( _char_new_line );

	return _lines, len( _lines );


##
##
##
def Write( _path, _data = '', _flags = 'w+' ):
	## Try, try, try again...
	try:
		## Open the file with write-permissions and proper encoding
		with io.open( _path, _flags, encoding = 'utf8' ) as _file:
			## Write the data...
			_file.write( _data );

		## Once we're done, return None to ensure no error output is output to the output panel..
		return None
	except NameError as _error:
		## Naming error? Too long maybe? Invalid chars? -- actually because global unicode didn't exist..
		## return 'NameError( {0} ): {1} ' . format( _error.errno, _error.strerror );
		return 'Acecool - Code Mapping System > SaveFileCache > NameError: ' + str( sys.exc_info( )[ 0 ] ) + ' / ' + str( _error );
	except UnicodeError as _error:
		## Data Formatting Issue
		return 'Acecool - Code Mapping System > SaveFileCache > UnicodeError( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) );
	except IOError as _error:
		## General Input / Output Error
		return 'Acecool - Code Mapping System > SaveFileCache > I / O Error( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) );
	except:
		# Handle all other errors
		return 'Acecool - Code Mapping System > SaveFileCache > Unexpected error: ' + str( sys.exc_info( )[ 0 ] );


##
## Adds content to the beginning of a file
##
def Prepend( _path, _data = '', _flags = 'w+' ):
	return File.Write( _path, _data +  File.Read( _path ) );


##
## Adds content to the end of a file
##
def Append( _path, _data = '', _flags = 'a+' ):
	return File.Write( _path,  File.Read( _path ) + _data );


##
## Aliases
##
def Prefix( _path, _data = '', _flags = 'w+' ): return self.Prepend( _path, _data, _flags );
def Suffix( _path, _data = '', _flags = 'a+' ): return self.Append( _path, _data, _flags );


##
##
##
def Copy( _file, _path, _new_path ):
	##
	_file_col = 135;
	_file_old = os.path.join( _path, _file );
	_file_new = os.path.join( _new_path, _file );

	##
	if ( File.Exists( _file_old ) ):
		try:
			shutil.copyfile( _file_old, _file_new );
		except Exception as _error:
			print( 'AcecoolLib.File.Copy -> File: ' + Text.FormatColumn( 25, _file ) + ' -> From: ' + Text.FormatColumn( _file_col - 25, _path ) + ' -> To: ' + Text.FormatColumn( _file_col, _file_new ) + ' -> Error: ' + _error );
	else:
		print( 'AcecoolLib.File.Copy -> File Not Found: ' + Text.FormatColumn( _file_col, _file_old ) + ' -> Trying to Move To: ' + Text.FormatColumn( _file_col - 25, _new_path ) + ' -> As New File: ' + Text.FormatColumn( _file_col, _file_new ) );


##
##
##
def Rename( _path, _file, _new ):
	pass;


##
##
##
def Remove( _path ):
	try:
		os.remove( _path );
	except Exception as _error:
		print( 'AcecoolLib.File.Remove -> Exception on Path: ' + _path + ' -> ' + _error );



##
## Returns the timestamp from the actual file to help determine the last time it was modified...
##
def GetTimeStamp( _file ):
	## Default timestamp set at 0
	_timestamp = 0;

	## Attempt to read the file timestamp...
	try:
		## We try because numerous issues can arise from file I / O
		_timestamp = os.path.getmtime( _file );
	except ( OSError ):
		## File not found, inaccessible or other error - Set the timestamp to -1 so we know an error was thrown..
		_timestamp = -1;

	## Output the data..
	## print( '>> Timestamp for file: ' + _file + ' is ' + str( _timestamp ) );

	## Grab the real-time File TimeStamp if the file exists, otherwise -1 or 0...
	return round( _timestamp, 2 );


##
## Alias
##
def TimeStamp( _file ):
	return File.GetTimeStamp( _file );


##
##
##
def Size( _file ):
	_info = os.stat( _file );
	return _info.st_size;

##
##
##
def CRC( _file ):
	return str( File.GetTimeStamp( _file ) );