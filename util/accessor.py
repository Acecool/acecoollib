##
## Acecool's Python Library - AccessorFunc System - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
## Questions and Answers:
##
##		Q: What is an AccessorFunc?
##
## 		A: An AccessorFunc is essentially a variable functiion created to reduce the amount of code needed to perform a task over and over: ie: Getters and setters
## 			are very common, and 'menial' to rewrite over and over and over when it isn't necessary... Programming is all about optimization, simplifying things and
## 			creating building blocks. An AccessorFunc is a building block used to speed up the development process.
##
##
##		Q: What are the benefits of using an AccessorFunc over writing the code by hand?
##
## 		A: The benefits include, but may not be limited to:
##
## 			+ Less code typed to achieve the same result
##
## 			+ Less time spent recreating the wheel, or typing the same thing over and over or even copying / pasting the same getter / setter / helpers over and
##				over, then renaming them to match the name and possibly forgetting one or two in the hundreds of lines of duplicate code created...
##
## 			+ Less cluttered code making it more human readable
##
## 			+ Smaller file-size
##
## 			+ More time to create unique code
##
##
##
##
##		Q: What are the benefits of using this AccessorFunc System?
##
##		A: This system creates many functions, helpers, and also allows you to built-in data-type and / or value restrictions. It does a lot..
##
##			+ Ultimate control over almost everything within the system
##
## 			+ Create upwards of 10s of functions with just a single line of code.
##
## 			+ You can specify exactly which functions you want, you can easily limit to a getter / setter, or add the helpers such as ToString, and more...
##
## 			+ You can choose the getter-prefix for the getters, or not ( in which case it will default to Get )
##
## 			+ Data-Type limitations can be imposed on an Accessor.. ie: If you create an X / Y / Z positioning system object, with 3 lines you can create hundreds
##				of functions and limit the data-type which can be assigned... ie: for x / y / z, you'd expect them to be numerical, with decimal support. You can
##				assign this with a simple addition to the call. Adding this feature to every getter and / or setter in a hand-written object would mean a lot of
##				extra lines of code you'd have to write, and then repeat for each function - unless you created a helper function which I would hope you would as
##				the purpose of programming is to simplify, and create building blocks to use...
##
## Task: verify this - See if you allow data-type boolean / number, and then set up values for numbers that no other numbers can be set, and booleans can if you don't explicity set true / false as allowed...
## 			+ Value limitations can be imposed on an Accessor.. ie: If you create a dual-purpose variable which could either be boolean or numerical, you can either
##				use the data-type limitation, or this... Or both. So you can setup bool and number data type restrictions, then if there are a set of numbers which
## 				are the only numbers which can be used, then you can set those up...
##
## 			+ You can create simple systems such as getters / setters and helpers to create things, in a single line, such as a currency system... to more advanced
## 				systems based on how you call the AccessorFunc creator.
##
## 			+ It creates Accessor Functions, and also redirects the simple keyed calls through the functions... ie: self.x = 1234.56 is the same as self.SetX( 1234.56 )
##				because the name, X, creates the functions, and the key, x, creates the local vars x, and _x.. also, the internal __x var, should be created / used
## 				when you initialize the Accessor.
##
## 			+
##
## 			+
##
## 			+
##
## 			+
##
## 			+
##
## 			+
##
## 			+
##
##
##
##
##
##
##		Q: What are the negatives of using an AccessorFunc over writing the code by hand?
##
##		A: The negatives include, but may not be limited to:
##
##			- Higher level of overhead required - Instead of simply creating the getter / setter / helpers exactly as you want, you are creating more background
## 				data for the same purpose.
##
##			- Objects which only use Accessors may look overly simple to your employers - things such as simple 2d / 3d coordinate objects, angle storage, vector
##				objects, and others.. Even though there can be some advanced math in some, and the code would've been hundreds of lines without using the system,
##				making something look too easy to an employer usually makes them think they could do it themselves, or that they don't need you... This is why
##				you should give them the line about programming creating building blocks and the final outcome looks simple, but under the hood there is a LOT
##				going on.
##
##			- If you call, and do not use it correctly, you could end up creating more functions than you actually need, or use.
##
##
##
##
##
##		Q: What are the negatives of using this AccessorFunc System?
##
##			- This AccessorFunc system creates a LOT of functions, helpers, object helpers, etc... If you are limited to an obscenely small amount of storage on
##				the device you're coding for, you may run out of room unless you specify exactly which functions you want to use / allow.
##
##			- You have to define 2 classes, instead of 1, in order to have access to the internal accessor. Otherwise it wouldn't be created. I am trying to find
## 				a way around this, but until I have successfully found a way that allows the __<key> to be added, or a way to target the current class, this is it.
## 				ie:
##
##				class ExampleBase( object ):
##					pass
##				class Example( ExampleBase ):
## 					## This creates self.__a, self._a, self.a, and also self.GetA( ), self.SetA( _value ), and more...
##					__a = AccessorFunc( parent = ExampleBase, key = 'a', name = 'A' );
##
## 					## This creates self.__b, self._b, self.b, and also self.B( ), self.SetB( _value ), and more...
##					__a = AccessorFunc( parent = ExampleBase, key = 'a', name = 'A', getter_prefix = '' );
##
## 					## This creates self.__c, self._c, self.c, self._d, self.d and tries to add self.__d ( not sure if it succeeds ), and also self.C( ), self.SetC( _value ), self.E( ), self.SetE( _value ), and more...
## 					## Note: I extra set the extra keys as d, and the extra names as E to show the difference. Names will create the function names, and keys will be the simple key names.. next exaple for exmaple..
## 					##		Also, keys / names allows you to set up ALIASES. In short, thoe functions are created, but they only LINK to the original. No data is stored in the _<alias> variable, it just points to _<_key>...
##					__a = AccessorFunc( parent = ExampleBase, key = 'a', keys = { 'd' }, name = 'A', names = { 'E' }, getter_prefix = '' );
##
## 					## This creates self.__z, self._z, self.z, and also self.GetVertical( ), self.SetVertical( _value ), and more...
## Task: Verify note...
## 					## Note: This lets you do: self.z = 1234.56 or self.SetVertical( 1234.56 ) with them both going to the same, if I recall correctly...
## 					##		it also limits the data-type to numbers only. ie: integers and floats.. You could manually do this by allowed_types = ( float, int, complex, etc... )
##					__z = AccessorFunc( parent = ExampleBase, key = 'z', name = 'Vertical', allowed_types = TYPE_NUMBERS );
##
##
##
##
##
##
##
##
## These are the allowed arguments for the AccessorFunc which uses a vararg system.
##
## 		_parent								= _varargs.get( 'parent',						None );
##		_key								= _varargs.get( 'key',							'Key' );
##		_keys								= _varargs.get( 'keys',							None );
##		_name								= _varargs.get( 'name',							'Name' );
##		_names								= _varargs.get( 'names',						None );
##		_default							= _varargs.get( 'default',						None );
##		_allowed_types						= _varargs.get( 'allowed_types',				TYPE_ANY );
##		_allowed_values						= _varargs.get( 'allowed_values',				VALUE_ANY );
##		_documentation						= _varargs.get( 'documentation',				_name + ' Documentation' );
##		_getter_prefix						= _varargs.get( 'getter_prefix',				'Get' );
##		_allow_erroneous_default			= _varargs.get( 'allow_erroneous_default',		False );
##		_display_created_accessors			= _varargs.get( 'show_accessors',				False );
##		_options							= _varargs.get( 'options',						{ } );
##		_setup								= _varargs.get( 'setup',						_setup_defaults );
##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.util.accessor' );

##

## Globals
from ..__global__ import *;

##
from .. import debug;
from .. import Acecool;
## from .. import dict as Dict;
from .. import table as Table;
from .. import string as String;
from ..example import GetExample;



##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_text = '';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	##
	_text += GetExample( AccessorFuncCoreBase );
	_text += GetExample( AccessorFuncExample );
	_text += GetExample( AccessorFuncException );


	return _text;


##
## AccessorFunc Exception - Used when we want to throw an error / exception in case the developer isn't using the AccessorFunc system correctly...
##
class AccessorFuncExceptionBase( Exception ):
	pass;
class AccessorFuncException( AccessorFuncExceptionBase ):
	##
	##
	##
	__name__ = 'AccessorFuncException';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += debug.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += debug.Footer( ) + '\n';


		_text += debug.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += debug.Footer( ) + '\n';


		## _text += debug.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += debug.Footer( ) + '\n';

		return _text


	##
	##
	##
	def __init__( self, _key = 'Unknown Key', _error = 'General Exception' ):
		##
		print( '[ AccessorFuncException ' + str( _key ) + ' ] ' + _error )

		##
		return super( ).__init__( )






## ##
## ## AccessorFunc Class - Works similar to GMod implementation... Lots of features, easy to add / create new helpers...
## ##
## ## class AccessorFuncBase( ):
## ## 	pass

## class AccessorFunc( AccessorFuncBase ):
## 	##
## 	## AccessorFuncs for the AccessorFunc class - to simplify a few things...
## 	##


## 	##
## 	## Create a new AccessorFunc Object for a class....
## 	##
## 	def __init__( self, _parent ):
## 		## Setup the AccessorFunc class table..
## 		self.data = { }

## 		## Set the parent class - this is where the functions are added while the data remains within this object...
## 		self.parent = _parent

## ACCESSORFUNC_ID_GET								= 'get';
## ACCESSORFUNC_ID_GET_RAW							= 'get_raw';
## ACCESSORFUNC_ID_ISSET							= 'isset';
## ACCESSORFUNC_ID_GET_STR							= 'get_str';
## ACCESSORFUNC_ID_GET_LEN							= 'get_len';
## ACCESSORFUNC_ID_GET_LEN_STR						= 'get_len_str';
## ACCESSORFUNC_ID_GET_DEFAULT_VALUE				= 'get_default_value';
## ACCESSORFUNC_ID_GET_ACCESSOR						= 'get_accessor';
## ACCESSORFUNC_ID_GET_ALLOWED_TYPES				= 'get_allowed_types';
## ACCESSORFUNC_ID_GET_ALLOWED_VALUES				= 'get_allowed_values';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES			= 'get_base_allowed_types';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES			= 'get_base_allowed_values';
## ACCESSORFUNC_ID_HAS_GETTER_PREFIX				= 'has_getter_prefix';
## ACCESSORFUNC_ID_GET_GETTER_PREFIX				= 'get_getter_prefix';
## ACCESSORFUNC_ID_GET_NAME							= 'get_name';
## ACCESSORFUNC_ID_GET_KEY							= 'get_key';
## ACCESSORFUNC_ID_GET_ACCESSOR_KEY					= 'get_accessor_key';
## ACCESSORFUNC_ID_GET_DATA_KEY						= 'get_data_key';
## ACCESSORFUNC_ID_SET								= 'set';
## ACCESSORFUNC_ID_ADD								= 'add';
## ACCESSORFUNC_ID_RESET							= 'reset';
## ACCESSORFUNC_ID_GET_HELPERS						= 'get_helpers';
## ACCESSORFUNC_ID_GET_OUTPUT						= 'get_output';
## ACCESSORFUNC_ID_GET_GETTER_OUTPUT				= 'get_getter_output';


## ACCESSORFUNC_ID_GET								= 'get';
## ACCESSORFUNC_ID_GET_RAW							= 'get_raw';
## ACCESSORFUNC_ID_ISSET							= 'isset';
## ACCESSORFUNC_ID_GET_STR							= 'get_str';
## ACCESSORFUNC_ID_GET_LEN							= 'get_len';
## ACCESSORFUNC_ID_GET_LEN_STR						= 'get_len_str';
## ACCESSORFUNC_ID_GET_DEFAULT_VALUE				= 'get_default_value';
## ACCESSORFUNC_ID_GET_ACCESSOR						= 'get_accessor';
## ACCESSORFUNC_ID_GET_ALLOWED_TYPES				= 'get_allowed_types';
## ACCESSORFUNC_ID_GET_ALLOWED_VALUES				= 'get_allowed_values';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES			= 'get_base_allowed_types';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES			= 'get_base_allowed_values';
## ACCESSORFUNC_ID_HAS_GETTER_PREFIX				= 'has_getter_prefix';
## ACCESSORFUNC_ID_GET_GETTER_PREFIX				= 'get_getter_prefix';
## ACCESSORFUNC_ID_GET_NAME							= 'get_name';
## ACCESSORFUNC_ID_GET_KEY							= 'get_key';
## ACCESSORFUNC_ID_GET_ACCESSOR_KEY					= 'get_accessor_key';
## ACCESSORFUNC_ID_GET_DATA_KEY						= 'get_data_key';
## ACCESSORFUNC_ID_SET								= 'set';
## ACCESSORFUNC_ID_RESET							= 'reset';
## ACCESSORFUNC_ID_GET_HELPERS						= 'get_helpers';
## ACCESSORFUNC_ID_GET_OUTPUT						= 'get_output';
## ACCESSORFUNC_ID_GET_GETTER_OUTPUT				= 'get_getter_output';

##
## AccessorFuncBase: An easy way to add Dynamic Properties - Simply define the __<key> = AccessorFuncBase( TheClassYouAddItTo, '<key>', 'DefaultValue - You may need to create a base-class with only pass in it, and extend your primary class from that then add it in..' )
##
## Usage:
## -------------------------------------------------------------------
## 	class MyClassBase:
## 		pass
## 	class MyClass( MyClassBase ):
## 		# Note: Arg 4 is optional to set Allowed Data-Types and can be None, a type( x ) or a Dict, List or Tuple of type( x )'s which is converted to an O( 1 ) Dict of allowed data-types used to restrict the setter..
## 		# Note: Arg 5 is optional to set Allowed Stored Values and can be None, a single value or a Dict, List or Tuple of values which is converted to an O( 1 ) Dict of allowed values used to restrict the setter..
## 		# Note: I am working on adding min / max restrictions for number-based getters / setters, and callbacks used to alter the data and help in many ways...
## 		__Height = AccessorFuncBase( MyClassBase, 'Height', 0 )
## 		__Width = AccessorFuncBase( MyClassBase, 'Width', 1, ( type( 0 ), type( 0.0 ) ) )
## 		__Depth = AccessorFuncBase( MyClassBase, 'Depth', 2, None, ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ) )
##
## 		def __str__( self ):
## 			return 'Height: ' + str( self.Height ) + '\t\tWidth: ' + str( self.Width ) + '\t\tDepth: ' + str( self.Depth )
##
## 		## Or
##
## 		def __str__( self ):
## 			_text = 'Height: ' + str( self.Height )
## 			_text += '\t\tWidth: ' + str( self.Width )
## 			_text += '\t\tDepth: ' + str( self.Depth )
## 			return _text
##
##
## 	_class = MyClass( )
## 	print( _class )
## 	_class.Height = 10
## 	_class.Width = 20
## 	_class.Depth = 30
## 	print( _class )
##	_class.Depth = 9
##	print( _class )
## -------------------------------------------------------------------
## Output:
## Height: 0		Width: 1		Depth: 2
## Height: 10		Width: 20		Depth: 2
## Height: 10		Width: 20		Depth: 9
##
##
## AccessorFuncBase FlowChart
##	-> __init__ is called with key information letting us know which functions we want to assign, whether we use the defaults or custom replacements, and more...
##	-> All of the functions are defined in local memory in preparation for the registry phase - I may be able to improve this efficiency / overhead to reduce it to the amount necessary and no more... It'd mean slightly more logic, but constant, but I'd still prefer an alternative..
##	-> Each local function is passed through to the Registry System with an identifier such as get, set, etc.. If the setup table ahsn't been changed, all of them are allowed... If the setup_override is setup, then only the ones which have been altered or exist in that table will hav eany bearing on what is to come.. Both tables can be used - ie Setup to prevent all, then setup
## _override to set, or change, but this isn't necessary... If you want to remove all, simply use setup.. If you simply want to fchange 1 or more, use setup_override.
##
##
##
##
## Task: Add an IsUsing*ToString style TernaryFunc return for Boolean Type information to return Enabled / Disabled, On / Off, True / False, etc... helper when requested...
## def IsUsingCacheSystemToString( self ):
## 	return ( 'Disabled', 'Enabled' )[ self.GetSetting( 'use_caching_system', False ) ]
##
## Task: Add SetMin, SetMax helpers for number data-types ( Float is set at 0.0 and 1.0 by default )
## Task: Convert current system of tons of functions, etc.. for function names, function call examples, and so on into the setup table so they can all be referencced using a simple ENUMerator...
## Task: Setup several defaults - AcecoolFuncBase as ->.CreateGetter( 'Name', 'key', _func_override, _on_callback, _post_callback ), ->.SetupFloat( min / max at 0.0, 1.0, etc.. ), ->.SetupString( ), ->.SetupUltraLight( Get, Set, Add, and a few others - but not much more )
## Task: Setup Instance Table containing a list of all accessors, and link the helper table to that list and link it into the Setup Tables to provide EXACT results of what is enabled...
## Task: Setup File Output / wiki.txt or wiki.md style file generation which lists all functions created instead of output to text - or in wiki/filename.md ( so AcecoolCodeMappingSystem.py, Acecool.py, etc.. )..
## Task:
##
##
##
##
##
##
class AccessorFuncCoreBase:

	## The key to target in parent object
	__key				= None;
	__name				= None;
	__prefix_get		= None;

	## Default Data, if provided
	__default			= None;

	## Allowed types / values
	__allowed_types		= None;
	__allowed_values	= None;

	##
	__documentation		= '[ this.{key} / this._{key} / this.__{key} Documentation ] {documentation} - Default Value: {default_value} - Allowed Data-Types: {allowed_types} - Allowed Values: {allowed_values}';

	## Advanced Setup - By default, all helpers are defined... However, if Setup is defined, ALL must be set to True, or only the ones set to True will be created, none others...]
	__setup_defaults = {
		## All Instance IDs are renamed to this, for now... Do not replace this function...
		'instance':		True,

		## ## All Instance Accessor IDs
		## 'instance_reset_accessors_by_name':			True,
		## 'on_instance_reset_accessors_by_name':		False,
		## 'pre_instance_reset_accessors_by_name':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_set_accessors_by_name':			True,
		## 'on_instance_set_accessors_by_name':		False,
		## 'pre_instance_set_accessors_by_name':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_get_accessors_by_name':			True,
		## 'on_instance_get_accessors_by_name':		False,
		## 'pre_instance_get_accessors_by_name':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_reset_accessors_by_key':			True,
		## 'on_instance_reset_accessors_by_key':		False,
		## 'pre_instance_reset_accessors_by_key':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_set_accessors_by_key':			True,
		## 'on_instance_set_accessors_by_key':			False,
		## 'pre_instance_set_accessors_by_key':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_get_accessors_by_key':			True,
		## 'on_instance_get_accessors_by_key':			False,
		## 'pre_instance_get_accessors_by_key':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_reset_grouped_accessors':			True,
		## 'on_instance_reset_grouped_accessors':		False,
		## 'pre_instance_reset_grouped_accessors':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_set_grouped_accessors':			True,
		## 'on_instance_set_grouped_accessors':		False,
		## 'pre_instance_set_grouped_accessors':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_add_grouped_accessors':			True,
		## 'on_instance_add_grouped_accessors':		False,
		## 'pre_instance_add_grouped_accessors':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_get_grouped_accessors':			True,
		## 'on_instance_get_grouped_accessors':		False,
		## 'pre_instance_get_grouped_accessors':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_get_accessors_database':			True,
		## 'on_instance_get_accessors_database':		False,
		## 'pre_instance_get_accessors_database':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_add_accessors_database':			True,
		## 'on_instance_add_accessors_database':		False,
		## 'pre_instance_add_accessors_database':		False,

		## ##  - Calls ( this, ) - Returns
		## 'instance_reset_accessors_database':		True,
		## 'on_instance_reset_accessors_database':		False,
		## 'pre_instance_reset_accessors_database':	False,


		##
		## All of the others Accessor IDs...
		##

		## Get - Calls Get( this, ) - Returns
		'get':										True,

		## Get - Calls Get( this, ) - Returns
		'get_raw':									True,

		## Get - Calls Get( this, ) - Returns
		'isset':									True,

		## Get - Calls Get( this, ) - Returns
		'get_str':									True,

		## Get - Calls Get( this, ) - Returns
		'get_len':									True,

		## Get - Calls Get( this, ) - Returns
		'get_len_str':								True,

		## Get - Calls Get( this, ) - Returns
		'get_default_value':						True,

		## Get - Calls Get( this, ) - Returns
		'get_property':								True,

		## Get - Calls Get( this, ) - Returns
		'get_accessor':								True,

		## Get - Calls Get( this, ) - Returns
		'get_allowed_types':						True,

		## Get - Calls Get( this, ) - Returns
		'get_allowed_values':						True,

		## Get - Calls Get( this, ) - Returns
		'get_base_allowed_types':					True,

		## Get - Calls Get( this, ) - Returns
		'get_base_allowed_values':					True,

		## Get - Calls Get( this, ) - Returns
		'has_getter_prefix':						True,

		## Get - Calls Get( this, ) - Returns
		'get_getter_prefix':						True,

		## Get - Calls Get( this, ) - Returns
		'get_name':									True,

		## Get - Calls Get( this, ) - Returns
		'get_key':									True,

		## Get - Calls Get( this, ) - Returns
		'get_accessor_key':							True,

		## Get - Calls Get( this, ) - Returns
		'get_data_key':								True,

		## Set - Calls Get( this, ) - Returns
		'set':										True,

		## Add - Calls Get( this, ) - Returns
		'add':										True,

		## Pop - Calls Pop( this, ) - Returns
		'pop':										False,

		## Get - Calls Get( this, ) - Returns
		'reset':									True,

		## Get - Calls Get( this, ) - Returns
		'get_helpers':								True,

		## Get - Calls Get( this, ) - Returns
		'get_output':								True,

		## Get - Calls Get( this, ) - Returns
		'get_getter_output':						True,


		##
		## All of the others Accessor IDs...
		##


		##
		## New System for handling function additions, when they are added, and more...
		##
		## ID					Included?		Description
		## name:								This is the function name - A simple string with formatted key replacers are added.. All of the keys are passed through and only the ones the string provides will be replaced. This name shouldn't include this., arguments, etc.. Just the raw name.
		## name_call:			X				This is the callable function name - It will have a new key {name} which is the previous value after being formatted. So it'd only need: 'this/self.{name}( args );' as the string. and this / self can be set as a key too. I may simply just set up an args key which will include the arguments instead of this, as the full function callable name can be built - and I'd simply assign a new tag to determine if it is an instance function ( which is added to the class which adds the property / accessor which uses this. as an identifier in debugging output ) or a property / an accessor function ( which is a function residing in this property / accessor object, identified as self. in debugging output )
		## args:								This is the arguments string - ie for pop: '_index = len - 1, _count = 1', for get: '_default_override = None, _no_default_return = False', etc...
		## type:				X				This is the type of function - ie if it is an instance function ( which generates this. in the name ) or a property function ( which uses self. ) - Note: I may not use this as all instance functions use the instance_ prefix and in the registry, as they are all added or not, they are identified as 'instance'
		## active:								This sets the default enabled status of a particular function
		## active_on:							This sets a condition when the default enabled status can be overriden automatically ( without the dev adding it to setup_override, or setup in the initialization phase )
		## callback_on:							This sets a callback function to be executed during a function - for get, this would be called with the currently assigned value and the potential new value along with whether or not that new value passes allowed-types / values checks. The return would be whether or not to override, and a new value if override. It can either be the current value, unchanged, or a new value ( this value will be subjected to the same allowed-types / values checks )
		## callback_pre:						This sets a callback function to be executed prior to a function being called.. similar to _on, this receives the same information however the only choices are to allow or prevent the function from continuing.. Maybe more later..
		## callback_post:						This sets a callback function to be executed after a function has been called.. similar to _on, this receives the old value, and the new value - but the new value is what has already been assigned.
		##
		##

		## Default Table values - Only add or change what needs changing...
		ACCESSORFUNC_ID_SETUP_DEFAULTS: {
			'name_call_property':				'{self}.{name}( {args} );',
			'name_call_instance':				'{this}.{name}( {args} );',
			'name':								'{get}{PropertyName}',
			'key':								'{PropertyKey}',
			## 'name_call':						'{name_call_instance}',
			'args':								'_default_override = None, _hide_default_return = False',
			## 'type':								ACCESSORFUNC_TYPE_INSTANCE,
			'type':								ACCESSORFUNC_TYPE_PROPERTY,
			'active':							True,
			'active_on':						None,
			'callback_on':						None,
			'callback_pre':						None,
			'callback_post':					None,

		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_RAW: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_ISSET: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_STR: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_LEN: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_LEN_STR: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_DEFAULT_VALUE: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_ACCESSOR: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_ALLOWED_TYPES: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_ALLOWED_VALUES: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_HAS_GETTER_PREFIX: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_GETTER_PREFIX: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_NAME: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_KEY: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_ACCESSOR_KEY: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_DATA_KEY: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_SET: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_ADD: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_POP: {
			'active':							False,
			'active_on':						( lambda self: self.IsListProperty( ) ),
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_RESET: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_HELPERS: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_OUTPUT: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},

		## Get - Calls Get( this, ) - Returns
		ACCESSORFUNC_ID_GET_GETTER_OUTPUT: {
			'active':							True,
			'callback_on':						False,
			'callback_pre':						False,
			'callback_post':					False,
		},


		##
		## All of the Accessor IDs which can be called after the main function is called... For Setters, it can be used to make something else happen. For getters, it can trigger events, or change the returns - But this, because it happens after, will have the result provided to the callback.....
		##

		##  OnGet - Calls OnGet( this, _current, _raw ) - Return anything other than None, if authorized type or value, will return that value - otherwise the normal function will run its return ... )
		'on_get':									False,
		'on_get_fail':								False,
		'on_get_fail_msg':							'OnGetFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_raw':								False,
		'on_get_raw_fail':							False,
		'on_get_raw_fail_msg':						'OnGetRawFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_isset':									False,
		'on_isset_fail':							False,
		'on_isset_fail_msg':						'OnIssetFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_str':								False,
		'on_get_str_fail':							False,
		'on_get_str_fail_msg':						'OnGetStrFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_len':								False,
		'on_get_len_fail':							False,
		'on_get_len_fail_msg':						'OnGetLenFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_len_str':							False,
		'on_get_len_str_fail':						False,
		'on_get_len_str_fail_msg':					'OnGetLenStrFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_default_value':						False,
		'on_get_default_value_fail':				False,
		'on_get_default_value_fail_msg':			'OnGetDefaultValueFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_accessor':							False,
		'on_get_accessor_fail':						False,
		'on_get_accessor_fail_msg':					'OnGetAccessorFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_allowed_types':						False,
		'on_get_allowed_types_fail':				False,
		'on_get_allowed_types_fail_msg':			'OnGetAllowedTypesFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_allowed_values':					False,
		'on_get_allowed_values_fail':				False,
		'on_get_allowed_values_fail_msg':			'OnGetAllowedValuesFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_base_allowed_types':				False,
		'on_get_base_allowed_types_fail':			False,
		'on_get_base_allowed_types_fail_msg':		'OnGetBaseAllowedTypesFail:Default Message',

		## On - Calls On( this, ) - Return
		'on_get_base_allowed_values':				False,
		'on_get_base_allowed_values_fail':			False,
		'on_get_base_allowed_values_fail_msg':		'OnGetBaseAllowedValuesFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_has_getter_prefix':						False,
		'on_has_getter_prefix_fail':				False,
		'on_has_getter_prefix_fail_msg':			'OnHasGetterPrefixFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_getter_prefix':						False,
		'on_get_getter_prefix_fail':				False,
		'on_get_getter_prefix_fail_msg':			'OnGetGetterPrefixFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_name':								False,
		'on_get_name_fail':							False,
		'on_get_name_fail_msg':						'OnGetNameFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_key':								False,
		'on_get_key_fail':							False,
		'on_get_key_fail_msg':						'OnGetNameFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_accessor_key':						False,
		'on_get_accessor_key_fail':					False,
		'on_get_accessor_key_fail_msg':				'OnGetAccessorKeyFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_data_key':							False,
		'on_get_data_key_fail':						False,
		'on_get_data_key_fail_msg':					'OnGetDataKeyFail: Default Message',

		## OnSet - Calls OnSet( this, _current, _new, _is_authorized ) - Used to trigger a call-chained from the Setter, if you return any other value than None, that value will be set if authorized..
		'on_set':									False,
		'on_set_fail':								False,
		'on_set_fail_msg':							'OnSetFail: Default Message',

		## OnAdd - Calls OnAdd( this, _current, _new, _is_authorized ) - Used to trigger a call-chained from the Setter, if you return any other value than None, that value will be set if authorized..
		'on_add':									False,
		'on_add_fail':								False,
		'on_add_fail_msg':							'OnAddFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_reset':									False,
		'on_reset_fail':							False,
		'on_reset_fail_msg':						'OnResetFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_helpers':							False,
		'on_get_helpers_fail':						False,
		'on_get_helpers_fail_msg':					'OnGetHelpersFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_output':							False,
		'on_get_output_fail':						False,
		'on_get_output_fail_msg':					'OnGetOutputFail: Default Message',

		## On - Calls On( this, ) - Return
		'on_get_getter_output':						False,
		'on_get_getter_output_fail':				False,
		'on_get_getter_output_fail_msg':			'OnGetGetterOutputFail: Default Message',


		##
		## All of the Accessor IDs which can be called BEFORE the main function is called... For Setters, it can be used to prevent a value from being set, or alter it.... For Getters, it can return a different value or influence wht is returned.. - This will not have the result provided to the callback - the default value, etc.. will be.. but the current value may not be... or the current may be for set,, and the value we intend to set, etc... but for getters the grabbing the raw data may be more work..
		##

		## Pre-Get will call PreGet( this ) - If I add the raw or current value, then it is called after the function call... Returning anything other than Nne will return that value ( if it is authorized by restricted types / values.. if not, None is used )..
		'pre_get':									False,
		'pre_get_fail':								False,
		'pre_get_fail_msg':							'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_raw':								False,
		'pre_get_raw_fail':							False,
		'pre_get_raw_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_isset':								False,
		'pre_isset_fail':							False,
		'pre_isset_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_str':								False,
		'pre_get_str_fail':							False,
		'pre_get_str_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_len':								False,
		'pre_get_len_fail':							False,
		'pre_get_len_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_len_str':							False,
		'pre_get_len_str_fail':						False,
		'pre_get_len_str_fail_msg':					'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_default_value':					False,
		'pre_get_default_value_fail':				False,
		'pre_get_default_value_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_accessor':							False,
		'pre_get_accessor_fail':					False,
		'pre_get_accessor_fail_msg':				'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_allowed_types':					False,
		'pre_get_allowed_types_fail':				False,
		'pre_get_allowed_types_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_allowed_values':					False,
		'pre_get_allowed_values_fail':				False,
		'pre_get_allowed_values_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_base_allowed_types':				False,
		'pre_get_base_allowed_types_fail':			False,
		'pre_get_base_allowed_types_fail_msg':		'OnFail: Default Message',



		## Pre - Calls Pre( this, ) - Return
		'pre_get_base_allowed_values':				False,
		'pre_get_base_allowed_values_fail':			False,
		'pre_get_base_allowed_values_fail_msg':		'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_has_getter_prefix':					False,
		'pre_has_getter_prefix_fail':				False,
		'pre_has_getter_prefix_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_getter_prefix':					False,
		'pre_get_getter_prefix_fail':				False,
		'pre_get_getter_prefix_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_name':								False,
		'pre_get_name_fail':						False,
		'pre_get_name_fail_msg':					'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_key':								False,
		'pre_get_key_fail':							False,
		'pre_get_key_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_accessor_key':						False,
		'pre_get_accessor_key_fail':				False,
		'pre_get_accessor_key_fail_msg':			'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_data_key':							False,
		'pre_get_data_key_fail':					False,
		'pre_get_data_key_fail_msg':				'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_set':									False,
		'pre_set_fail':								False,
		'pre_set_fail_msg':							'OnSetFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_add':									False,
		'pre_add_fail':								False,
		'pre_add_fail_msg':							'OnAddFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_reset':								False,
		'pre_reset_fail':							False,
		'pre_reset_fail_msg':						'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_helpers':							False,
		'pre_get_helpers_fail':						False,
		'pre_get_helpers_fail_msg':					'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_output':							False,
		'pre_get_output_fail':						False,
		'pre_get_output_fail_msg':					'OnFail: Default Message',


		## Pre - Calls Pre( this, ) - Return
		'pre_get_getter_output':					False,
		'pre_get_getter_output_fail':				False,
		'pre_get_getter_output_fail_msg':			'OnFail: Default Message',

	}


	##
	## Returns the data from this object in a nice ouput
	##
	def __str__( self ):
		##
		_text = '[ AccessorFuncBase ] Key: ' + str( self.__key ) + ' :::: Class ID: ' + str( id( self.__class__ ) ) + ' :::: self ID: ' + str( id( self ) );
		_text += '\t\tDefault: ' + str( self.DefaultValue( ) );
		_text += '\t\tAllowed Types: ' + str( self.GetAllowedTypes( ) );
		_text += '\t\tAllowed Values: ' + str( self.GetAllowedValues( ) );
		return _text;


	##
	## AccessorFunc Node...
	##
	@classmethod
	def AddProperty( _class, _parent, _name, _default = None, _allowed_types = None, _allowed_values = None, _documentation = '', _prefix_get = 'Get', _key = None, _allow_erroneous_default = False, _options = { } ):
		return _class(
			parent						= _parent,
			name						= _name,
			default						= _default,
			allowed_types				= _allowed_types,
			allowed_values				= _allowed_values,
			documentation				= _documentation,
			getter_prefix				= _prefix_get,
			key							= _key,
			allow_erroneous_default		= _allow_erroneous_default,
			options						= _options
		);


	##
	## AccessorFunc Node...
	##
	@classmethod
	def AddGetter( _class, _parent, _key, _name, _func ):
		return _class(
			parent						= _parent,
			name						= _name,
			documentation				= 'Getter: ' + _name,
			key							= _key,
			setup						= { 'get': _func }
		);


	##
	## Uses keyed variable arguments instead of forced a, c, b arguments...
	##
	## Task: Create Get / Set Min / Max Helper Accessors for Data-Types restricted to Numbers..
	## Task: Create 0 - 1 Fraction Restrictions Automatic if Type Restricted to TYPE_FLOAT_MOD or something... ie, set min / max value restrictions to 0, 1, allowed types FLOAT..
	## Task: Create other Data-Type Specific Helpers to make data-type and value restrictions easier to setup or manage...
	## Task: Create Setup ids as enumerated values
	## Task: Update setup to include more than 1 value... ie it will contain fail message, on setup callback, pre on set callback, the function name, the function argument helper output, and more... When I override setup from the init in the future it will work like stup override... so the base table is all, if I cchange something and only want that, the default info is read from the table and stuff I change  or add si used otherwise default... so override lets me change one of the list or many.. setup lets me start almost from scratch but re-use elements from the ones I want...
	##
	## Note: The following Callbacks have been implemented...
	## 	on_set, on_set_fail
	##
	##
	def __init__( self, **_varargs ):
		## Defaults..
		_setup_defaults						= self.__setup_defaults;

		## Helpers...
		## Task: Integrate keys and names system - remove singular name / key system... Set it up so that all keys are implemented, with 'key' or the first in keys being the primary, and all others link to that reference... For names, the same thing, generate the functions..
		_parent								= _varargs.get( 'parent',						None );
		_key								= _varargs.get( 'key',							'Key' );
		_keys								= _varargs.get( 'keys',							None );
		_name								= _varargs.get( 'name',							'Name' );
		_names								= _varargs.get( 'names',						None );
		_default							= _varargs.get( 'default',						None );
		_allowed_types						= _varargs.get( 'allowed_types',				TYPE_ANY );
		_allowed_values						= _varargs.get( 'allowed_values',				VALUE_ANY );
		_documentation						= _varargs.get( 'documentation',				_name + ' documentation missing - Set up default documentation system to replace this message soon!' );
		_getter_prefix						= _varargs.get( 'getter_prefix',				'Get' );
		_allow_erroneous_default			= _varargs.get( 'allow_erroneous_default',		False );
		_display_created_accessors			= _varargs.get( 'show_accessors',				False );
		_options							= _varargs.get( 'options',						{ } );
		_setup								= _varargs.get( 'setup',						_setup_defaults );

		## ## It this Accessor able to use super( ).__init__( ... ) to update the values?
		## _keys								= _varargs.get( 'keys',							None );

		## Setup Override works differently to Setup - If the user defines Setup and only adds 1 function to True, then only that func is fcreated...
		## For override, if one is set to True or False, then that key in Setup is altered to the new value so it lets you make changes to the default list...
		_setup_override						= _varargs.get( 'setup_override',				{ } );


		##
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] Creating: ' + _name );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] Using Key: ' + _key );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] With Key Aliases: ' + str( _keys ) );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] With Name Aliases: ' + str( _names ) );

		## If we have anything to alter, we do it...
		if ( len( _setup_override ) > 0 ):
			for _k, _v in _setup_override.items( ):
				## print( '[ Acecool Library -> AccessorFuncBase -> Init ] SetupOverride Is altering _setup( ' + _key + ': ' + str( _setup.get( _key, False ) ) + ' ) to: ' + str( _value ) );
				_setup[ _k ] = _v;

		## setup = { get = True }

		## ##
		## _text								= '\n' + String.FormatColumnEx( '-', 130, ' ' )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'Parent: ',						100, str( _parent ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'name: ',							100, str( _name ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'default: ',						100, str( _default ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allowed_types: ',					100, str( _allowed_types ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allowed_values: ',				100, str( _allowed_values ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'documentation: ',					100, str( _documentation ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'getter_prefix: ',					100, str( _getter_prefix ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'key: ',							100, str( _key ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allow_erroneous_default: ',		100, str( _allow_erroneous_default ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'options: ',						100, str( _options ) )
		## _text								+= '\n' + String.FormatColumnEx( '-', 130, ' ' )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )

		## ##
		## print( 'AccessorFuncBase.AddProperty: ' + _text )

		##
		self.__class_name			= _parent.__name__;

		## Setup name / Key system... Name is for functions, key is for data storage / property ie self.x, self.y, self.z instead of self.X, self.Y, self.Z which is annoying to write... but self.Height works...
		self.__name					= _name;
		self.__names				= _names;
		self.__key					= ( _key, _name )[ _key == None ];
		self.__keys					= ( _keys, _names )[ _keys == None ];
		self.__prefix_get			= _getter_prefix;
		self.__setup				= _setup;

		##
		self.__display_accessors	= _display_created_accessors;

		## These values will never change for this property - they are static...
		self.__default				= _default;

		## Setup the allowed types and values table...
		self.__base_allowed_types	= _allowed_types;
		self.__allowed_types		= Table.SetupQuickLookupTable( _allowed_types );
		self.__base_allowed_values	= _allowed_values;
		self.__allowed_values		= Table.SetupQuickLookupTable( _allowed_values );


		##
		## self.SetKeyAliases( _keys );
		## self.SetNameAliases( _names );

		##
		_allowed_types_desc, _allowed_values_desc = self.GetAllowedDesc( );
		self.__documentation		= self.__documentation.format( key = self.Key( ), documentation = _documentation, default_value = _default, allowed_types = _allowed_types_desc, allowed_values = _allowed_values_desc );

		## Setup the property using this class functions for the Getter, Setter, and Deleter functions
		## _property = property( lambda _parent: self.Get( _parent ), lambda _parent, _value: self.Set( _parent, _value ), lambda _parent: self.Del( _parent ), self.__documentation )
		## _property = property( lambda  this: self.Get( this ), lambda this, _value: self.Set( this, _value ), lambda this: self.Del( ), self.__documentation )
		## _property = property( lambda self: self.Get( _key ), lambda self, _value: self.Set( _key, _value ), lambda self: self.Del( _key ), self.__documentation )

		## Setup the property using this class functions for the Getter, Setter, and Deleter functions
		_property					= property( self.Get, self.Set, self.Del, self.__documentation );

		## We have to re-set the _parent.__<key> for the parent class for some reason... We do this so we can call self.__x.Get( ), etc... Without it, you see this: "AttributeError: 'MyClass' object has no attribute '__x'"
		setattr( _parent, self.AccessorKey( ), self );

		## We are storing per class unique value assigned in _parent._<key>
		setattr( _parent, self.DataKey( ), None );

		## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
		setattr( _parent, self.Key( ), _property );


		## For each key alias, we simply set up the self.<key_alias> = property... - the data will only ever be stored in the default key location, not at all otherwise everything would need a loop to process all keys and I'd prefer not to do that... the aliases let you easily set up extra ways to get the data.. For instance, an Angle class uses p / y / r for pitch / yaw / roll... So if I set up p / y / r as the defaults and allow pitch / yaw / roll, when I use self.p = 123 or self.pitch = 123, they both store at self._p ... and if I retrieve data, they both return from self._p...
		if ( _keys != None and IsTable( _keys ) ):
			## Set up a raw property to allow 'cloning' so that the default will behave normally, but for added aliases, this can make _<key_alias> behave the same as _<key> where the raw value is directly controlled... I'd have control of it all, so this just needs to point to _<key> - then I can also make one for __<key_alias>, maybe.. - Note: Set and SetRaw would be identical - they're both meant to set a value to the raw data storage area with proper callback support leaving SetRaw unnecessary... - Note: DelRaw may have merit if we decide to populate Raw to remove the Accessor completely and DelRaw to simply nullify the stored raw value to None..
			## _property_raw				= property( self.GetRaw, self.SetRaw, self.DelRaw, self.__documentation );
			_property_raw				= property( self.GetRaw, self.Set, self.DelRaw, self.__documentation );

			## For each alias, set up the raw / alias property for _<alias> so it is redirected to _<key>, also set up the standard property for the <alias> - Determine how to set up __<alias> without having the private system kick in...
			for _k in _keys:
				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				## setattr( _parent, '__' + _k, _property );
				## setattr( _parent, '__' + _k, lambda: getattr( _parent, '__' + _k ) );
				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				## setattr( _parent, '_' + _k, lambda: getattr( _parent, '_' + _k ) );
				## setattr( _parent, '_' + _k, getattr( _parent, self.GetDataKey( ) ) );

				## We are setting the _<key_alias> to a raw property which redirects actions to _<key> while keeping natural integrity..
				setattr( _parent, self.AccessorKey( _k ), _property_raw );

				## We are setting the _<key_alias> to a raw property which redirects actions to _<key> while keeping natural integrity..
				setattr( _parent, self.DataKey( _k ), _property_raw );

				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				setattr( _parent, self.Key( _k ), _property );

				##
				## print( 'AccessorFuncBase: Creating Extra Key References... ' + _k + '\t\tNote: This does not mean data is stored in more than 1 location... Data will only be stored in the default data key location or "self._<key>", but this system allows you a new way to access that data and assign it.. IE for Angle class, pitch / yaw / roll.. If I set the default key as p / y / and r and add aliases for pitch / yaw / roll then using self.p = 123 is the same as self.pitch = 123 - the data is stored at self._p... The data can also be retrieved with self._pitch via the Get Raw Proeprty which redirects the raw data alias to the raw data key... I may add _pitch as another property reference, but as it would not work the same as other _<key>s, it seems counter intuitive to implement....' );


		##
		if ( not getattr( _parent, 'accessor_func_db', None ) ):
			##
			## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
			##
			setattr( _parent, 'accessor_func_db', {
				## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
				## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
				'groups': {
					## Default group - this is for EVERYTHING...
					'*': { }

					## 4 28 44 85
					## 'All': {
					##		'Pitch': True,
					##		'Yaw': True,
					##		'Roll': True,
					## },
				},

				## a list of keys which also link to the accessor func, property, names?
				'keys': {
					## 'roll': { },
					## 'r': { },
				},

				## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
				'names': {
					## 'Roll': { },
					## 'R': { },
				},

				## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
				'call_names': {
					## 'GetRollAccessor': '',
					## 'GetRollAllowedTypes': '',
					## 'GetRoll': ''
				},

				## Accessors list is a simple list of all accessor objects
				## 'accessors': [

				## ]

				##
				## '': [],
				## '': [],
				## '': []
			}
		);

		## Setup all of the Accessor Functions...
		self.SetupAccessorFuncs( _parent );

		## If this is the first AccessorFunc being added, we need to add hidden / internal data / keys for certain bits of data.... Along with Helper functions... ResetKeys( .. )
		self.SetupInstanceHelpers( _parent );



		##
		## print( str( self ) )

		## Verify the default value if Allowed Types or Values is set...
		if ( not _allow_erroneous_default and _default != None and not callable( _default ) and ( _allowed_types != None or _allowed_values != None ) ):
			##
			if ( not self.IsValidData( _default ) ):
				##
				self.__default = None;

				##
				raise AccessorFuncException( _key, '***** Default Value does not meet Allowed Data-Types and / or Allowed Value Specifications and the Override / Ignore Option was not activated... *****' );



	##
	##
	##
	def GetAccessorDB( self, _parent ):
		return getattr( _parent, '_accessorfunc_database', None );

	##
	##
	## Task: Setup words and enumerators for each of these special words - also determine ways to implement these into the properties to simplify everything...
	##
	'''
		__init__
		__name__

		__len__
		__str__

		__getattr__
		__setattr__
		__delattr__
		__getattribute__


		__eq__
		__ne__
		__lt__
		__gt__
		__le__
		__ge__


		__abs__


		__add__
		__sub__
		__mul__
		__floordiv__
		__div__
		__truediv__
		__mod__
		__pow__
		__divmod__

		__iadd__
		__isub__
		__imul__
		__ifloordiv__
		__idiv__
		__itruediv__
		__imod__
		__ipow__

		__radd__
		__rsub__
		__rmul__
		__rfloordiv__
		__rdiv__
		__rtruediv__
		__rmod__
		__rdivmod__
		__rpow__

		__lshift__
		__rshift__
		__and__
		__or__
		__xor__

		__ilshift__
		__irshift__
		__iand__
		__ior__
		__ixor__

		__rlshift__
		__rrshift__
		__rand__
		__ror__
		__rxor__

	'''


	'''
		__enter__( self )															Defines what the context manager should do at the beginning of the block created by the with statement. Note that the return value of __enter__ is bound to the target of the with statement, or the name after the as.
		__exit__( self, _exception_type, _exception_value, _traceback )				Defines what the context manager should do after its block has been executed (or terminates). It can be used to handle exceptions, perform cleanup, or do something always done immediately after the action in the block. If the block executes successfully, exception_type, exception_value, and traceback will be None. Otherwise, you can choose to handle the exception or let the user handle it; if you want to handle it, make sure __exit__ returns True after all is said and done. If you don't want the exception to be handled by the context manager, just let it happen.
		__call__( self, *_varargs )													Allows an instance of a class to be called as a function. Essentially, this means that x() is the same as x.__call__(). Note that __call__ takes a variable number of arguments; this means that you define __call__ as you would any other function, taking however many arguments you'd like it to.
		__instancecheck__( self, _instance )										Checks if an instance is an instance of the class you defined (e.g. isinstance(instance, class).
		__subclasscheck__( self, _subclass )										Checks if a class subclasses the class you defined (e.g. issubclass(subclass, class)).
		__getitem__(self, key)														Defines behavior for when an item is accessed, using the notation self[key]. This is also part of both the mutable and immutable container protocols. It should also raise appropriate exceptions: TypeError if the type of the key is wrong and KeyError if there is no corresponding value for the key.
		__setitem__(self, key, value)												Defines behavior for when an item is assigned to, using the notation self[nkey] = value. This is part of the mutable container protocol. Again, you should raise KeyError and TypeError where appropriate.
		__delitem__(self, key)														Defines behavior for when an item is deleted (e.g. del self[key]). This is only part of the mutable container protocol. You must raise the appropriate exceptions when an invalid key is used.
		__iter__(self)																Should return an iterator for the container. Iterators are returned in a number of contexts, most notably by the iter() built in function and when a container is looped over using the form for x in container:. Iterators are their own objects, and they also must define an __iter__ method that returns self.
		__reversed__(self)															Called to implement behavior for the reversed() built in function. Should return a reversed version of the sequence. Implement this only if the sequence class is ordered, like list or tuple.
		__contains__(self, item)													__contains__ defines behavior for membership tests using in and not in. Why isn't this part of a sequence protocol, you ask? Because when __contains__ isn't defined, Python just iterates over the sequence and returns True if it comes across the item it's looking for.
		__missing__(self, key)														__missing__ is used in subclasses of dict. It defines behavior for whenever a key is accessed that does not exist in a dictionary (so, for instance, if I had a dictionary d and said d["george"] when "george" is not a key in the dict, d.__missing__("george") would be called).

		__repr__(self)																Defines behavior for when repr() is called on an instance of your class. The major difference between str() and repr() is intended audience. repr() is intended to produce output that is mostly machine-readable (in many cases, it could be valid Python code even), whereas str() is intended to be human-readable.
		__unicode__(self)															Defines behavior for when unicode() is called on an instance of your class. unicode() is like str(), but it returns a unicode string. Be wary: if a client calls str() on an instance of your class and you've only defined __unicode__(), it won't work. You should always try to define __str__() as well in case someone doesn't have the luxury of using unicode.
		__format__(self, formatstr)													Defines behavior for when an instance of your class is used in new-style string formatting. For instance, "Hello, {0:abc}!".format(a) would lead to the call a.__format__("abc"). This can be useful for defining your own numerical or string types that you might like to give special formatting options.
		__hash__(self)																Defines behavior for when hash() is called on an instance of your class. It has to return an integer, and its result is used for quick key comparison in dictionaries. Note that this usually entails implementing __eq__ as well. Live by the following rule: a == b implies hash(a) == hash(b).
		__nonzero__(self)															Defines behavior for when bool() is called on an instance of your class. Should return True or False, depending on whether you would want to consider the instance to be True or False.
		__dir__(self)																Defines behavior for when dir() is called on an instance of your class. This method should return a list of attributes for the user. Typically, implementing __dir__ is unnecessary, but it can be vitally important for interactive use of your classes if you redefine __getattr__ or __getattribute__ (which you will see in the next section) or are otherwise dynamically generating attributes.
		__sizeof__(self)															Defines behavior for when sys.getsizeof() is called on an instance of your class. This should return the size of your object, in bytes. This is generally more useful for Python classes implemented in C extensions, but it helps to be aware of it.
		__int__(self)																Implements type conversion to int.
		__long__(self)																Implements type conversion to long.
		__float__(self)																Implements type conversion to float.
		__complex__(self)															Implements type conversion to complex.
		__oct__(self)																Implements type conversion to octal.
		__hex__(self)																Implements type conversion to hexadecimal.
		__index__(self)																Implements type conversion to an int when the object is used in a slice expression. If you define a custom numeric type that might be used in slicing, you should define __index__.
		__trunc__(self)																Called when math.trunc(self) is called. __trunc__ should return the value of `self truncated to an integral type (usually a long).
		__coerce__(self, other)														Method to implement mixed mode arithmetic. __coerce__ should return None if type conversion is impossible. Otherwise, it should return a pair (2-tuple) of self and other, manipulated to have the same type.


		__pos__(self)																Implements behavior for unary positive (e.g. +some_object)
		__neg__(self)																Implements behavior for negation (e.g. -some_object)
		__invert__(self)															Implements behavior for inversion using the ~ operator. For an explanation on what this does, see the Wikipedia article on bitwise operations.
		__round__(self, n)															Implements behavior for the built in round() function. n is the number of decimal places to round to.
		__floor__(self)																Implements behavior for math.floor(), i.e., rounding down to the nearest integer.
		__ceil__(self)																Implements behavior for math.ceil(), i.e., rounding up to the nearest integer.
		__trunc__(self)																Implements behavior for math.trunc(), i.e., truncating to an integral.

		__cmp__(self, other)														__cmp__ is the most basic of the comparison magic methods. It actually implements behavior for all of the comparison operators (<, ==, !=, etc.), but it might not do it the way you want (for example, if whether one instance was equal to another were determined by one criterion and and whether an instance is greater than another were determined by something else). __cmp__ should return a negative integer if self < other, zero if self == other, and positive if self > other. It's usually best to define each comparison you need rather than define them all at once, but __cmp__ can be a good way to save repetition and improve clarity when you need all comparisons implemented with similar criteria.

		__new__(cls, [...)															__new__ is the first method to get called in an object's instantiation. It takes the class, then any other arguments that it will pass along to __init__. __new__ is used fairly rarely, but it does have its purposes, particularly when subclassing an immutable type like a tuple or a string. I don't want to go in to too much detail on __new__ because it's not too useful, but it is covered in great detail in the Python docs.
		__del__(self)																If __new__ and __init__ formed the constructor of the object, __del__ is the destructor. It doesn't implement behavior for the statement del x (so that code would not translate to x.__del__()). Rather, it defines behavior for when an object is garbage collected. It can be quite useful for objects that might require extra cleanup upon deletion, like sockets or file objects. Be careful, however, as there is no guarantee that __del__ will be executed if the object is still alive when the interpreter exits, so __del__ can't serve as a replacement for good coding practices (like always closing a connection when you're done with it. In fact, __del__ should almost never be used because of the precarious circumstances under which it is called; use it with caution!
	'''


	'''
		__init__(self, [...)														The initializer for the class. It gets passed whatever the primary constructor was called with (so, for example, if we called x = SomeClass(10, 'foo'), __init__ would get passed 10 and 'foo' as arguments. __init__ is almost universally used in Python class definitions.

		__len__(self)																Returns the length of the container. Part of the protocol for both immutable and mutable containers.
		__str__(self)																Defines behavior for when str() is called on an instance of your class.

		__getattr__(self, name)														You can define behavior for when a user attempts to access an attribute that doesn't exist (either at all or yet). This can be useful for catching and redirecting common misspellings, giving warnings about using deprecated attributes (you can still choose to compute and return that attribute, if you wish), or deftly handing an AttributeError. It only gets called when a nonexistent attribute is accessed, however, so it isn't a true encapsulation solution.
		__setattr__(self, name, value)												Unlike __getattr__, __setattr__ is an encapsulation solution. It allows you to define behavior for assignment to an attribute regardless of whether or not that attribute exists, meaning you can define custom rules for any changes in the values of attributes. However, you have to be careful with how you use __setattr__, as the example at the end of the list will show.
		__delattr__(self, name)														This is the exact same as __setattr__, but for deleting attributes instead of setting them. The same precautions need to be taken as with __setattr__ as well in order to prevent infinite recursion (calling del self.name in the implementation of __delattr__ would cause infinite recursion).
		__getattribute__(self, name)												After all this, __getattribute__ fits in pretty well with its companions __setattr__ and __delattr__. However, I don't recommend you use it. __getattribute__ can only be used with new-style classes (all classes are new-style in the newest versions of Python, and in older versions you can make a class new-style by subclassing object. It allows you to define rules for whenever an attribute's value is accessed. It suffers from some similar infinite recursion problems as its partners-in-crime (this time you call the base class's __getattribute__ method to prevent this). It also mainly obviates the need for __getattr__, which, when __getattribute__ is implemented, only gets called if it is called explicitly or an AttributeError is raised. This method can be used (after all, it's your choice), but I don't recommend it because it has a small use case (it's far more rare that we need special behavior to retrieve a value than to assign to it) and because it can be really difficult to implement bug-free.


		__eq__(self, other)															Defines behavior for the equality operator, ==.
		__ne__(self, other)															Defines behavior for the inequality operator, !=.
		__lt__(self, other)															Defines behavior for the less-than operator, <.
		__gt__(self, other)															Defines behavior for the greater-than operator, >.
		__le__(self, other)															Defines behavior for the less-than-or-equal-to operator, <=.
		__ge__(self, other)															Defines behavior for the greater-than-or-equal-to operator, >=.

		__abs__(self)																Implements behavior for the built in abs() function.

		__add__(self, other)														Implements addition.
		__sub__(self, other)														Implements subtraction.
		__mul__(self, other)														Implements multiplication.
		__floordiv__(self, other)													Implements integer division using the // operator.
		__div__(self, other)														Implements division using the / operator.
		__truediv__(self, other)													Implements true division. Note that this only works when from __future__ import division is in effect.
		__mod__(self, other)														Implements modulo using the % operator.
		__divmod__(self, other)														Implements behavior for long division using the divmod() built in function.
		__pow__																		Implements behavior for exponents using the ** operator.

		__iadd__(self, other)														Implements addition with assignment.
		__isub__(self, other)														Implements subtraction with assignment.
		__imul__(self, other)														Implements multiplication with assignment.
		__ifloordiv__(self, other)													Implements integer division with assignment using the //= operator.
		__idiv__(self, other)														Implements division with assignment using the /= operator.
		__itruediv__(self, other)													Implements true division with assignment. Note that this only works when from __future__ import division is in effect.
		__imod__(self, other)														Implements modulo with assignment using the %= operator.
		__ipow__																	Implements behavior for exponents with assignment using the **= operator.

		__radd__(self, other)														Implements reflected addition.
		__rsub__(self, other)														Implements reflected subtraction.
		__rmul__(self, other)														Implements reflected multiplication.
		__rfloordiv__(self, other)													Implements reflected integer division using the // operator.
		__rdiv__(self, other)														Implements reflected division using the / operator.
		__rtruediv__(self, other)													Implements reflected true division. Note that this only works when from __future__ import division is in effect.
		__rmod__(self, other)														Implements reflected modulo using the % operator.
		__rdivmod__(self, other)													Implements behavior for long division using the divmod() built in function, when divmod(other, self) is called.
		__rpow__																	Implements behavior for reflected exponents using the ** operator.

		__lshift__(self, other)														Implements left bitwise shift using the << operator.
		__rshift__(self, other)														Implements right bitwise shift using the >> operator.
		__and__(self, other)														Implements bitwise and using the & operator.
		__or__(self, other)															Implements bitwise or using the | operator.
		__xor__(self, other )														Implements bitwise xor using the ^ operator.

		__ilshift__(self, other)													Implements left bitwise shift with assignment using the <<= operator.
		__irshift__(self, other)													Implements right bitwise shift with assignment using the >>= operator.
		__iand__(self, other)														Implements bitwise and with assignment using the &= operator.
		__ior__(self, other)														Implements bitwise or with assignment using the |= operator.
		__ixor__(self, other)														Implements bitwise xor with assignment using the ^= operator.

		__rlshift__(self, other)													Implements reflected left bitwise shift using the << operator.
		__rrshift__(self, other)													Implements reflected right bitwise shift using the >> operator.
		__rand__(self, other)														Implements reflected bitwise and using the & operator.
		__ror__(self, other)														Implements reflected bitwise or using the | operator.
		__rxor__(self, other)														Implements reflected bitwise xor using the ^ operator.
	'''


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += debug.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += debug.Footer( ) + '\n';


		_text += debug.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += debug.Footer( ) + '\n';


		## _text += debug.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += debug.Footer( ) + '\n';

		return _text;



	##
	## This creates, if not exists, helper functions for the class using AccessorFunc systems such as ResetAccessorValues, etc...
	##
	def SetupInstanceHelpers( self, _parent = None ):
		## If these already exist, then don't set them up again...
		if ( _parent == None ):
			return False;


		##
		if ( getattr( _parent, 'accessorfunc_setup', None ) ):
			##
			_key			= self.Key( );
			_name			= self.Name( );

			##
			## print( '[ AccessorFuncBase ] Added Name DB:		' + _name );
			## print( '[ AccessorFuncBase ] Added Key DB:		' + _key );


			return True;
		else:
			setattr( _parent, 'accessorfunc_setup', True );

		##
		if ( self.ShowCreatedAccessors( ) ):
			## print( '[ AccessorFuncBase ]\t##' );
			## print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ] Please note: several helper functions have been added to the parent class, ' + str( _parent ) + ', for the first-time setup of AccessorFunc Support for this class...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## These Helpers are meant to simplify common actions and improve efficiency of writing code...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## Note: Default restrictions mean Width and Depth will not be set as they only allow 0 to 9 values..' );
			print( '[ AccessorFuncBase ]\t## Note: Resetting a key / name sets the stored value to None - The getter will still return the default value unless overwritten or default returns are disabled...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( );
			print( );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## This will let you Set 1 to many Accessor Name Values at a time - Note: This is identical to calling _class.SetName( _value ) or _class.Name = _value -- Data-Type / Value Setting Restrictions will still function...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '\t\tHelper:\t\t\t_class.SetAccessors( _name, _value [ , ... ] )' );
			print( );
			print( "\t\tUsage:\t\t\t_foo.SetAccessors( 'Name', 'Foo Fighters', 'X', 100, 'Y', 200, 'Z', 300, 'Blah', 'Blahing all the way', 'Height', 400, 'Width', 500, 'Depth', 600 )" );
			print( "\t\tIdentical To:\n\t\t\t\t\t\t_foo.Name = 'Foo Fighters'\n\t\t\t\t\t\t_foo.x = 100\n\t\t\t\t\t\t_foo.y = 200\n\t\t\t\t\t\t_foo.SetZ( 300 )\n\t\t\t\t\t\t_foo.SetBlah( 'Blahing all the way' )\n\t\t\t\t\t\t_foo.Height = 400\n\t\t\t\t\t\t_foo.SetWidth( 500 )\n\t\t\t\t\t\t_foo.SetDepth( 600 )" );
			print( );
			print( );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## This will let you Reset 1 to many Accessor Names to default value ( None ) - Note: Even if you reset the stored value to None, there is a Default Value system which will still function unless you explicitly disable it on the Getter call...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '\t\tHelper:\t\t\t_class.ResetAccessors( _name [ , ... ] )' );
			print( );
			print( "\t\tUsage:\t\t\t_foo.ResetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )" );
			print( "\t\tIdentical To:\n\t\t\t\t\t\t_foo.Name = None\n\t\t\t\t\t\t_foo.x = None\n\t\t\t\t\t\t_foo.y = None\n\t\t\t\t\t\t_foo.SetZ( None )\n\t\t\t\t\t\t_foo.ResetBlah( )\n\t\t\t\t\t\t_foo.Height = None\n\t\t\t\t\t\t_foo.SetWidth( None )\n\t\t\t\t\t\t_foo.ResetDepth( )" );
			print( );
			print( );
			print( );
			print( '##' );
			print( '## Note: This is the first one added... A complete list of the functions created is shown below for the first - for the rest, a list of names / keys is provided instead of outputting the same information over and over..' );
			print( '##' );
			print( '[ AccessorFuncBase ] Added Name DB:		' + self.Name( ) );
			print( '[ AccessorFuncBase ] Added Key DB:		' + self.Key( ) );
			print( );
			## print( getattr( _parent, self.AccessorNameGetHelpers( ) )( _parent ) );
			print( self.AccessorCallGetHelpers( _parent, True ) );
			print( );


		## ##
		## ## Accessor Func DB...
		## ##
		## ## _accessor_func_db = {
		## ## 	'names': { },
		## ## 	'keys': { },

		## ## };
		## ## setattr( _parent, '_accessor_func_db', _accessor_func_db );
		## ##
		## if ( not getattr( _parent, 'x_accessor_func_db', None ) ):
		## 	##
		## 	## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
		## 	##
		## 	setattr( _parent, 'x_accessor_func_db', {
		## 		## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
		## 		## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
		## 		'groups': {
		## 			## Default group - this is for EVERYTHING...
		## 			'*': [ ]

		## 			## 4 28 44 85
		## 			## 'All': {
		## 			##		'Pitch': True,
		## 			##		'Yaw': True,
		## 			##		'Roll': True,
		## 			## },
		## 		},

		## 		## a list of keys which also link to the accessor func, property, names?
		## 		'keys': {
		## 			## 'roll': { },
		## 			## 'r': { },
		## 		},

		## 		## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
		## 		'names': {
		## 			## 'Roll': { },
		## 			## 'R': { },
		## 		},

		## 		## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
		## 		'call_names': {
		## 			## 'GetRollAccessor': '',
		## 			## 'GetRollAllowedTypes': '',
		## 			## 'GetRoll': ''
		## 		},

		## 		## ## Accessors list is a simple list of all accessor objects
		## 		## 'accessors': [

		## 		## ]

		## 		##
		## 		## '': [],
		## 		## '': [],
		## 		## '': []
		## 	}
		## );


		## ## function this.ResetAccessors( [ _name, ]+ ) - Resets the value for each key / name submitted...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameReset( ), lambda this, _name, *_names: self.ResetAccessorFuncValues( this, True, _name, *_names ) )

		## ## function this.SetAccessors( [ _name, _value, ]+ ) - Sets the value for each Accessor name supplied... SetAccessorFuncValues( self, this = None, _by_name = True, *_args )
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameSet( ), lambda this, _name, _value, *_args: self.SetAccessorFuncValues( this, True, _name, _value, *_args ) )

		## ## function this.GetAccessors( [ _name, ]+ ) - Returns the value for each Accessor name supplied... Can be returned into a single var as a list, or the list can be split simply by using _value, _value2, _value3 = this.GetAccessors( _name, _name2, _name3 )
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGet( ), lambda this, _name, *_names: self.GetAccessorFuncValues( this, True, _name, *_names ) )




		## ## function this.ResetAccessorsByKey( [ _key ]+ ) - Resets the value for each Accessor key submitted...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameResetByKey( ), lambda this, _key, *_keys: self.ResetAccessorFuncValues( this, False, _key, *_keys ) )

		## ## function this.SetAccessorsByKey( [ _key, _value, ]+ ) - Assigns a value to each Accessor for each key / value pair provided.
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameSetByKey( ), lambda this, _key, _value, *_args: self.SetAccessorFuncValues( this, False, _key, _value, *_args ) )

		## ## function this.GetAccessorsByKey( [ _key ]+ ) - Resets ( Assigns None Value ) to each Accessor bound to each key provided.
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGetByKey( ), lambda this, _key, *_keys: self.GetAccessorFuncValues( this, False, _key, *_keys ) )




		## ## function this.GetAccessorsDB( this ) - Returns the Accessor DB for the instance object - a list of all accessors, references, by name / key, and more information...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGetDB( ), lambda this: getattr( this, 'accessor_func_db', [ ] ) )

		## ## function this.AddAccessorsDB( this ) - Adds a new entry to the database
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameAddDB( ), lambda this: None )

		## ## function this.ResetAccessorsDB( this ) - Resets the entire Accessor Func Database - This shouldn't be used unless all elements have been removed - a lot of work...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameResetDB( ), lambda this: None )






		##
		## Setup the Parent Class __str__ function, if one doesn't already exist - output all of the accessors in the db, and more...
		##
		if ( getattr( _parent, 'AccessorOverrideFunc__str__', False ) ): #not callable( getattr( _parent, '__str__' ) ) ):
			##
			## Parent __str__
			##
			def InstanceToString( this ):
				return 'Testing Override ToString Function...';

			##
			## Register Special SetupInstanceHelpers Accessors for the Instance Class Calls...
			##
			##

			##
			if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
				##
				print( '\nRegisterAccessor -> RegisterNamedAccessor id: ' + Text.FormatColumn( 30, '__str__', 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );

			## function self.GetAccessorsDB( )																, '__str__'
			self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE__STR__, InstanceToString );


		## MAP_ACCESSORFUNC_IDS
		## MAP_ACCESSORFUNC_ID_FUNC_NAMES
		## MAP_ACCESSORFUNC_ID_FUNC_ARGS
		## MAP_ACCESSORFUNC_ID_FUNC_CALL_NAMES


		## ACCESSORFUNC_ID_INSTANCE,
		## ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_SET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_GET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE,
		## ACCESSORFUNC_ID_INSTANCE_GET_DATABASE,
		## ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE

		##
		## Helpers used to Update the values, or retrieve the values, by Accessor Group ID
		##





		##
		## Helpers used to perform actions on Accessor References ( used for the group functions which assign data, etc.. which use the Get/Add/Set/ResetGroupAccessors functions to retrieve accessor references, set, add, reset.. ) such as updating an entire group of Accessor Values to a single value, etc..
		##





		##
		## Helpers used to Update the values, or retrieve the values, by Accessor name, of Accessor Funcs
		##


		##
		## function this.ResetAccessors( [ _name, ]+ ) - Resets the value for each key / name submitted...
		##
		def ResetAccessors( this, _name, *_names ):
			return self.ResetAccessorFuncValues( this, True, _name, *_names );


		##
		## function this.SetAccessors( [ _name, _value, ]+ ) - Sets the value for each Accessor name supplied...
		##
		def SetAccessors( this, _name, _value, *_args ):
			return self.SetAccessorFuncValues( this, True, _name, _value, *_args );


		##
		## function this.GetAccessors( [ _name, ]+ ) - Returns the value for each Accessor name supplied... Can be returned into a single var as a list, or the list can be split simply by using _value, _value2, _value3 = this.GetAccessors( _name, _name2, _name3 )
		##
		def GetAccessors( this, _name, *_names ):
			return self.GetAccessorFuncValues( this, True, _name, *_names );


		##
		## Helpers used to Update the values, or retrieve the values, by Accessor key, of Accessor Funcs
		##


		##
		## function this.ResetAccessorsByKey( [ _key ]+ ) - Resets the value for each Accessor key submitted...
		##
		def ResetAccessorsByKey( this, _key, *_keys ):
			return self.ResetAccessorFuncValues( this, False, _key, *_keys );


		##
		## function this.SetAccessorsByKey( [ _key, _value, ]+ ) - Assigns a value to each Accessor for each key / value pair provided.
		##
		def SetAccessorsByKey( this, _key, _value, *_args ):
			return self.SetAccessorFuncValues( this, False, _key, _value, *_args );


		##
		## function this.GetAccessorsByKey( [ _key ]+ ) - Resets ( Assigns None Value ) to each Accessor bound to each key provided.
		##
		def GetAccessorsByKey( this, _key, *_keys ):
			return self.GetAccessorFuncValues( this, False, _key, *_keys );

		##
		## Helpers used to manage Accessor Groupings which can contain 1 to many ( 0 will not exist ) Accessor References by Name and / or Key - Reset Accessors in group, Assign Accessors to Group, Add Accessors To Group without resetting the group as Set does, and to Retrieve / Get the list of Accessor References in a group...
		##


		##
		## function this.ResetGroupAccessors( [ _group ]+ ) - Resets the Group Accessor List, doesn't change the value of each Accessor ( For that, use the ByGroup designated function calls )
		##
		def ResetGroupAccessors( this, _group, *_groups ):
			return None; #self.ResetAccessorFuncValues( this, False, _key, *_groups )


		##
		## function this.SetGroupAccessors( _group, [ _name, ]+ ) - Resets the Group Accessor List to only include the Accessors provided.
		##
		def SetGroupAccessors( this, _group, _name, *_names ):
			return None; #self.SetAccessorFuncValues( this, False, _key, _name, *_names )


		##
		## function this.AddGroupAccessors( _group, [ _name, ]+ ) - Adds the Accessors provided to the Group Accessor List without Resetting it..
		##
		def AddGroupAccessors( this, _group, _name, *_names ):
			return None; #self.SetAccessorFuncValues( this, False, _group, _name, *_names )


		##
		## function this.GetGroupAccessors( [ _group, ]+ ) - Returns a list of Accessors assigned to the group provided..
		##
		def GetGroupAccessors( this, _group, *_groups ):
			return None; #getattr( this.accessor_func_db.groups, _group, [ ] )

		##
		## Helpers pertaining to the instance object AcccessorFunc DB ( Note: * - by default - value for group name refers to ALL elements which is used to print out functions created, etc.. when that option is enabled, or in an __str__ function in the instance class )
		##


		##
		## function this.GetAccessorsDB( this ) - Returns the Accessor DB for the instance object - a list of all accessors, references, by name / key, and more information...
		##
		def GetAccessorsDB( this ):
			return getattr( this, 'accessor_func_db', { } );


		##
		##
		##
		def GetAccessorsGroupsDB( this ):
			return this.GetAccessorsDB( ).get( 'groups', None );


		##
		##
		##
		def GetAccessorsKeysDB( this ):
			return this.GetAccessorsDB( ).get( 'keys', None );


		##
		##
		##
		def GetAccessorsNamesDB( this ):
			return this.GetAccessorsDB( ).get( 'names', None );


		##
		##
		##
		def GetAccessorsFuncNamesDB( this ):
			return this.GetAccessorsDB( ).get( 'call_names', None );


		##
		##
		##
		def GetAccessorsReferenceDB( this ):
			return this.GetAccessorsDB( ).get( 'accessors', None );


		##
		## function this.AddAccessorsDB( this ) - Adds a new entry to the database
		##
		def AddAccessorsDB( this ):
			## Grab the database
			_db = GetAccessorsDB( this );

			## Add the data ( key and name quick entries, as well as references, etc... so we can search by both - maybe even add function names to the searchable table )


			return True;


		##
		## function this.ResetAccessorsDB( this ) - Resets the entire Accessor Func Database - This shouldn't be used unless all elements have been removed - a lot of work...
		##
		def ResetAccessorsDB( this ):
			## Task: Todo: Right now, we don't need it - unless a dynamic class is made where the accessors aren't static - right now it won't be necessary but it may become necessary later which is why a blank function exists..
			return False;


		##
		## Register all Remaining SetupInstanceHelpers Accessors for the Instance Class Calls... Note: These instance functions are different because they are only added once and aren't Name specific. These are helpers added to the parent class to help perform accessor actions using less lines, etc... ie: ResetAccessors( 'ThreadJobs' ,'Thread', 'Blah' ), ResetAccessorsByKey( 'thread_jobs', 'thread', 'blah' ), ResetAccessorsByGroup( 'threading' ), etc..
		##

		##
		## Note: These functions are added to the PARENT class, the class which is receiving a property but only the first property adds these, so it has helper functions to manage Accessors easier...
		##

		##
		if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			print( '\nRegisterAccessor -> RegisterInstanceAccessors ' + Text.FormatColumn( 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) );

			##
			if ( not self.Setup( 'instance' ) ):
				print( '\tNote: This list appears empty - this means setup = { } was redefined without "instance": true, or setup_override = { "instance": false, } was defubed... This means the instance helpers are not added... If just one other AccessorFunc allows them, they are added... They are only ever added once too..' );


		##
		## Task: Object Helper Accessors - These are added to an object so the object has dev-friendly accessorfunc helpers to quickly reset data for multiple accessors, update data, and much more.. - Setup new method of creating these...
		##

		## function self.ResetAccessors( [ _name, ]+ )														, self.AccessorsNameReset( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ResetAccessors );

		## function self.SetAccessors( [ _name, _value ]+ )													, self.AccessorsNameSet( _name, _value, ...)
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, SetAccessors );

		## function self.GetAccessors( [ _name, ]+ )														, self.AccessorsNameGet( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, GetAccessors );

		## function self.ResetAccessorsByKey( [ _key ]+ )													, self.AccessorsNameResetByKey( _key, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ResetAccessorsByKey );

		## function self.SetAccessorsByKey( [ _key, _value, ]+ )											, self.AccessorsNameSetByKey( _key, _value, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, SetAccessorsByKey );

		## function self.GetAccessorsByKey( [ _key ]+ )														, self.AccessorsNameGetByKey( _key, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, GetAccessorsByKey );

		## function self.ResetGroupAccessors( [ _group ]+ )													, self.AccessorsNameResetGroup( _group, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ResetGroupAccessors );

		## function self.SetGroupAccessors( _group, [ _name, ]+ )											, self.AccessorsNameSetGroup( _group, _dict? )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, SetGroupAccessors );

		## function self.GetGroupAccessors( [ _group, ]+ )													, self.AccessorsNameGetGroup( _group, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, GetGroupAccessors );

		## function self.AddGroupAccessors( _group, [ _name, ]+ )											, self.AccessorsNameAddGroup( _group, _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, AddGroupAccessors );

		## function self.ResetAccessorsDB( )																, self.AccessorsNameResetDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ResetAccessorsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, GetAccessorsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_FUNC_NAMES_DATABASE, GetAccessorsFuncNamesDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_GROUPS_DATABASE, GetAccessorsGroupsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_KEYS_DATABASE, GetAccessorsKeysDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_NAMES_DATABASE, GetAccessorsNamesDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_REFERENCES_DATABASE, GetAccessorsReferenceDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, GetAccessorsDB );

		## function self.AddAccessorsDB( )																	, self.AccessorsNameAddDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, AddAccessorsDB );

		## function self.GetAccessorsWiki( )																, self.AccessorsNameGetWIKI( )
		## self.RegisterAccessor( _parent, 'instance_wiki', GetAccessorsWIKI );

		## function self.CreateWikiCSV( )																	, self.AccessorsNameGetWiki( )
		## self.RegisterAccessor( _parent, 'instance_wiki', GetAccessorsWiki );

		## function self.GetInstanceWiki( )																	, self.AccessorsNameGetInstanceWiki( )
		## self.RegisterAccessor( _parent, 'instance_helpers_wiki', GetAccessorsInstanceWiki );

		## print( );


	##
	## Helper to setup Accessor Functions...
	##
	def SetupAccessorFuncs( self, _parent = None ):
		##
		_key			= self.Key( );
		_key_data		= self.GetDataKey( );
		_key_accessor	= self.GetAccessorKey( );
		_name			= self.Name( );


		##
		## The following are Getters / Setters created for the class requesting them... I use this instead of self for this functions so it is clear that self represents AccessorFuncBase, this represents the class instance requesting the functions, _parent represents the non-instanced class requesting these functions.
		##


		##
		## function self.Get<Name>( _default_override = None, _ignore_defaults = False )
		##
		## Args: Override Default as _default_override - This overrides the default value in favor of an on a per-call default setting basis..
		## Args: Ignore ALL Defaults as _no_default - This overrides all default values in favor of returning None if a value isn't set...
		##
		def AccessorFuncGet( this, _default_override = None, _ignore_defaults = False ):
			## print( 'AccessorFuncGet args: ' + str( this ) + ' // ' + str( _default_override ) + ' // ' + str( _ignore_defaults ) + ' // ' + str( _x ) )
			## Debugging
			## print( '[ AccessorFunc ] ' + _name_get + ' is being called!' ) ( Logic.TernaryFunc( _default_override != None, _default_override, _default ) )

			##
			_value = getattr( this, _key );
			_isset = AccessorFuncIsSet( this );

			## If we have a value to return, return it... Or, if we don't have a value to return and we ignore defaults, we return the stored value ( None )...
			if ( ( _isset ) or ( not _isset and _ignore_defaults ) ):
				## Return the value assigned as it is set...
				return AccessorFuncGetRaw( this );

			## If there is no override value, we return the value retrieved from the getter which returns the original default value... Otherwise we return the override default value...
			if ( _default_override == None ):
				return _value;

			## Return the default override...
			return _default_override;


		##
		## function self.Get<Key>Raw( _default ) - Returns the value stored under _<key>
		##
		def AccessorFuncGetRaw( this ):
			return getattr( this, self.GetDataKey( ) );


		##
		## function self.Is<Key>Set( _default ) - Returns whether or not there is a value stored in _<key> ( By default, there isn't... )
		##
		def AccessorFuncIsSet( this ):
			return AccessorFuncGetRaw( this ) != None;



		##
		## function self.Get<Key>ToString( _default ) - Returns the data in string-format..
		##
		def AccessorFuncGetToString( this, _default_override = None, _ignore_defaults = False ):
			return str( AccessorFuncGet( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>Len( _default ) - Returns the Length of characters of the data
		##
		def AccessorFuncGetLen( this, _default_override = '', _ignore_defaults = False ):
			##
			return len( AccessorFuncGet( this, _default_override, _ignore_defaults ) );
			## return len( AccessorFuncGetToString( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>LenToString( _default ) - Returns the Length of characters of the data
		##
		def AccessorFuncGetLenToString( this, _default_override = '', _ignore_defaults = False ):
			##
			return str( AccessorFuncGetLen( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>DefaultValue( ) -
		##
		def AccessorFuncGetDefaultValue( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameDefaultValue( ) );


		##
		## function self.Get<Key>Property( _default ) - Returns the property
		##
		def AccessorFuncGetProperty( this ):
			##
			return getattr( this, self.Key( ) );



		##
		## function self.Get<Key>Accessor( _default ) - Returns the accessor, or it calls the accessor with args but includes self without the client needing to...
		##
		## Helper used as: return GetKeyAccessor( this, 'FunctionName', *_args ) - typical use: return GetKeyAccessor( this, 'Set', this, _value )
		## Note: *_args are left without automatic this because not all functions are formulated def Set( self, _parent, _value = None ):... Some may only need self ( the first this represents that automatically via getattr ), the other args after self are represented by *_args and the function name by 'Set' or 'FunctionName'...
		##
		def AccessorFuncGetAccessor( this, _func_name = None, *_args ):
			## ## Grab the accessor reference		_accessor = getattr( this, self.GetAccessorKey( ) )
			## _accessor = getattr( this, '__' + _key )

			## ## Determine, if a function name is given, if that function is callable... If it is, we call it with supplied arguments..
			## if ( _func_name and callable( getattr( _accessor, _func_name ) ) ):
			## 	return getattr( _accessor, _func_name )( *_args )

			## ## If no function name is provided, or the function isn't callable ( read as: doesn't exist, or some other issue ) then we return the accessor reference to simplify things....
			## return _accessor

			## Determine, if a function name is given, if that function is callable... If it is, we call it with supplied arguments..
			if ( _func_name and callable( getattr( self, _func_name ) ) ):
				return getattr( self, _func_name )( *_args );

			## If no function name is provided, or the function isn't callable ( read as: doesn't exist, or some other issue ) then we return the accessor reference to simplify things....
			return self;


		##
		## function self.Get<Key>AllowedTypes( ) - Returns a Dict of allowed types in O( 1 ) format...
		##
		def AccessorFuncGetAllowedTypes( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAllowedTypes( ) );


		##
		## function self.Get<Key>AllowedValues( ) - Returns a Dict of allowed values in O( 1 ) format...
		##
		def AccessorFuncGetAllowedValues( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAllowedValues( ) );


		##
		## function self.Get<Key>BaseAllowedTypes( ) - Returns a Dict of allowed types in O( 1 ) format...
		##
		def AccessorFuncGetBaseAllowedTypes( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetBaseAllowedTypes( ) );


		##
		## function self.Get<Key>BaseAllowedValues( ) - Returns a Dict of allowed values in O( 1 ) format...
		##
		def AccessorFuncGetBaseAllowedValues( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetBaseAllowedValues( ) );


		##
		## function self.Has<Key>GetterPrefix( ) - Returns whether or not a Getter Prefix exists ( Must be String data-type, non '' )
		##
		def AccessorFuncHasGetterPrefix( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameHasGetterPrefix( ) );


		##
		## function self.Get<Key>GetterPrefix( ) - Returns the Getter PRefix ( Returns '' if doesn't have a prefix )
		##
		def AccessorFuncGetGetterPrefix( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetterPrefix( ) );


		##
		## function self.Get<Key>Name( ) - Returns the Formal ( Title-Case, typically ) Name for this Property
		##
		def AccessorFuncGetName( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameName( ) );



		##
		## function self.Get<Key>Key( ) - Returns the ID ( typically lowercase ) Key for this Property
		##
		def AccessorFuncGetKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameKey( ) );


		##
		## function self.Get<Key>AccessorKey( ) - Returns the Internal Accessor Key for this Property ( Typically __<key> )
		##
		def AccessorFuncGetAccessorKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAccessorKey( ) );


		##
		## function self.Get<Key>DataKey( ) - Returns the Internal Data Storage Key for this Property ( Typically _<key> )
		##
		def AccessorFuncGetDataKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetDataKey( ) );


		##
		## function self.Set<Key>( _value ) -
		##
		def AccessorFuncSet( this, _value = None ):
			return AccessorFuncGetAccessor( this, 'Set', this, _value );


		##
		## function self.Add<Key>( _value ) -
		##
		def AccessorFuncAdd( this, _value = None ):
			return AccessorFuncGetAccessor( this, 'Add', this, _value );


		##
		## function self.Pop<Key>( _index = len - 1, _count = 1 ) -
		##
		def AccessorFuncPop( this, _index = None, _count = 1 ):
			return AccessorFuncGetAccessor( this, 'Pop', this, _index, _count );


		##
		## function self.Reset<Key>( _value ) -
		##
		def AccessorFuncReset( this ):
			## return GetKeyAccessor( this, 'Set', this, None )
			return AccessorFuncGetAccessor( this, 'Reset', this );


		##
		## Output all Instance Functions added to the parent class which initialized the Accessor Properties
		##
		def GetInstanceWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...


			## Task: Output layout of wiki should be either | list of names vertical | next one along side | next | next - but word-wrap is bad, so it'd need to be output to the output panel, or I'll open it like I open the expanded vars example... I should also update the plugin output info the same way...
			## Task: Output layout of wiki: Either before or after the functions list, show the Accessor stats such as the name before outputting functions, getter prefix, etc... Since this is the instance object un-named, it covers all accessors added to the instance so we need to update the database in the actual instance / parent... and read from it...

			##
			## Parent Object Information
			## Number of Accessors, the names of all of them in a simple list, etc..
			##
			## Maybe more info...
			## Maybe more info...
			##
			## Function List Header - Name above each, list of functions used... Format String...
			## function list | function list | function list |
			## function list | function list | function list |
			## function list | function list | function list |
			##
			## Maybe more info...
			## Maybe more info...
			##
			##



			## For each Name, grab named wikis...







			##
			return _text;


		##
		## Output all Single Name / Accessor / Property-Specific Functions added
		##
		def GetNameWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...


			##
			## Task: This is the name wiki... Basically, it gets the longest name and gives the length, adds a buffer and that's how wide the column will be for all of the functions...
			## Task: May need more... So, GetNameWiki should be for each Named function, so ... hmm.....
			##
			##




			##
			return _text;


		##
		## Output all Names / Accessors / Properties added to the parent class... This should be complete ( calls made to GetNameWiki for each Accessor set added, and one time call to GetInstanceWiki to output those functions and details )
		##
		def GetAccessorWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...
			##
			## Get Accessor Wiki - This should be the wiki entry for everything this accessor related... This accessor is name based, the functions created, and more... Or, am I formatting it differently? GetNameWiki basically does the same thing... not sure... InstanceWiki pulls it all together...
			##






			##
			return _text;


		##
		##
		##
		def GetGetterOutput( this, _name, _default_override = None, _ignore_defaults = False, _getter_prefix = 'Get' ):
			_col_key = 35;
			_col_value = 20;
			_show_args = False;

			##
			_default_override_text = 'D: ';
			_ignore_defaults_text = 'I: ';

			## Task: Setup Accessor DB to log all keys / names, etc.... and some helper accessors, instances, or other info, in order to easily grab data...
			## Exit self ( stuck on 1 object because called by __str__ self - trying to call on all keys / names... ), re-enter using provided name as key or modify name to fit key, if it works... to get proper data...
			_base = self.GenerateAccessorKey( this );

			##
			_args = '' #_default_override_text + ': ' + str( _default_override ) + ', ' + _ignore_defaults_text + ': ' + str( _ignore_defaults )

			## ##
			## _text = ''
			## _text += String.FormatColumn( _col_key - 13,		'self.Is' + _name + 'Set( ): ',														_col_value - 10,	str( getattr( this, 'Is' + _name + 'Set' )( ) ) )
			## ## _text += String.FormatColumn( _col_key - 14,	'self.' + _getter_prefix + _name + '( ' + _args + ' ): ',							_col_value,			str( getattr( this, _getter_prefix + _name + '' )( _default_override, _ignore_defaults ) ) )
			## _text += String.FormatColumn( _col_key - 6,		'self.' + _getter_prefix + _name + 'ToString( ' + _args + ' ): ',					_col_value,			getattr( this, _getter_prefix + _name + 'ToString' )( _default_override, _ignore_defaults ) )
			## ## _text += String.FormatColumn( _col_key,		'self.' + _getter_prefix + _name + 'Len( ' + _args + ' ): ',						_col_value,			str( getattr( this, _getter_prefix + _name + 'Len' )( _default_override, _ignore_defaults ) ) )
			## _text += String.FormatColumn( _col_key - 3,		'self.' + _getter_prefix + _name + 'LenToString( ' + _args + ' ): ',				_col_value,			getattr( this, _getter_prefix + _name + 'LenToString' )( _default_override, _ignore_defaults ) )
			## _text += String.FormatColumn( _col_key - 12,		'self.' + _getter_prefix + _name + 'Raw( ): ',										_col_value,			str( getattr( this, _getter_prefix + _name + 'Raw' )( ) ) )
			## _text += String.FormatColumn( _col_key - 3,		'self.' + _getter_prefix + _name + 'DefaultValue( ): ',								_col_value,			str( getattr( this, _getter_prefix + _name + 'DefaultValue' )( ) ) )

			##
			_text = '';
			_text += String.FormatColumn( _col_key - 13,	self.AccessorCallNameIsSet( _show_args ) + ': ',				_col_value - 10,	str( self.AccessorCallIsSet( this ) ) );
			_text += String.FormatColumn( _col_key - 14,	self.AccessorCallNameGet( _show_args ) + ': ',					_col_value + 5,		str( self.AccessorCallGet( this, _default_override, _ignore_defaults ) ) );
			_text += String.FormatColumn( _col_key - 12,	self.AccessorCallNameGetRaw( _show_args ) + ': ',				_col_value + 5,		str( self.AccessorCallGetRaw( this ) ) );
			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetDefaultValue( _show_args ) + ': ',		_col_value + 5,		str( self.AccessorCallGetDefaultValue( this ) ) );
			_text += String.FormatColumn( _col_key - 12,	self.AccessorCallNameGetLen( _show_args ) + ': ',				_col_value - 15,	str( self.AccessorCallGetLen( this, _default_override, _ignore_defaults ) ) );

			## _text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameHasGetterPrefix( _name, _getter_prefix, _show_args ) + ': ',		_col_value - 10,	str( getattr( getattr( this, '__' + _name, None )self.AccessorCallHasGetterPrefix( this, _name, _getter_prefix ) ) )
			## _text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetGetterPrefix( _name, _getter_prefix, _show_args ) + ': ',		_col_value - 10,	str( getattr( getattr( this, '__' + _name, None )self.AccessorCallGetGetterPrefix( this, _name, _getter_prefix ) ) )

			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameHasGetterPrefix( _show_args ) + ': ',		_col_value + 25,	str( getattr( _base, 'GetBaseAllowedTypes' )( ) ) );
			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetGetterPrefix( _show_args ) + ': ',		_col_value + 25,	str( getattr( _base, 'GetBaseAllowedValues' )( ) ) );

			## Need to link allowed types through to instance?? Since this is a
			## _text += String.FormatColumn( _col_key - 3,		self.StaticNameGetBaseAllowedTypes( ) + ': ',											_col_value + 20,	str( self.GetBaseAllowedTypes( ) ) )
			## _text += String.FormatColumn( _col_key - 2,		self.StaticNameGetBaseAllowedValues( ) + ': ',											_col_value + 20,	str( self.GetBaseAllowedValues( ) ) )


			## _text += String.FormatColumn( _col_key - 15,		self.AccessorCallNameGetToString( _name, _getter_prefix, _show_args ) + ': ',				col_value + 5,		self.AccessorCallGetToString( this, _default_override, _ignore_defaults, _name, _getter_prefix ) )
			## _text += String.FormatColumn( _col_key - 12,		self.AccessorCallNameGetLenToString( _name, _getter_prefix, _show_args ) + ': ',			_col_value - 10,	self.AccessorCallGetLenToString( this, _default_override, _ignore_defaults, _name, _getter_prefix ) )


			return _text # + '\n'


		##
		##
		##
		def GetKeyOutput( this, _key = None, _header = False, _footer = False ):
			##
			if ( _key == None and not _header and not _footer):
				return '\n\t\t[ No Key Provided ]';

			##
			def GetHorizontalBar( ):
				## Max Width: 467 + tab = 471 + left = 581
				_text = String.FormatColumnEx( '-',		_col_key_tab,		''	)
				_text += String.FormatColumnEx( '-',	_col_key,			'',		_col_val,			''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_raw,		'',		_col_val_raw,		''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_default,	'',		_col_val_default,	''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_types,		'',		_col_val_types,		''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_values,	'',		_col_val_values,	''	) + _text_divider + _text_divider + '\n';
				_text += '';

				return _text;

			##
			def GetHeader( ):
				_text = '';
				_text += GetHorizontalBar( );
				_text += this.Name + '\t\t --- MyClass: ---- id( this.__class__ ): ' + str( id( this.__class__ ) ) + ' :::: id( this ): ' + str( id( this ) )+ '\n';
				_text += GetHorizontalBar( );
				_text += String.FormatColumnEx( ' ',	_col_key_tab,		'' )
				_text += String.FormatColumnEx( ' ',	_col_key,			'Key',								_col_val,			'Getter Value',						_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_raw,		'Raw Key',							_col_val_raw,		'Raw / Stored Value',				_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_default,	'Get Default Value',				_col_val_default,	'Default Value',					_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_types,		'Get Allowed Types',				_col_val_types,		'Allowed Types',					_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_values,	'Get Allowed Values',				_col_val_values,	'Allowed Values',					_col_key_divider,	_char_divider )+ '\n';
				_text += GetHorizontalBar( );

				return _text;

			##
			def GetFooter( ):
				return String.FormatColumnEx( '~',	( _col_key_divider * 5 - 1 + _col_key_tab + _col_key + _col_val + _col_key_raw + _col_val_raw + _col_key_default + _col_val_default + _col_key_types + _col_val_types + _col_key_values + _col_val_values ),		'' ) + '\n';


			## Max Width: 467 + tab = 471 + left = 581

			##
			_col_key_tab			= 4;
			_col_key_divider		= 2;
			_col_key				= 10;
			_col_key_raw			= 10;
			_col_key_default		= 30;
			_col_key_types			= 30;
			_col_key_values			= 30;

			##
			_col_val				= 20;
			_col_val_raw			= 25;
			_col_val_default		= 25;
			_col_val_types			= 75;
			_col_val_values			= 185;

			_char_divider_bar		= '|-';
			_char_divider			= '| ';

			##
			_text_divider			= String.FormatColumnEx( '-',	_col_key_divider,	_char_divider_bar	);

			_text					= '';

			##
			if ( _header ):
				_text += GetHeader( );

			##
			if ( _key ):
				##
				_default									= getattr( this, _key );
				_raw										= getattr( this, '_' + _key );
				_accessor									= getattr( this, '__' + _key );
				## _getter_prefix							= _accessor.GetterPrefix( );
				## _name									= _accessor.Name( );
				_internal									= _accessor.DefaultValue( );
				_allowed_types_desc, _allowed_values_desc	= _accessor.GetAllowedDesc( );


				_text += '\t'
				_text += String.FormatColumn(			_col_key,			_key + ':',										_col_val,			str( _default ),						_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_raw,		'_' + _key + ':',								_col_val_raw,		str( _raw ),							_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_default,	'__' + _key + '.DefaultValue( ):',				_col_val_default,	str( _internal ),						_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_types,		'__' + _key + '.GetAllowedTypes( )',			_col_val_types,		str( _allowed_types_desc ),				_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_values,	'__' + _key + '.GetAllowedValues( )',			_col_val_values,	str( _allowed_values_desc ),			_col_key_divider,	_char_divider );
				_text += '\n';


			##
			if ( _footer ):
				_text += GetFooter( );

			##
			return _text;


		##
		## function self.Get<Key>Helpers( ) - Returns the complete Helper Function List for this property ( May or may not include internal functions )...
		##
		def AccessorFuncGetHelpers( this, _header = False ):
			## Buffer bound to give the name 20 chars max - all names should line up as long as the name is 20 chars or less... otherwise it'll be cut off..
			_buffer = 20 - len( _name );

			_col_func = 80;
			_col_desc = 140;
			_col_value = 250;

			## Text Output...
			_text = '';

			## Text output for the row of functions...
			_text += '\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n';
			if ( _header ):
				_text += String.FormatColumnEx( ' ',		_col_value, 'Proper usage of this property ranges ( Please Note: The key vs name is CaSe-SeNsItIvE - the key is typically lowercase and the Name, used in Function Calls, is Title or UpperCamelCase ):' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\tUsing the property itself to Set / Get - When setting a value, if you defined authorized data-types / values, then the value will only be assigned if it passes those checks, regardless of method you use to set the data ( unless you set it to the internal storage key )' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_class.' + self.Key( ) + ' = VALUE' ) + '\n';
				## _text += '\n';
				## _text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.' + self.Key( ) + ' == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_var = _class.' + self.Key( ) + '' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\tTo using the Accessor Functions Provided to make coding in Python feel more like standard programming languages' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_class.Set' + self.Name( ) + '( VALUE )' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( ) == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_var = _class.Get' + self.Name( ) + '( )' ) + '\n';
				_text += '\n\t\t## Note: The Functions have useful OPTIONAL arguments to overwrite default behavior - The Getter allows you to override the default default value if no value is set... The second arg also allows you to ignore default values, so if a value isn\'t set, nothing will be returned...\n';
				## _text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( DEFAULT_OVERRIDE, IGNORE_DEFAULTS ) == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += '\t\t## This example would overwrite the default value of: ' + str( self.DefaultValue( ) ) + ' to None, if a value was never assigned / set. Ignore defaults is False by default, and it is optional so omitting it is perfectly fine..\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( None ) == VALUE ):\n\t\t\treturn' ) + '\n';
				_text += '\n\t\t## This example would be the same thing as above, but None from the first argument is not used.. ie.. if a value was never set, None would be the value stored.. Since ignore defaults is true, a default value will not be substituted for None - so None would be returned...\n\t\t## I could change the first arg None to "Blah" and None would still be returned, unless I change True to False, or remove the second arg as ignore defaults is optional, and allow Defaults... Then "Blah" would only be returned if a value was never set..\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( None, True ) == VALUE ):\n\t\t\treturn' ) + '\n';
				_text += '\n\t\tPlease Note: It doesn\'t matter if you use the Property index method or the Accessor Function method to Get or Set values... They are both processed identically - it is 100% your personal preference. ie you can not set a banned data-type or value using one method over the other if you set authorized data-types / values when you initialize the Accessor Function System.';
				_text += '\n';
			_text += '\n';1

			_text += '\n';
			_text += 'Documentation for this Accessor:\n';
			_text += self.Documentation( );

			_text += '\n';
			_text += '\n';

			## Base Accessor Helpers in Instance Object
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\tthis.ResetAccessors( _name [ , _name ] ... )',						_col_desc, 'Instance Class Helper Function: Allows resetting many key stored values to None on a single line.',												_col_value, 'In short: Calls this.Reset<Name>() for each _name provided...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\tthis.SetAccessors( _name, _value [ , _name, _value ] ... )',			_col_desc, 'Instance Class Helper Function: Allows assigning many keys / values on a single line - useful for initial setup, or to minimize lines...',		_col_value, 'In short: Calls this.Set<Name>( _value ) for each _name / _value pairing...'											 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t_data  =\tthis.GetAccessors( _name [ , _name ] ... )',						_col_desc, 'Instance Class Helper Function: Allows getting the stored values on a single line.',															_col_value, 'In short: Returns this.Get<Name>() for each _name provided... Note: You can use the _data = format to return the [ ] returned...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t_a, _b, _c =\tthis.GetAccessors( _name [ , _name ] ... )',					_col_desc, 'Instance Class Helper Function: Allows getting the stored values on a single line.',															_col_value, 'In short: Returns this.Get<Name>() for each _name provided... Note: You can split the list by adding variable assignments in the same order you get each value...'	 ) + '\n';
			_text += '\n';

			## Base Accessor Helpers in Instance Object
			## this.SetAccessorsByKey( _key, _value [ , _key, _value ] ... )
			## this.ResetAccessorsByKey( _key [ , _key ] ... )
			## this.GetAccessorsByKey( _key [ , _key ] ... )
			##
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameResetByKey( ),								_col_desc, self.AccessorsDescResetByKey( ),					_col_value, 'In short: Calls this.<Key> = None; for each _key provided...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameSetByKey( ),								_col_desc, self.AccessorsDescSetByKey( ),					_col_value, 'In short: Calls this.<Key> = _value; for each _key / _value pairing...'											 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t_data  =\t' + self.AccessorsCallNameGetByKey( ),							_col_desc, self.AccessorsDescGetByKey( ),					_col_value, 'In short: Returns this.<Key> for each _key provided... Note: You can use the _data = format to return the [ ] returned...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t_a, _b, _c =\t' + self.AccessorsCallNameGetByKey( ),							_col_desc, self.AccessorsDescGetByKey( ),					_col_value, 'In short: Returns this.<Key> for each _key provided... Note: You can split the list by adding variable assignments in the same order you get each value...'	 ) + '\n';
			_text += '\n';

			## Group Accessor Helpers in Instance Object ( Each ID can be in more than 1 group )...
			## this.ResetAccessorsGroup( _name [ , _name ] ... )
			## this.SetAccessorsGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameResetGroup( ),									_col_desc, self.AccessorsDescResetGroup( ),					_col_value, 'In short: Calls this.Reset<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameSetGroup( ),									_col_desc, self.AccessorsDescSetGroup( ),					_col_value, 'In short: Calls this.Set<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameAddGroup( ),									_col_desc, self.AccessorsDescGetGroup( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameAddGroup( ),									_col_desc, self.AccessorsDescGetGroup( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += '\n';

			## Group Accessor Helpers in Instance Object ( Each ID can be in more than 1 group )...
			## this.ResetAccessorsGroup( _name [ , _name ] ... )
			## this.SetAccessorsGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameResetGrouped( ),								_col_desc, self.AccessorsDescResetGrouped( ),				_col_value, 'In short: Calls this.Reset<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameSetGrouped( ),									_col_desc, self.AccessorsDescSetGrouped( ),					_col_value, 'In short: Calls this.Set<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameGetGrouped( ),									_col_desc, self.AccessorsDescGetGrouped( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += '\n';


			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameResetDB( ),								_col_desc, self.AccessorsDescResetDB( ),					_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameSetDB( ),									_col_desc, self.AccessorsDescSetDB( ),						_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameGetDB( ),									_col_desc, self.AccessorsDescGetDB( ),						_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameAddDB( ),									_col_desc, self.AccessorsDescAddDB( ),						_col_value, ''	 ) + '\n';
			_text += '\n';




			_text += '\n';
			_text += '\nNote: Functions below may list self.Get / Set / Name( _args ) - self is meant as the class instance reference in the cases below - coded as this in AccessorFuncBase Class..\n';
			_text += '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGet( ),					_col_desc, self.AccessorDescGet( ),						_col_value,		str( self.AccessorCallGet( this ) )						) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetRaw( ),					_col_desc, self.AccessorDescGetRaw( ),					_col_value,		str( self.AccessorCallGetRaw( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameIsSet( ),					_col_desc, self.AccessorDescIsSet( ),					_col_value,		str( self.AccessorCallIsSet( this ) )					) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetToString( ),			_col_desc, self.AccessorDescGetToString( ),				_col_value,		str( self.AccessorCallGetToString( this ) )				) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetLen( ),					_col_desc, self.AccessorDescGetLen( ),					_col_value,		str( self.AccessorCallGetLen( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetLenToString( ),			_col_desc, self.AccessorDescGetLenToString( ),			_col_value,		str( self.AccessorCallGetLenToString( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetDefaultValue( ),		_col_desc, self.AccessorDescGetDefaultValue( ),			_col_value,		str( self.AccessorCallGetDefaultValue( this ) )			) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAccessor( ),			_col_desc, self.AccessorDescGetAccessor( ),				_col_value,		str( self.AccessorCallGetAccessor( this ) )				) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAllowedTypes( ),		_col_desc, self.AccessorDescGetAllowedTypes( ),			_col_value,		str( self.AccessorCallGetBaseAllowedTypes( this ) )		) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAllowedValues( ),		_col_desc, self.AccessorDescGetAllowedValues( ),		_col_value,		str( self.AccessorCallGetBaseAllowedValues( this ) )	) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetHelpers( ),				_col_desc, self.AccessorDescGetHelpers( ),				_col_value,		'THESE ROWS OF TEXT'				 					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetOutput( ),				_col_desc, self.AccessorDescGetOutput( ),				_col_value,		'ROWS OF TEXT'				 							) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetGetterOutput( ),		_col_desc, self.AccessorDescGetGetterOutput( ),			_col_value,		'ROWS OF TEXT'				 							) + '\n\n';


			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameSet( ),					_col_desc, self.AccessorDescSet( ),						_col_value,		'N / A'													) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameReset( ),					_col_desc, self.AccessorDescReset( ),					_col_value,		'N / A'													) + '\n';


			## _data = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )
			## print( '''_data = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )''' )
			## print( '-- _data as a List being output has ' + str( len( _data ) ) + ' entries...' )

			## _name, _x, _y, _z, _blah, _h, _w, _d = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )
			## print( '-- Now, instead of as a list, we use each individual var...' )
			## print( '''_name, _x, _y, _z, _blah, _h, _w, _d = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )''' )


			_text += '\n'
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameHasGetterPrefix( ),		_col_desc, self.AccessorDescHasGetterPrefix( ),			_col_value,		str( self.AccessorCallHasGetterPrefix( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetGetterPrefix( ),		_col_desc, self.AccessorDescGetGetterPrefix( ),			_col_value,		str( self.AccessorCallGetGetterPrefix( this ) )			) + '\n';

			_text += '\n'
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetName( ),				_col_desc, self.AccessorDescGetName( ),					_col_value,		str( self.AccessorCallGetName( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetKey( ),					_col_desc, self.AccessorDescGetKey( ),					_col_value,		str( self.AccessorCallGetKey( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAccessorKey( ),			_col_desc, self.AccessorDescGetAccessorKey( ),			_col_value,		str( self.AccessorCallGetAccessorKey( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetDataKey( ),				_col_desc, self.AccessorDescGetDataKey( ),				_col_value,		str( self.AccessorCallGetDataKey( this ) )				) + '\n';

			return _text




		## ##
		## ##
		## ##
		## def AccessorFuncGetOutput( self, this = None ):
		## 	return ''

		## function self.Get<Key>DefaultValue( )
		## AccessorFuncGetOutput.__name__ = self.AccessorNameGetOutput( );
		## setattr( _parent, AccessorFuncGetOutput.__name__, AccessorFuncGetOutput )


		## ##
		## ##
		## ##
		## def AccessorFuncGetGetterOutput( self, this = None ):
		## 	return ''

		## function self.Get<Key>DefaultValue( )
		## AccessorFuncGetGetterOutput.__name__ = self.AccessorNameGetGetterOutput( );
		## setattr( _parent, AccessorFuncGetGetterOutput.__name__, AccessorFuncGetGetterOutput )





		##
		## Register all SetupAccessorFuncs Accessors for the Instance Class Calls... - Note: These instance functions are added to the parent class too, but these are name specific.. They are added for each name added. so GetThreadJobs, GetThread, GetBlah, etc...
		##

		##
		if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			print( '\nRegisterAccessor -> RegisterNamedAccessors ' + Text.FormatColumn( 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );


		##
		## Task: Instance Added Accessors - These are the accessors created for variables within an objects instance...
		##
		## function self.Get<Name>( _default_override, _ignore_defaults )
		## function self.Get<N>Raw( )
		## function self.Is<N>Set( )
		## function self.Get<N>ToString( )
		## function self.Get<N>Len( )
		## function self.Get<N>LenToString( )
		## function self.Get<N>DefaultValue( )
		## function self.Get<N>Property( )
		## function self.Get<N>Accessor( )
		## function self.Get<N>AllowedTypes( )
		## function self.Get<N>AllowedValues( )
		## function self.Get<N>BaseAllowedTypes( )
		## function self.Get<N>BaseAllowedValues( )
		## function self.<N>HasGetterPrefix( )
		## function self.Get<N>GetterPrefix( )
		## function self.Get<N>GetName( )
		## function self.Get<N>GetKey( )
		## function self.Get<N>AccessorKey( )
		## function self.Get<N>DataKey( )
		## function self.Set<N>( )
		## function self.Add<N>( _
		## function self.Pop<N>( _
		## function self.Reset<N>( )
		## function self.Get<N>Helpers( )
		## function self.Get<N>Output( *
		## function self.Get<N>GetterOutput( *

		## function self.Get<Name>( _default_override, _ignore_defaults )														, self.AccessorNameGet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET, AccessorFuncGet );

		## function self.Get<Name>Raw( )																							, self.AccessorNameGetRaw( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_RAW, AccessorFuncGetRaw );

		## function self.Is<Name>Set( )																							, self.AccessorNameIsSet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_ISSET, AccessorFuncIsSet );

		## function self.Get<Name>ToString( )																					, self.AccessorNameGetToString( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_STR, AccessorFuncGetToString );

		## function self.Get<Name>Len( )																							, self.AccessorNameGetLen( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_LEN, AccessorFuncGetLen );

		## function self.Get<Name>LenToString( )																					, self.AccessorNameGetLenToString( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_LEN_STR, AccessorFuncGetLenToString );

		## function self.Get<Name>DefaultValue( )																				, self.AccessorNameGetDefaultValue( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, AccessorFuncGetDefaultValue );

		## function self.Get<Name>Property( ) - Like Get<Name>Accessor - instead of grabbing self / AccessorFuncBase object, we grab the property defined at <key>...
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_PROPERTY, AccessorFuncGetProperty );

		## function self.Get<Name>Accessor( ) - Uses Accessor ( self ) stored at this.__<Name> and then grabs FuncName and passes args :: ie self.AccessorFuncGetAccessor( 'FuncName', _Args ) which returns this.__<Key>.FuncName( *_args )																																					, self.AccessorNameGetAccessor( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ACCESSOR, AccessorFuncGetAccessor );

		## function self.Get<Name>AllowedTypes( )																				, self.AccessorNameGetAllowedTypes( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, AccessorFuncGetAllowedTypes );

		## function self.Get<Name>AllowedValues( )																				, self.AccessorNameGetAllowedValues( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, AccessorFuncGetAllowedValues );

		## function self.Get<Name>BaseAllowedTypes( )																			, self.AccessorNameGetBaseAllowedTypes( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, AccessorFuncGetBaseAllowedTypes );

		## function self.Get<Name>BaseAllowedValues( )																			, self.AccessorNameGetBaseAllowedValues( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, AccessorFuncGetBaseAllowedValues );

		## function self.<Name>HasGetterPrefix( )																				, self.AccessorNameHasGetterPrefix( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, AccessorFuncHasGetterPrefix );

		## function self.Get<Name>GetterPrefix( )																				, self.AccessorNameGetGetterPrefix( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_GETTER_PREFIX, AccessorFuncGetGetterPrefix );

		## function self.Get<Name>GetName( )																						, self.AccessorNameGetName( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_NAME, AccessorFuncGetName );

		## function self.Get<Name>GetKey( )																						, self.AccessorNameGetKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_KEY, AccessorFuncGetKey );

		## function self.Get<Name>AccessorKey( )																					, self.AccessorNameGetAccessorKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, AccessorFuncGetAccessorKey );

		## function self.Get<Name>DataKey( )																						, self.AccessorNameGetDataKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_DATA_KEY, AccessorFuncGetDataKey );

		## function self.Set<Name>( )																							, self.AccessorNameSet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_SET, AccessorFuncSet );

		## function self.Add<Name>( _value )																						, self.AccessorNameAdd( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_ADD, AccessorFuncAdd );

		## function self.Pop<Name>( _index = len - 1, _count = 1 )																, self.AccessorNamePop( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_POP, AccessorFuncPop );



		## function self.Reset<Name>( )																							, self.AccessorNameReset( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_RESET, AccessorFuncReset );

		## function self.Get<Name>Helpers( )																						, self.AccessorNameGetHelpers( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_HELPERS, AccessorFuncGetHelpers );

		## function self.Get<Name>Output( *_keys ... )																				, self.AccessorNameGetOutput( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_OUTPUT, GetKeyOutput );

		## function self.Get<Name>GetterOutput( *_keys ... )																			, self.AccessorNameGetGetterOutput( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, GetGetterOutput );

		## function self.Get<Key>Wiki( )																						, self.AccessorNameGetWiki( )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_WIKI, GetWiki );

		## print( );


	##
	##
	##
	def ProcessCallback( self, _parent, _id, *_args ):
		## print( 'AccessorFuncBase -> ProcessCallback for id: ' + str( _id ) + ' using: \n\tself:\t\t' + str( self ) + '\n\t_parent:\t'  + str( _parent ) + '\n\t_id:\t\t"'  + str( _id ) + '"\n\t_args:\t\t'  + str( _args ) + '\n' )
		if ( callable( self.Setup( _id ) ) ):
			## print( 'AccessorFuncBase -> ProcessCallback for id: ' + str( _id ) + ' -> callback' )
			self.Setup( _id )( _parent, *_args );
			## { 'on_set': ( lambda self, this, _current, _new, _has_updated: this.Unpack( ) ) }


	##
	## Returns the AccessorFuncc Object Setup Instructions - if _key is defined, pull the record for that instruction..
	##
	def Setup( self, _key = None ):
		if ( _key == None ):
			return self.__setup;

		return self.__setup.get( _key, False );


	##
	## Returns whether or not this Property / Accessor is a Table type ( Enables Pop functionality, and others...
	##
	def IsTableProperty( self ):
		##
		_allowed_types = self.GetAllowedTypes( );

		## If we allow all data-types, only lists, dicts or tuples then enable pop and other table-specific helpers...
		return ( _allowed_types == None or _allowed_types.get( [ ], False ) or _allowed_types.get( { }, False ) or _allowed_types.get( ( ), False ) );


	##
	##
	##
	def Pop( self, _parent = None, _index = None, _count = 1 ):
		## Use the GetRaw new Property system for aliases to shorten the code here...
		_get = self.Get( _parent )
		_isset = self.GetRaw( _parent ) != None;
		if ( _isset and IsDict( _get ) or IsTableTypeSet( _get ) or IsList( _get ) or IsTuple( _get ) ):
			return _get.pop( ( _index, len( _get ) - 1 )[ _index == None ] )


	##
	## Helper - Returns whether or not a value can be set based on data-type and / or value restrictions, and checks to see if the parent class has a CanSet method for this key to act as an override which also gets a say, if it exists...
	##
	def CanSet( self, _parent = None, _value = None ):
		## Determine if the value being assigned is valid to use - if not we assign the current value, otherwise we assign the new value...
		_valid_data = self.IsValidData( _value );

		## Determine if the Parent Class Instance has a CanSet Callback / Override function - If it does, call it...
		if ( hasattr( _parent, 'CanSet' ) and callable( getattr( _parent, 'CanSet' ) ) ):
			pass;
			## If the CanSet returns a non-boolean, use _valid_data... If boolean, then use that - also make sure the callback has the valid data arg so they can see if the data fails or passes the valid data-type / value system...

		## Alternative - use CanSet<Key>
		if ( hasattr( _parent, 'CanSet' + self.Key( ) ) and callable( getattr( _parent, 'CanSet' + self.Key( ) ) ) ):
			pass;

		return _valid_data;


	##
	## Resets the stored value to None...
	##
	def Reset( self, _parent = None ):
		return setattr( _parent, self.GetDataKey( ), None );
		## return self.Set( _parent, None ); # , self.Key( );


	##
	## Called to Set the value
	##
	## def Set( self, _parent = None, _value = None ):
	def Set( self, _parent = None, _value = None ):
		## If parent isn't provided, we can't do anything other tham providing static information..
		if ( _parent == None ):
			## On Set Fail, if we have an on_fail_set key in our setup table and it is callable, then call it...
			self.ProcessCallback( _parent, 'on_set_fail', '_parent is undefined; can\'t retrieve current value!', None, _value, False );

			##
			## print( 'AccessorFunc -> Set -> Value NOT UPDATED because of _parent being not set for key: ' + self.Key( ) + ' to ' + str( _value ) + ' and it remains ' + str( _current ) );


			## print( 'AccessorFuncBase -> Set -> _parent == None' );
			return self;

		##Grab the current value, in case we don't update the value - we need to assign the current value otherwise None will be assigned...
		_current = getattr( _parent, self.GetDataKey( ) );

		## The value and data-type checks determine if _value is an authorized data-type or value - If is is not a valid data-type, and it is not a valid value, then we prevent the value from being set...
		if ( not self.CanSet( _parent, _value ) ):
			## Update the value or define it so that it exists...
			setattr( _parent, self.GetDataKey( ), _current );

			##
			## print( 'AccessorFuncBase -> Set -> !CanSet' );
			## print( 'AccessorFuncBase -> Set -> CanSet -> ProcessCallback using "on_set", ' + str( _parent ) + ', '  + str( _current ) + ', '  + str( _value ) + ', '  + str( True ) + '!' );

			## On Set Fail, if we have an on_fail_set key in our setup table and it is callable, then call it...
			self.ProcessCallback( _parent, 'on_set_fail', 'CanSet Callback prevented the assignment!', _current, _value, False );


			##
			## print( 'AccessorFunc -> Set -> Value NOT UPDATED for key: ' + self.Key( ) + ' to ' + str( _value ) + ' and it remains ' + str( _current ) );

			## if ( callable( self.Setup( 'on_set_fail' ) ) ):
			## 	self.Setup( 'on_set_fail' )( _current, _current, False );

			## Return the current value so we don't reset the variable - we only want it to be set to something else if the developer says so - setting it to an incorrect value doesn't mean reset it..
			return _current;

		## Update the value or define it so that it exists...
		setattr( _parent, self.GetDataKey( ), _value );

		##
		## print( 'AccessorFunc -> Set -> Updated Value for key: ' + self.Key( ) + ' to ' + str( _value ) );

		##
		## print( 'AccessorFuncBase -> Set -> CanSet' );
		## print( 'AccessorFuncBase -> Set -> CanSet -> ProcessCallback using "on_set", ' + str( _parent ) + ', '  + str( _current ) + ', '  + str( _value ) + ', '  + str( True ) + '!' );

		## On Set, if we have an on_set key in our setup table and it is callable, then call it...
		self.ProcessCallback( _parent, 'on_set', _current, _value, True );

		## if ( callable( self.Setup( 'on_set' ) ) ):
		## 	self.Setup( 'on_set' )( _current, _value, True );

		## Return the value - we are a setter afterall...
		return _value;


	##
	## Retrieves the value from the parent class._<key>, or, if the data is None, then it'll return the Default value
	##
	## Fix: For some reason, when _default is callable, it returns the function object instead of the return value which isn't wanted behavior and it isn't coded that way unless something else is taking affect or I missed something... Review...
	## def Get( self, _parent = None ):
	def Get( self, _parent = None ):
		##
		_default = self.DefaultValue( );

		## Use the GetRaw new Property system for aliases to shorten the code here...
		_base = self.GetRaw( _parent );

		##
		## print( 'AccessorFunc -> Get -> Trying to retrieve data for key: ' + self.Key( ) + ' in ' + str( self.Name( ) ) );

		## If _base isn't None, it means we have a viable value we can use
		if ( _base != None ):
			## print( 'Return Getter _base for ' + self.Name( ) );
			return _base;

		## If parent is or isn't provided and / or the raw value isn't defined - we need to return a default value. If the default value is set as a function, we call it... otherwise we return it..
		if ( _default != None and callable( _default ) ):
			## _data = _default( self, _parent );
			_data = _default( _parent );

			## print( 'Return Callable Getter for ' + self.Name( ) );

			## if ( self.CanSet( _parent, _data ) ):
			return _data;

		## print( 'Return Getter _default for ' + self.Name( ) );
		return _default;

		## return _base if ( _base != None ) else self.DefaultValue( );


	##
	## Called to Add the value
	##
	def Add( self, _parent = None, *_values ): #, _value = None
		## if ( _value != None ):
		## 	self.Set( _parent, self.Get( _parent ) + _value );

		##
		_data = self.Get( _parent );
		_type = type( self.Get( _parent ) );

		##
		if ( len( _values ) > 0 ):
			## print( 'Determining if AccessorFuncBase.Add is adding to a list or tuple: ' + str( type( _data ) is list or type( _data ) is tuple ) + ' or ' + str( isinstance( _data, list ) or isinstance( _data, tuple ) ) );
			for _value in _values:
				##
				if ( isinstance( _data, list ) or isinstance( _data, tuple ) ):
					_data = _data.append( _value );
				else:
					_data = _data + _value;

			##
			self.Set( _parent, _data );


	##
	## Called on deletion..
	##
	def Del( self, _parent = None ):
		pass;


	##
	## Retrieves the value from the parent class._<key>
	##
	def GetRaw( self, _parent = None ):
		## If paren't is set, then we check to see if a value is defined... in the Raw data location...
		if ( _parent != None ):
			_base = getattr( _parent, self.GetDataKey( ) );

			## Task: Future: If we allow raw value pass-through, then allow a raw stored function reference to be called - need to add *_args ( variable arguments list ) or **_varargs ( keys variable arguments list ) - add a controlled toggle...
			## if ( callable( _base ) ):
			## 	return _base( self, _parent );

			## Return the value, if set..
			if ( _base != None ):
				return _base;

		##
		return None;


	##
	## Called on deletion..
	##
	def DelRaw( self, _parent = None ):
		pass;


	##
	## Returns the Getter Prefix
	##
	def GetterPrefix( self, _prefix_override = None ):
		return ( ( '', self.__prefix_get )[ self.HasGetterPrefix( ) ], _prefix_override )[ _prefix_override != None ];


	##
	## Returns the key
	##
	def HasGetterPrefix( self ):
		return type( self.__prefix_get ) is str # self.GetterPrefix( ) != None and self.GetterPrefix( ) != '';


	##
	## Returns the Class Name this Accessor is registered to...
	##
	def ClassName( self, _name_override = None ):
		return ( self.__class_name, _name_override )[ _name_override != None ];


	##
	## Returns the key
	##
	def Name( self, _name_override = None ):
		return ( self.__name, _name_override )[ _name_override != None ];


	##
	## Returns the key
	##
	def Key( self, _key = None ):
		return ( ( _key, self.__key )[ _key == None ] );


	##
	## Returns the key
	##
	def DataKey( self, _key = None ):## Return the value - we are a getter afterall...
		return ( '_' + ( _key, self.__key )[ _key == None ] );


	##
	## Returns the key
	##
	def AccessorKey( self, _key = None ):## Return the value - we are a getter afterall...
		return ( '__' + ( _key, self.__key )[ _key == None ] );


	##
	## Alias of Key - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetKey( self, _key = None ):
		return self.Key( _key );


	##
	## Alias of DataKey - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetDataKey( self, _key = None ):
		return self.DataKey( _key );


	##
	## Alias of AccessorKey - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetAccessorKey( self, _key = None ):
		return self.AccessorKey( _key );


	##
	## Attempt to find an existent key through the use of the AccessorFunc Key, Name and LowerCase Name...
	##
	def GenerateKey( self, _parent = None, _prefix = '' ):
		## Try the default key
		_key = getattr( this, _parent + self.Key( ), None );

		## If the default key isn't there, try using the AccessorFunc Name as a key...
		_key = getattr( this, _parent + self.Name( ) ) if _key == None else _key;

		## If the AccessorFunc Name isn't assigned, try using the AccessorFunc Name as a lower-case string...
		_key = getattr( this, _parent + self.Name( ).lower( ) ) if _key == None else _key;

		## Returns either None, or the _key which exists...
		return _key;


	##
	## Attempt to find an existent Data Key
	##
	def GenerateDataKey( self, _parent = None ):
		return self.GenerateKey( _parent, '_' );


	##
	## Attempt to find an existent Accessor / Property Key
	##
	def GenerateAccessorKey( self, _parent = None ):
		return self.GenerateKey( _parent, '__' );


	##
	## Returns the default value
	##
	def DefaultValue( self ):
		return self.__default;


	##
	## Returns the documentation for this Accessor
	##
	def Documentation( self ):
		return self.__documentation;


	##
	## Determines, and Returns both Human-Friendly / Readable Outputs for Allowed Data-Types and Values...
	##
	def GetAllowedDesc( self ):
		_allowed_types			= self.GetBaseAllowedTypes( );
		_allowed_values			= self.GetBaseAllowedValues( );

		##
		_status_allowed_types	= ( _allowed_types, 'Unrestricted' )[ _allowed_types == None or _allowed_types == (None,) ];
		_status_allowed_values	= ( _allowed_values, 'Unrestricted' )[ _allowed_values == None or _allowed_values == (None,) ];

		##
		_output_allowed_types	= ( _status_allowed_types, 'Saved Value Restricted to Authorized Values ONLY' )[ _status_allowed_types == 'Unrestricted' and _status_allowed_values != 'Unrestricted' ];
		_output_allowed_values	= ( _status_allowed_values, 'Saved Value Restrictions Levied by Data-Type' )[ _status_allowed_types != 'Unrestricted' and _status_allowed_values == 'Unrestricted' ];

		return _output_allowed_types, _output_allowed_values;


	##
	## Returns the Allowed Types which the user / dev supplied on creation for this Accessor...
	##
	def GetBaseAllowedTypes( self ):
		return self.__base_allowed_types;


	##
	## Returns the Human-Friendly Version of Allowed Types for this Accessor...
	##
	def GetDescAllowedTypes( self ):
		_allowed_types, _allowed_values = self.GetAllowedDesc( );
		return _allowed_types;


	##
	## Returns None if there are no restrictions, or a Dict using O( 1 ) _dict.get( _x, False ) == True to determine whether or not the _type entered is allowed..
	##
	def GetAllowedTypes( self ):
		return self.__allowed_types;


	##
	## Returns the Allowed Values which the user / dev supplied on creation for this Accessor...
	##
	def GetBaseAllowedValues( self ):
		return self.__base_allowed_values;


	##
	## Returns the Human-Friendly Version of Allowed Values for this Accessor...
	##
	def GetDescAllowedValues( self ):
		_allowed_types, _allowed_values = self.GetAllowedDesc( );
		return _allowed_values;


	##
	## Returns None if there are no restrictions, or a Dict using O( 1 ) _dict.get( _x, False ) == True to determine whether or not the _value entered is allowed..
	##
	def GetAllowedValues( self ):
		return self.__allowed_values;


	##
	## Whether or not to display created accessors in log...
	##
	def ShowCreatedAccessors( self ):
		return self.__display_accessors;


	##
	## Returns whether or not the Data Type provided is authorized to be used
	##
	def IsValidDataType( self, _type = None ):
		## Helper
		_types = self.GetAllowedTypes( )

		## If we don't need to check, return true..
		if ( _types == None ):
			return True;

		##
		return ( _types == _type or _types.get( str( _type ), False ) != False );


	##
	## Returns whether or not the Data Value provided is authorized to be used
	##
	def IsValidDataValue( self, _value = None ):
		## Helper
		_values = self.GetAllowedValues( );

		## If we don't need to check, return true..
		if ( _values == None ):
			return True;

		##
		return ( _values == _value or _values.get( str( _value ), False ) == str( type( _value ) ) );


	##
	## Returns whether the data is valid based on data-type and value rules set in place.
	##
	def IsValidData( self, _value = None ):
		## Make sure if we only accept certain values without defining types, then IsValid can only return true if the value is amonst the allowed...
		## If we allow types, and no values, we need to make sure that only those types are valid...
		_type			= type( _value );

		## Helpers
		_types			= self.GetAllowedTypes( );
		_values			= self.GetAllowedValues( );

		## None is always True
		if ( _value == None ):
			return True;

		## If both types and values are None, return true..
		if ( _types == None and _values == None ):
			return True;

		##
		_valid_type		= self.IsValidDataType( _type );
		_valid_value	= self.IsValidDataValue( _value );

		## If only one or the other exists - make sure it is valid...
		if ( _values == None and _types != None ):
			## print( 'Comparing using: Data-Type' )
			return ( False, True )[ _valid_type ];
		elif ( _values != None and _types == None ):
			## print( 'Comparing using: Value' )
			return ( False, True )[ _valid_value ];

		## print( 'Comparing using: Data-Type and Value -- However, since we have both, we can allow Value to be a definitive ALLOW ALL OF THESE... For Type, we may want to restrict so it is either value or value and type' )
		## print( '_valid_type: ', _valid_type );
		## print( '_types: ', _types );
		## print( '_valid_value: ', _valid_value );
		## print( '_values: ', _values );
		## print( 'Value Type: ', str( type( _value ) ) );


		## If both are defined, make sure they both exist... -- If you define a value to be ok, then we must allow it.
		## So either both are ok, or just the value is ok. If no value restrictions are in place, value will return True.
		## Note: I may change this to OR - because if you add 1 value ( True ) and allow types BOOLEAN. And try to set False, then value will return false..
		## Note: So, either.. Value is complete control - or not. Although, since they return True when None... we need to come up with alternate logic.
		return ( False, True )[ _valid_type and _valid_value or _valid_value ];


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupAllowedDict( self, _data = None ):
		## If we have no data, return None...
		if ( _data == None ):
			return None;

		## Helper
		_type = type( _data );

		## If the data isn't None, we need to determine what it is - single element or table... So if not Dict, Tuple, List then we only add 1 object to Dict and return - else we go through each element..
		if ( _type != type( { } ) and _type != type( ( ) ) and _type != type( [ ] ) ):
			return { str( _data ): str( _type ) };

		## Now, we have established it is a Dict, Tuple or List, process all of the values into keys = True...
		_dict = { }

		## For each element, process it...
		for _value in _data:
			_dict[ str( _value ) ] = str( type( _value ) );

		## return the Dict..
		return _dict;


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def SetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _value ):
			## Debugging..
			if ( _by_name ):
				## print( 'SetAccessorFuncValues', 'Executing: Set' + str( _name ) + '( ' + str( _value ) + ' )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...setattr( this, self.GetKey( ), _value ) #
				return getattr( this, 'Set' + _name )( _value );
			else:
				## print( 'SetAccessorFuncValues', 'Executing: this.' + str( _name ) + '( ' + str( _value ) + ' )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return setattr( this, _name, _value );

		## Debugging..self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'SetAccessorFuncValues', 'Executing: SetAccessorFuncValues( ' + String.ToStringKeyedVarArgs( *_args ) + ' ) :: By Name: ' + str( _by_name ) + ' :: By Key: ' + str( not _by_name ) )

		## Execute...
		return Util.ProcessKeyVarArgs( self, Callback, *_args );


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def ResetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name ):
			## Debugging..
			if ( _by_name ):
				## print( 'ResetAccessorFuncValues', 'Executing: Reset' + _name + '( )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values... setattr( this, self.GetDataKey( ), None ) #
				return getattr( this, 'Reset' + _name )( );
			else:
				## print( 'ResetAccessorFuncValues', 'Executing: this.' + _name + ' = None' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return setattr( this, _name, None )( );

		## Debugging.. self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'ResetAccessorFuncValues', 'Executing: Reset( ' + String.ToString( *_args ) + ' ) :: By Name: ' + str( _by_name ) )

		## Execute...
		return Util.ProcessSimpleVarArgs( self, Callback, *_args );


	##
	## Helper - Extra Functionality for getting all AccessorFunc Names provided to the Values provided..
	##
	def GetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		##
		_data = [ ];

		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _data ):
				## Debugging..
			if ( _by_name ):
				## print( 'GetAccessorFuncValues', 'Executing: return Get' + _name + '( )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...getattr( this, self.Key( ) ) #
				return getattr( this, 'Get' + _name )( );
			else:
				## print( 'GetAccessorFuncValues', 'Executing: return this.' + _name + '' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return getattr( this, _name );

		## Debugging.. self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'GetAccessorFuncValues', 'Executing: Get( ' + String.ToString( *_args ) + ' ) :: By Name: ' + str( _by_name ) )

		## Execute...
		return Util.ProcessAccumulateVarArgs( self, Callback, _data, *_args );


	##
	## Returns the Ca
	##




	##
	## Registers the Accessor Function with a class..
	## Task: Add the Key / Name check along with Setup instructions to allow bypassing registry if setup is instructed to ignore... or if setup defines a callable, then that is used as the function instead of the one supplied...
	##
	## Task: Read the new setup table system which includes the active_on label to determine whether or not to activate... Also, allow named tags, as they currently are, or enumerated ids for the setup table... Also, read the callbacks from the setup table this way.. Maybe setup enumeration values for them? OR simply multiply by 10000 for post, -10000 for pre, and 1000 for on? But that limits us to 1000 functions or so.. A bit high ( functions PER accessor, not total functions ) but I'd rather not limit us that way... It's likely be better to associate each tag with a pre, post, on, active, active_on tags... Also, since the rest is defined using the mapping system very easily, there should be no issue using the mapping system to pre-define defaults and active_on states and others... then we can focus on callback info and the like for setup systems?
	##
	##
	##
	## self is this Accessor
	## _parent is the class the function is assigned to, this can also be self...
	## _id is the quick / simple identifier for the function... Something simple such as get, getstr, getlen, getlenstr, etc... Simle designations... This will be converted to Enumeration Soon.
	## _name is the function name which will be used...
	## _func is the default function to provide - if the _id is recognized as callable under Setup, then we use that instead of creating more args...
	##
	def RegisterAccessor( self, _parent, _id, _func, _is_callback = False ):
		## The numerical ID in string form of the enumerator
		_enum_id		= _id;

		## print( '\tenum_value:\t\t' + str( _enum_id ) );
		## print( '\tid:\t\t\t\t' + str( _id ) );
		## print( '\tMAP_ACCESSORFUNC_IDS:\t\t\t\t' + str( MAP_ACCESSORFUNC_IDS ) );

		## The text identifier such as get, get_raw, etc... backwards compatibility until replaced...
		_id				= MAP_ACCESSORFUNC_IDS[ _enum_id ];

		## ID Override - All Instance Functions are lopped in as a single item...
		_id				= ( _id, 'instance' )[ _id.startswith( 'instance_' ) ];

		##
		_setup			= self.Setup( _id );
		_is_callback	= ( _is_callback or _id.startswith( 'on_' ) or _id.startswith( 'pre_' ) );

		##
		if ( _setup == False ):
			## Only show the text if this isn't a callback
			## if ( not _is_callback ):
			## print( '[ Acecool Library -> AccessorFuncBase -> RegisterAccessor ] Setup Instructions are preventing this Accessor from being added.. ' + String.FormatColumn( 5, 'id:', 50, _id, 10, 'Name:', 100, _func_call ) );

			return False;

		##
		if ( callable( _setup ) ):
			## print( '[ Acecool Library -> AccessorFuncBase -> RegisterAccessor ] Setup Instructions are overriding this Accessor function entirely.. ' + String.FormatColumn( 5, 'id:', 50, _id, 10, 'Name:', 100, _name ) );
			_func = _setup;


		##
		## Preconfigure the namine system so we fill out all of the blanks, but we keep name set to something like {alias_name_option} or something that won't be replaced, until we get into the function...
		##

		## The dynamic values of the function name, etc.. such as Get and Name, etc..
		_func_vars = {
			'get':				self.GetterPrefix( ),
			'name':				'{name_alias_system}',
			'key':				self.Key( ),
			'key_data':			self.DataKey( ),
			'key_accessor':		self.AccessorKey( ),
		};


		## print( 'RegisterAccessor _func_vars: ', _func_vars );

		## The Arguments String
		_func_args		= MAP_ACCESSORFUNC_ID_FUNC_ARGS[ _enum_id ];

		## The function name without this/self. or parens and args
		_func_name		= String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _enum_id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );

		## The function parent usage... this for instance, self for the property / accessor, ie AccessorFuncBase object...
		_func_this		= String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE[ _enum_id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );

		## These vars are to assemble the final product - the callable function name for the current function being registered...
		_func_vars = {
			## Word Parent is self or this to reflect part of the property or the instance / class the property is added to
			'word_parent':	_func_this,

			## Map Name is the raw name without self/this. and args / parens.
			'map_name':		_func_name,
			'map_args':		_func_args,

			## Argument / Parenthesis Spaces - left always has 1, and if there are arguments then the right has one but if there are no args then there is no extra space which complies with my coding standard.
			'arg_spacer_l':	' ',
			'arg_spacer_r':	( '', ' ' )[ len( _func_args ) > 0 ],
		};

		## The function name as it would be used to be called...
		_func_call		= String.ExpandVariables( '{word_parent}.{map_name}({arg_spacer_l}{map_args}{arg_spacer_r});', MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );


		## ## _func.__name__ = _name;
		## _func.__name__ =_func_name;
		## ## setattr( _func, '__name__', _name );
		## setattr( _parent, _func.__name__, _func );

		## ## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
		## ## ## Automatically add the callbacks...
		## if ( not _id == 'instance' and not _is_callback ):
		## 	_setup = self.Setup( 'on_' + _id );
		## 	if ( _setup != False ):
		## 		_func.__name__ = 'On' + _func_name;
		## 		setattr( _parent, _func.__name__, _setup );

		## 	_setup = self.Setup( 'pre_' + _id );
		## 	if ( _setup != False ):
		## 		_func.__name__ = 'Pre' + _func_name;
		## 		setattr( _parent, _func.__name__, _setup );

		## ##
		## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
		## 	##
		## 	print( '\nRegisterAccessor -> RegisterNamedAccessor id: ' + Text.FormatColumn( 30, _id, 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );

		##
		## Register AccessorFunc Names
		##
		## @arg: _alias: This is the name, such as Blah
		## @arg: _name: This is the Is{name_alias_system}Set or so - everything is filled out except for the name...
		## @arg: _call: This is basically this.{name_alias_system}( _args_list ); - everything is filled out except for the name...
		##
		def RegisterNamedAccessor( _enum_id, _id, _alias, _prep_name, _prep_call ):
			##
			_this_name			= self.Name( );
			_usable_name		= String.ExpandVariables( _prep_name, { 'name_alias_system': _alias } );
			_usable_call		= String.ExpandVariables( _prep_call, { 'name_alias_system': _alias } );

			##
			## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			_is_name_original	= ( _this_name == _alias or _usable_name == _this_name );
			_is_name_instance	= ( _id.startswith( 'instance' ) );
			_is_name_alias		= ( not _is_name_instance and not _is_name_original );
			_func_type			= ( ( 'Alias', 'Instance' )[ _is_name_instance ], 'Original' )[ _is_name_original ];

				## ##
				## ## ( '\n' if _is_name_original else '' ) +

				## ##
				## ## print( 'RegisterAccessor -> RegisterNamedAccessor for class: [ ' + Text.FormatColumn( 20, self.ClassName( ) ) + ' for: ' + Text.FormatColumn( 20, _this_name ) + ' ] -> ' + Text.FormatColumn( 20, _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );

				## ##
				## if ( _func_type == 'Alias' and Acecool.GetSetting( 'output_accessorfunc_register_info_alias', False ) or _func_type == 'Instance' and Acecool.GetSetting( 'output_accessorfunc_register_info_instance', False ) or _func_type == 'Original' and Acecool.GetSetting( 'output_accessorfunc_register_info_original', False ) ):
				## 	print( '\t' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


			## Old
			## _func.__name__ = _name;
			## setattr( _func, '__name__', _name );

			## Register the function...
			self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
			self.RegisterFunction( _parent, _usable_name, _func );

			## ##
			## if ( not getattr( _parent, 'accessor_func_db', None ) ):
			## 	##
			## 	## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
			## 	##
			## 	setattr( _parent, 'accessor_func_db', {
			## 		## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
			## 		## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
			## 		'groups': {
			## 			## Default group - this is for EVERYTHING...
			## 			'*': [ ]

			## 			## 4 28 44 85
			## 			## 'All': {
			## 			##		'Pitch': True,
			## 			##		'Yaw': True,
			## 			##		'Roll': True,
			## 			## },
			## 		},

			## 		## a list of keys which also link to the accessor func, property, names?
			## 		'keys': {
			## 			## 'roll': { },
			## 			## 'r': { },
			## 		},

			## 		## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
			## 		'names': {
			## 			## 'Roll': { },
			## 			## 'R': { },
			## 		},

			## 		## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
			## 		'call_names': {
			## 			## 'GetRollAccessor': '',
			## 			## 'GetRollAllowedTypes': '',
			## 			## 'GetRoll': ''
			## 		},

			## 		## Accessors list is a simple list of all accessor objects
			## 		'accessors': [

			## 		]

			## 		##
			## 		## '': [],
			## 		## '': [],
			## 		## '': []
			## 	}
			## );


			##
			_accessor_db		= _parent.accessor_func_db;
			_db_groups			= _parent.accessor_func_db.get( 'groups', { } );
			_db_names			= _parent.accessor_func_db.get( 'names', { } );
			_db_func_names		= _parent.accessor_func_db.get( 'call_names', { } );
			_db_keys			= _parent.accessor_func_db.get( 'keys', { } );
			## _db_references		= _parent.accessor_func_db.get( 'references', { } );
			_db					= _parent.accessor_func_db.get( 'groups', { } );
			_db_groups_all		= _db_groups.get( '*', { } );

			## _ident = {
			## 	'accessor':		self,
			## 	'args':			_func_args,
			## 	'call_name':	_usable_call
			## };

			## _db_names[ self.Name( ) ] = self;
			## _db_func_names[ _usable_name ] = self.Name( );
			if ( not _db_func_names.get( self.Name( ), None ) ):
				_db_func_names[ self.Name( ) ] = { };

			_db_func_names[ self.Name( ) ][ _usable_name ] = {
				'is_original':	_is_name_original,
				'is_instance':	_is_name_instance,
				'is_alias':		_is_name_alias,

				'args':			_func_args,

			};
			_db_groups_all[ self.Name( ) ] = True;
			## _db_groups_all.append( self );



			## _db_names[ self.Key( ) ] = self;
			## _db_func_names[ _usable_name ] = self.Key( );
			if ( not _db_keys.get( self.Key( ), None ) ):
				_db_keys[ self.Key( ) ] = { };

			_db_keys[ self.Key( ) ][ self.Key( ) ] = {
				'is_original':	_is_name_original,
				'is_instance':	_is_name_instance,
				'is_alias':		_is_name_alias,

				## 'args':			_func_args,

			};
			## _db_keys[ self.Key( ) ] = [ ];
			## _db_keys[ self.Key( ) ] = self;
			if ( self.__keys ):
				for _key in self.__keys:
					## _db_keys[ _key ] = self;
					## _db_keys[ self.Key( ) ].append( _key );
					_db_keys[ self.Key( ) ][_key ] = {
						'is_original':	_is_name_original,
						'is_instance':	_is_name_instance,
						'is_alias':		_is_name_alias,

						## 'args':			_func_args,

					};


			## _db_groups_all[ self.Name( ) ] = True;
			## _db_groups_all.append( self );

			## _db_keys[ self.Key( ) ] = [ ];
			## ## _db_keys[ self.Key( ) ] = self;
			## if ( self.__keys ):
			## 	for _key in self.__keys:
			## 		## _db_keys[ _key ] = self;
			## 		_db_keys[ self.Key( ) ].append( _key );

			## _db_names[ self.Name( ) ] = [ ];
			## ## _db_names[ self.Name( ) ] = self;
			## if ( self.__names ):
			## 	for _name in self.__names:
			## 		## _db_names[ _name ] = self;
			## 		_db_names[ self.Name( ) ].append( _name );


			## Newest old version
			## _func.__name__ =_usable_name;
			## setattr( _parent, _func.__name__, _func );

			## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
			## ## Automatically add the callbacks...
			if ( not _id == 'instance' and not _is_callback ):
				_setup = self.Setup( 'on_' + _id );
				if ( _setup != False ):
					self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
					self.RegisterFunction( _parent, 'On' + _usable_name, _setup );
					_db_groups_all[ 'On' + _usable_name ] = self;
					## _func.__name__ = 'On' + _usable_name;
					## setattr( _parent, _func.__name__, _setup );



				_setup = self.Setup( 'pre_' + _id );
				if ( _setup != False ):
					self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
					self.RegisterFunction( _parent, 'Pre' + _usable_name, _setup );
					_db_groups_all[ 'Pre' + _usable_name ] = self;
					## _func.__name__ = 'Pre' + _usable_name;
					## setattr( _parent, _func.__name__, _setup );




		##
		_is_instance_helper = ( _func_name.find( '{' ) > -1 );
		_actual_name 		= ( _func_name, self.Name( ) )[ _is_instance_helper ];

		## Register the primary function name - For the output, if we have unprocessed {x} tags, then we use self.Name( ) as the replacer.. Otherwise, if no {x} tags are left, then it is a nameless / instance helper which doesn't have a unique name for each registered name...
		## print( 'RegisterAccessor -> Registering Primary Name: ' + _get_actual_name );
		RegisterNamedAccessor( _enum_id, _id, _actual_name, _func_name, _func_call );

		## Grab the list of Alias Names
		## _names = self.GetNameAliases( );
		_names = self.__names;
		## _names.append( self.Name( ) );
		##
		if ( _names != None and len( _names ) > 0 ):
			for _name in _names:
				RegisterNamedAccessor( _enum_id, _id, _name, _func_name, _func_call );










		## {
		##    'groups':{
		##       '*':{
		##          '':True,
		##          'Serialize':True,
		##          'X':True,
		##          'Y':True,
		##          'Z':True,
		##          'Pitch':True,
		##          'Yaw':True,
		##          'Roll':True,
		##          'Normalize':True,
		##          'Normalized':True
		##       }
		##    },
		##    'keys':{
		##       'this_object':{
		##          'this_object':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'serialize':{
		##          'serialize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'x':{
		##          'x':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'y':{
		##          'y':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'yaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'z':{
		##          'z':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'p':{
		##          'p':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'pitch':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'r':{
		##          'r':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'roll':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'normalize':{
		##          'normalize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'normalized':{
		##          'normalized':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       }
		##    },
		##    'names':{

		##    },
		##    'call_names':{
		##       '':{
		##          'Get':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'Set':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Serialize':{
		##          'Serialize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'SerializeToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'SerializeAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          }
		##       },
		##       'X':{
		##          'GetX':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetXToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetXAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetX':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Y':{
		##          'GetY':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetYAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetY':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Z':{
		##          'GetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsZSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetZLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetZProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasZGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'ResetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          }
		##       },
		##       'Pitch':{
		##          'GetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsPitchSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsPSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetPToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetPitchLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetPDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetPitchProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasPitchGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasPGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Yaw':{
		##          'GetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsYawSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsYSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetYToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetYawLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetYDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetYawProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasYawGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasYGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Roll':{
		##          'GetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsRollSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsRSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetRToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetRollLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetRDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetRollProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasRollGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasRGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetROutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Normalize':{
		##          'Normalize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          }
		##       },
		##       'Normalized':{
		##          'GetNormalized':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          }
		##       }
		##    }
		## }
























	##
	##
	##
	def RegisterFunction( self, _parent, _name, _func ):
		_func.__name__ =_name;
		setattr( _parent, _func.__name__, _func );


	##
	##
	##
	def RegisterFunctionNotify( self, _parent, _func_type, _usable_name, _usable_call ):
		if ( not getattr( _parent, _usable_name, None ) ):
			##
			if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
				##
				if ( _func_type == 'Alias' and Acecool.GetSetting( 'output_accessorfunc_register_info_alias', False ) or _func_type == 'Instance' and Acecool.GetSetting( 'output_accessorfunc_register_info_instance', False ) or _func_type == 'Original' and Acecool.GetSetting( 'output_accessorfunc_register_info_original', False ) ):
					print( '\t' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );
		## else:
		## 	print( '\tError trying to register function - ALREADY REGISTERED: ' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


		## ##
		## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
		## 	##
		## 	print( 'RegisterAccessor -> RegisterXAccessor for class: [ ' + Text.FormatColumn( 20, self.ClassName( ) ) );
		## 	## print( '\t' + Text.FormatColumn( 20, _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


		## ##
		## for _i in range( 1 + len( _names ) ):
		## 	## We use index - 1 as the index so it'll be -1 through len( _names ) - 1;
		## 	## _index = _i - 1;

		## 	## This grabs the name, but it won't allow it to go below 0 or out of bounds...
		## 	## _name = _names[ Math.Clamp( _index, 0, len( _names ) - 1 ) ];
		## 	_name = _names[ _i ];

		## 	## ## If we're at the first, we add the primary name... If we aren't, name is already set...
		## 	## if ( _i == 0 ):
		## 	## 	_name = self.Name( );

		## _usable_name
		## 	##
		## 	print( 'RegisterNamedAccessor -> Alias Solutions - Pre: ' + _func_name + ' || ' + _func_call );

		## 	##
		## 	_func_name = String.ExpandVariables( _func_name, { 'name_alias_system': _name } );
		## 	_func_call = String.ExpandVariables( _func_call, { 'name_alias_system': _name } );

		## 	##
		## 	print( 'RegisterNamedAccessor -> Alias Solutions: ' + _func_name + ' || ' + _func_call );

		## 	## _func.__name__ = _name;
		## 	_func.__name__ =_func_name;
		## 	## setattr( _func, '__name__', _name );
		## 	setattr( _parent, _func.__name__, _func );

		## 	## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
		## 	## ## Automatically add the callbacks...
		## 	if ( not _id == 'instance' and not _is_callback ):
		## 		_setup = self.Setup( 'on_' + _id );
		## 		if ( _setup != False ):
		## 			_func.__name__ = 'On' + _func_name;
		## 			setattr( _parent, _func.__name__, _setup );

		## 		_setup = self.Setup( 'pre_' + _id );
		## 		if ( _setup != False ):
		## 			_func.__name__ = 'Pre' + _func_name;
		## 			setattr( _parent, _func.__name__, _setup );


	## ##
	## ##
	## ##
	## class AccessorFuncBase( AccessorFuncCore ):
	## 	##
	## 	## Helpers
	## 	##
	## 	__name__ = 'AccessorFuncBase';


	## 	##
	## 	__name_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',		keys = None,	name = 'NameAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
	## 		'get': lambda this: this.__names,
	## 		'set': lambda this, _value: setattr( this, '__names', _value )
	## 	} );
	## 	__key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',		keys = None,	name = 'KeyAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
	## 		'get': lambda this: this.__keys,
	## 		'set': lambda this, _value: setattr( this, '__keys', _value )
	## 	} );


	## 	##
	## 	##
	## 	##
	## 	def __init__( self, **_varargs ):
	## 		super( ).__init__( **_varargs );

	## 	## 																																																																																								( lambda this, _version, _minor, _revision: this.__init__( _version, _minor, _revision ) )
	## 	## __name_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',						name = 'NameAliases',						default = [ ],			group = '',		getter_prefix = 'Get',		documentation = 'Name Aliases',			allowed_types = TYPE_ANY,			allowed_values = VALUE_ANY,		setup = { 'get': ( lambda this: this.__name_aliases ), 'set': ( lambda this, _value: this.__name_aliases = _value ) }		);
	## 	## __key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',						name = 'KeyAliases',						default = [ ],			group = '',		getter_prefix = 'Get',		documentation = 'Key Aliases',			allowed_types = TYPE_ANY,			allowed_values = VALUE_ANY,		setup = { 'get': ( lambda this: this.__key_aliases ), 'set': ( lambda this, _value: this.__key_aliases = _value ) }		);
	## 	## __OutputViewID			= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'OutputViewID',						name = 'OutputViewID',			default = None,				group = '',			getter_prefix = 'Get',		documentation = 'OutputViewID',			allowed_types = ( TYPE_INTEGER ),	allowed_values = VALUE_ANY													);



	## 		## ## For each key alias, we simply
	## 		## if ( _names != None and IsTable( _names ) ):
	## 		## 	## For each alias, set up the raw / alias property for _<alias> so it is redirected to _<key>, also set up the standard property for the <alias> - Determine how to set up __<alias> without having the private system kick in...
	## 		## 	for _name in _names:
	## 		## 		RegisterNamedAccessor( _name, _func_name, _func_call );

	## 		##
	## 		## print( 'RegisterAccessor:\t\t\t' + _func_call )




	## 			## self.RegisterAccessor( _parent, 'on_' + _id, _func, True );
	## 			## self.RegisterAccessor( _parent, 'pre_' + _id, _func, True );



	## 		## print( 'RegisterAccessor called' );
	## 		## print( '\tenum_value:\t\t' + str( _enum_id ) );
	## 		## print( '\tid:\t\t\t\t' + _id );
	## 		## print( '\tfunc_raw:\t\t' + MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _enum_id ] );

	## 		## print( '\tself/this:\t\t' + str( _func_this ) );
	## 		## print( '\tname:\t\t\t' + _func_name );
	## 		## print( '\targs:\t\t\t' + _func_args );
	## 		## print( '\tcall:\t\t\t' + _func_call );
	## 		## print( );


	## 		## def Setup( _id, _func = None ):
	## 		## 	print( 'Setup called' );
	## 		## 	print( '\tenum_value:\t\t' + str( _id ) );
	## 		## 	print( '\tid:\t\t\t\t' + str( MAP_ACCESSORFUNC_IDS[ _id ] ) );
	## 		## 	print( '\tfunc_raw:\t\t\t' + str( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _id ] ) );


	## 		## 	_extras = {
	## 		## 		'get':		'Get',
	## 		## 		'name':		'Blah',
	## 		## 	};

	## 		## 	_func_args = MAP_ACCESSORFUNC_ID_FUNC_ARGS[ _id ];
	## 		## 	_func_name = String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );
	## 		## 	_func_this = String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE[ _id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );

	## 		## 	_extras = {
	## 		## 		## Word Parent is self or this to reflect part of the property or the instance / class the property is added to
	## 		## 		'word_parent':	_func_this,

	## 		## 		## Map Name is the raw name without self/this. and args / parens.
	## 		## 		'map_name':		_func_name,
	## 		## 		'map_args':		_func_args,

	## 		## 		## Argument / Parenthesis Spaces - left always has 1, and if there are arguments then the right has one but if there are no args then there is no extra space which complies with my coding standard.
	## 		## 		'arg_spacer_l':	' ',
	## 		## 		'arg_spacer_r':	( '', ' ' )[ len( _func_args ) > 0 ],
	## 		## 	};
	## 		## 	_func_call = String.ExpandVariables( '{word_parent}.{map_name}({arg_spacer_l}{map_args}{arg_spacer_r});', MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );

	## 		## 	print( '\tself/this:\t\t' + str( _func_this ) );
	## 		## 	print( '\tname:\t\t\t' + _func_name );
	## 		## 	print( '\targs:\t\t\t' + _func_args );
	## 		## 	print( '\tcall:\t\t\t' + _func_call );

	## 		## 	print( );







class AccessorFuncBase( AccessorFuncCoreBase ):
	##
	## Helpers
	##
	__name__ = 'AccessorFuncBase';



	##  Jote froii7hnAs part of
	__name_alases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',		keys = None,	name = 'NameAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
		## 'instance': True,
		'get': lambda this: this.__names,
		'set': lambda this, _value: setattr( this, '__names', _value )
	} );
	__key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',		keys = None,	name = 'KeyAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
		'get': lambda this: this.__keys,
		'set': lambda this, _value: setattr( this, '__keys', _value )
	} );













##
## This example is to show off all, or most of the features of the AccessorFunc system which aren't necessarily able to jne
##
##
## Tasl: AcecoolFuncExample: Things to exmplore: Data-Type specialization, auto functions based on data-type ro value typing...
##
##
##
##
##
##
##
##
##
##
##
class AccessorFuncExample( ):
	##
	##
	##

	##
	__name__ = 'AccessorFuncExample';


	##
	##
	##
	def __example__( ):
		##
		_text = '';

		##
		_text += 'AccessorFuncExample - Pending...';


		##
		return _text;