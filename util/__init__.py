##
## Acecool's Python Library - util Package - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\n\t> AcecoolLib.util' );

## Globals
from ..__global__ import *;

##
from .. import debug;

##
from . import accessor;
from ..example import GetExample;

##
## print( '\tAcecoolLib.util - Imported' );


## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;

##
##
##
## class UtilBase( ):
## 	pass;
## class Util:
## 	##
## 	##
## 	##
## 	__name__ = 'Util'


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	## ##
	## _text = '';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	## ##
	## _text += GetExample( Angle );

	## _text += debug.Header( 'Util.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += debug.Footer( ) + '\n';


	_text += debug.Header( 'Util.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += debug.Header( 'Util.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += debug.Footer( ) + '\n';

	return _text;




##
## Helper Function used to process VarArgs...
##
def ProcessVarArgs( self, _args, _callback, _print_category, _print_text, *_text_args ):
	## Set up out output / return value to a blank-string...
	_output = '';

	## Make sure we have something to process
	if ( _args != None and len( _args ) > 0 ):
		## Process the data...
		for _i, _arg in enumerate( _args ):
			_output = _output + _callback( self, _i, _arg );

	## Convert the _text_args to print-data... _output is always {0}
	_print = [ ];
	_print.append( str( _output ) );
	if ( len( _text_args ) > 0 ):
		for _, _value in enumerate( _text_args ):
			_print.append( str( _value ) );

	## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
	print( _print_category, ( _print_text.format( *_print ) ) );

	## Always return something
	return _output;


##
## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
##
def ProcessSimpleVarArgs( self, _callback, *_args ):
	## Define the amount of iterations required...
	_varargs = len( _args );

	## If callback is provided and arguments are provided and we have a valid number of arguments...
	if ( callable( _callback ) and _args != None and _varargs > 0 ):

		## Loop through the varargs...
		for _index in range( _varargs ):
			## Grab the key / value for our index..
			_key = _args[ _index ];

			## Execute the callback with the key / value...
			_callback( self, _key );
	else:
		## Return false if we can't do anything..
		return False;

	## Return true when done...
	return True;


##
## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
##
def ProcessKeyVarArgs( self, _callback, *_args ):
	## Define the amount of iterations required...
	_vararg_pairs = math.floor( len( _args ) / 2 );

	## If callback is provided and arguments are provided and we have a valid number of arguments...
	if ( callable( _callback ) and _args != None and _vararg_pairs > 0 ):

		## Loop through the varargs...
		for _i in range( _vararg_pairs ):
			## Grab the appropriate index for the first element, ie the key...
			_index = ( _i * 2 );

			## Grab the key / value for our index..
			_key = _args[ _index ];
			_value = _args[ _index + 1 ];

			## Execute the callback with the key / value...
			_callback( self, _key, _value );
	else:
		## Return false if we can't do anything..
		return False;

	## Return true when done...
	return True;


##
## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
##
def ProcessAccumulateVarArgs( self, _callback, _data = [ ], *_args ):
	## Define the amount of iterations required...
	_varargs = len( _args );

	## If callback is provided and arguments are provided and we have a valid number of arguments...
	if ( callable( _callback ) and _args != None and _varargs > 0 ):

		## Loop through the varargs...
		for _index in range( _varargs ):
			## Grab the key / value for our index..
			_key = _args[ _index ];

			## Execute the callback with the key / value...
			_data.append( _callback( self, _key, _data ) );

	## Return true when done...
	return _data;


##
##
##
def ImportInvalidlyNamed( _base, _name_invalid, _name ):
	with open( _base + _name_invalid + '.py', 'rb' ) as _data:
		_name = imp.load_module( _name, _data, _name_invalid, ( '.py', 'rb', imp.PY_SOURCE ) );

	return _name;


##
## return Acecool.logic.ternary( not _data and _data != False, False, True )
##
def isset( _data = None ):
	if ( _data == None ):
		return False;

	_isset = True;
	try:
		_data;
	except NameError:
		_isset = False;

	return _isset;