##
## Acecool's Python Library - Versioning System - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.version' );

##
from . import text;
from .__global__ import *;

##
from . import debug;
from .util.accessor import *;
from . import text as Text;
from .example import GetExample;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';



	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	_text += GetExample( Version );

	return _text;


## Generate ENUMeration for Version Octet Modes
VERSION_OCTET_RESET_NEVER, VERSION_OCTET_RESET_ON_ANY_CHANGE, VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE, VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE, ENUM_LIST_VERSION_OCTET_RESET = ENUM( 4 );

## Generate ENUMeration for Version Octents - Order them so they can be compared numerically with MAJOR the highest value, etc..
VERSION_OCTET_PATCH, VERSION_OCTET_MINOR, VERSION_OCTET_MAJOR, ENUM_LIST_VERSION_OCTES = ENUM( 3 );

##
VERSION_DESIGNATION_ALPHA, VERSION_DESIGNATION_BETA, VERSION_DESIGNATION_STABLE, VERSION_DESIGNATION_DEV, ENUM_LIST_VERSION_DESIGNATION, MAP_ENUM_LIST_VERSION_DESIGNATION, MAPR_ENUM_LIST_VERSION_DESIGNATION = ENUM( None, ( 'Alpha', 'Beta', 'Stable', 'Development' ) );

##
## Versioning System - Stores a version in Major.Minor.Revision format and to compare version each octet is converted into 10 ** len_of_previous_octets + buffer ( essentially removing the decimals ) so the values can be compared and this works quite well...
## Note: I updated this to change the way _version is used... Instead of the old method which uses SetVersion( String, None, None ) or String, String, String or Number, Number, Number and setting an actual version value and updating the major / minor / revision numbers, I instead only maintain the major / minor / revision values and use Getters for the Getversion to output a string / serialized piece of info... I also use __init__ to handle updating the data so technically there is less code involved in the newer method. I have left the old method to show.. so on_set called UnPack which converted the string GetVersion into numbers so any time SetVersion was called, it woul dbe called to update the 3 other functions.. -- I left both to show some of the ways the AccessorFunc system can be used with its callbacks..
##
## Rules: If X.0.0 is set, 0.X.X are assigned 0. If 0.X.0 is set, 0.0.X is assigned 0 - As for multiplying versions together, adding, etc... I will likely drastically change the behavior...
##
class VersionBase:
	pass
class Version( VersionBase ):
	##
	##
	##
	__name__ = 'Version';


	##
	##
	##

	## The version we use for text, can use self.GetVersion( ) or simply self.Get( ) fr Version.Get( ), Version.Set( ), Version.IsSet( ), etc.. which is a lot more intuitive...
	## __version					= AccessorFuncBase( parent = VersionBase,	key = 'version',		keys = [ ],																									name = 'Version',			names = [ '' ],																	default = '0.0.0',									getter_prefix = 'Get',			documentation = 'Versioning Blank / Base',		allowed_types = ( VALUE_ANY ),					allowed_values = ( VALUE_ANY ),					setup_override = { 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: this.GetMajorToString( ) + '.' + this.GetMinorToString( ) + '.' + this.GetPatchToString( ) ), 'set': ( lambda this, _version, _minor, _revision = 0: this.__init__( _version, _minor, _revision ) ) }															);

	## The version we use for text, can use self.GetVersion( ) or simply self.Get( ) fr Version.Get( ), Version.Set( ), Version.IsSet( ), etc.. which is a lot more intuitive...
	__version					= AccessorFuncBase( parent = VersionBase,	key = 'version',		keys = [ ],																									name = 'Version',			names = [ '' ],																	default = '0.0.0',									getter_prefix = 'Get',			documentation = 'Versioning Blank / Base',		allowed_types = ( VALUE_ANY ),					allowed_values = ( VALUE_ANY ),					setup_override = { 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: this.GetMajorToString( ) + '.' + this.GetMinorToString( ) + '.' + this.GetPatchToString( ) ), 'set': ( lambda this, _version, _minor, _revision = 0, _designation = None: this.__init__( _version, _minor, _revision, _designation ) ) }															);

	## Actual stored values... Major.Minor.Patch
	#( lambda this, _value, _new, _is_valid: this.ResetAccessors( 'Minor', 'Patch' ) if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) ) else '' ) } );
	__major						= AccessorFuncBase( parent = VersionBase,	key = 'major',			keys = [ 'version_major',			'main',		'primary' ],												name = 'Major',				names = [ 'VersionMajor',		'Primary',		'Main' ],						default = 0,										getter_prefix = 'Get',			documentation = 'X.0.0',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _value, _new, _is_valid: this.HandleResetOctet( VERSION_OCTET_MAJOR, _value, _new, _is_valid ) ) } );
	#( lambda this, _value, _new, _is_valid: this.ResetAccessors( 'Patch' ) if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) ) else '' ) } );'' ) } );
	__minor						= AccessorFuncBase( parent = VersionBase,	key = 'minor',			keys = [ 'version_minor',			'sub',		'secondary' ],												name = 'Minor',				names = [ 'VersionMinor',		'Secondary',	'Sub' ],						default = 0,										getter_prefix = 'Get',			documentation = '0.X.0',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _value, _new, _is_valid: this.HandleResetOctet( VERSION_OCTET_MINOR, _value, _new, _is_valid ) ) } );
	__patch						= AccessorFuncBase( parent = VersionBase,	key = 'patch',			keys = [ 'version_revision',		'build',	'revision' ],												name = 'Patch',				names = [ 'VersionPatch',		'Revision',		'VersionRevision' ],			default = 0,										getter_prefix = 'Get',			documentation = '0.0.X',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY )		);

	## When do we revert lower level octets? Ie, do we reset ever, do we reset only if the value is larger than the previous, do we reset if it is different, do we only reset if lower?
	__octet_mode				= AccessorFuncBase( parent = VersionBase,	key = 'octet_mode',		keys = [ 'version_octet_reset_mode',	'octet_reset_mode', 'version_octet_mode',	'octet_mode' ],			name = 'OctetResetMode',	names = [ 'VersionOctetMode', 'OctetMode', 'VersionOctetResetMode' ],			default = VERSION_OCTET_RESET_ON_ANY_CHANGE,		getter_prefix = 'Get',			documentation = 'Define when lower level octets are reverted to 0... On any value change from a higher octet, if the value is changed to something greater than, if it is changed to something less, or never',		allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VERSION_OCTET_RESET_NEVER, VERSION_OCTET_RESET_ON_ANY_CHANGE, VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE, VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE ) 		); #,					setup_override = { 'set_callback': ( lambda this, _value: this.Unpack( ) ) }																		);

	## When the version is converted to a string and / or number without decimals, this determines how many 0s are used in place of the defcijmp
	__buffer					= AccessorFuncBase( parent = VersionBase,	key = 'buffer',			keys = [ 'version_octet_buffer',	'octet_buffer' ],														name = 'OctetBuffer',		names = [ 'VersionOctetBuffer',	'Buffer' ],										default = 1 ,										getter_prefix = 'Get',			documentation = 'Version Octet Buffer Value',	allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ), 		); #,					setup_override = { 'set_callback': ( lambda this, _value: this.Unpack( ) ) }																		);

	## To extend the system at some point...
	__lang						= AccessorFuncBase( parent = VersionBase,	key = 'lang',																														name = 'InLanguage',		names = None,																	default = 'ACMS_LANG_UNKNOWN',						getter_prefix = '',				documentation = '',								allowed_types = ( TYPE_ANY ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: this.GetSetting( 'language_name', 'Default' ) ) }																		);

	## Designation, for Alpha, Beta, Stable / Release, Development / Preview -- Designation? State? What is a good word for this... state maybe..
	__designation				= AccessorFuncBase( parent = VersionBase,	key = 'designation',	keys = [ 'version_state', 'state', 'version_designation' ],													name = 'Designation',		names = [ 'VersionDesignation', 'State', 'VersionState' ],						default = VERSION_DESIGNATION_DEV,					getter_prefix = 'Get',			documentation = '0.0.0 b:000 X',				allowed_types = ( TYPE_ANY ),					allowed_values = ( VERSION_DESIGNATION_ALPHA, VERSION_DESIGNATION_BETA, VERSION_DESIGNATION_STABLE, VERSION_DESIGNATION_DEV ),					setup_override = { 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: str( MAP_ENUM_LIST_VERSION_DESIGNATION[ this.GetDesignation( ) ] ) ) }		);






	##
	##
	##
	def HandleResetOctet( this, _octet, _value, _new, _is_valid ):
		##
		return
		## _can_reset = this.CanHandleResetOctet( _octet, _value, _new, _is_valid );

		##
		## if ( _octet == VERSION_OCTET_MAJOR and _can_reset ):
		## 	this.ResetAccessors( 'Minor', 'Patch' );

		## if ( _octet == VERSION_OCTET_MINOR and _can_reset ):
		## 	this.ResetAccessors( 'Patch' );


	##
	## Determine whether or not we can reset one or more lower octets based on OctetReset Mode, the current value compared to the new vaslue, if the value is actually able to be set ( is valid only ), etc.. This is easier in a larger function form as we can split it up, use helpers so less repeated code,
	##
	## def CanRunOctetResetMode( self, _value, _new, _is_valid ):
	def CanHandleResetOctet( this, _octet, _value, _new, _is_valid ):
		## Fetch the current mode...
		_mode = this.GetOctetResetMode( );

		## If we never reset the octets, then don't... If the value isn't value, then the data hasn't actually changed so we can ignore it here too...
		if ( _mode == VERSION_OCTET_ ): # ); #_NEVER or not _is_valid ):
			return False;

		## Grab the default values in case we need them...
		_default_major, _default_minor, _default_patch = this.GetMajorDefaultValue( ), this.GetMinorDefaultValue( ), this.GetPatchDefaultValue( );

		## If the new value is valid, and it is None - we check the octet to determine which default value we compare it to, then we compare it to the _value, if not None, to see whether or not it meets the requirements of the Octet Mode.. This may be easier to process by calling return self.CanResetOctet( self, _octet, _value, _default, True );
		## Note: We don't even need to check for _is_valid as all of these calls will be value change _is_valid because of our first statement kicking the call out if not valid... or if we never reset octets..
		## if ( _is_valid and _new == None ):
			## pass;

		## For None values, we look up the default value to see what it changes to...
		## or _new != None and _value != None

		## If we only allow the reset if a value is higher than, or less than, then we need to verify the values to make sure they aren't None...
		## If _new is None, then we need to determine how we handle this case... _value, if None, uses the default because that's the actual value it gets... So, if _new is None, then _new needs to become the default value and then we can compare the change based on their config..

		## If the Octet is Major, use the default major value...
		if ( _octet == VERSION_OCTET_MAJOR ):
			_value	= ( _value, _default_major )[ _value == None ];
			_new	= ( _new, _default_major )[ _new == None ];

		## If the Octet is Minor, use the default minor value...
		if ( _octet == VERSION_OCTET_MINOR ):
			_value	= ( _value, _default_minor )[ _value == None ];
			_new	= ( _new, _default_minor )[ _new == None ];

		## If the Octet is Patch, use the default patch value...
		if ( _octet == VERSION_OCTET_PATCH ):
			_value	= ( _value, _default_patch )[ _value == None ];
			_new	= ( _new, _default_patch )[ _new == None ];


		## If we're in any-change mode, then allow it, even if the values are reset to None...
		## Task: Compare the default values returned when None is used to the previous value to determine if we actually need to reset - because if they're the same, then technically there is no change either....
		if ( _mode == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ):
			return True;

		## If we only allow the reset if a value is greater than, and the new value is greater than, then allow it
		if ( _mode == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ):
			return True;

		## If we only allow the reset if a value is less than, and the new value is less than, then allow it
		if ( _mode == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ):
			return True;

		##
		print( 'Version -> CanResetOctet -> Returning False - Default Return ... ' );

		## All situations should be covered, but just in case we'll return False here - maybe True later and add a print statement to see when, if at all, this is happening.. If it isn't, we'll get rid of one of our clauses and use it as the default.
		return False
		## if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) )


	##
	## Version Initializer - This updates all octet values based on the data being passed through meaning it can also be used as a Setter...
	##
	def __init__( self, _version = '0.0.0', _minor = None, _revision = None, _designation = None ):
		## Pre-Declare the version values as their defaults... We'll change them below..
		_ver_major, _ver_minor, _ver_build = 0, 0, 0;

		## Update the version values in order to update them in a single place below...
		if ( isstring( _version ) and isstring( _minor ) and isstring( _revision ) ):
			_ver_major, _ver_minor, _ver_build = int( _version ), int( _minor ), int( _revision );
		elif ( isnumber( _version ) and isnumber( _minor ) and isnumber( _revision ) ):
			_ver_major, _ver_minor, _ver_build = _version, _minor, _revision;
		elif ( isstring( _version ) ):
			_data = _version.split( '.' );
			_ver_major, _ver_minor, _ver_build = int( _data[ 0 ] ), int( _data[ 1 ] ), int( _data[ 2 ] );
		else:
			raise Exception( 'AcecoolLib.Version->__init__ Error: Supplied arguments aren\'t all numerical, or the first isn\'t a string containing all version octets. Types are: ( ' + str( type( _version ) ) + ', ' + str( type( _minor ) ) + ', ' + str( type( _revision ) ) + ' )' );

		## Update the 3 version Octets
		self.SetVersionMajor( _ver_major );
		self.SetVersionMinor( _ver_minor );
		self.SetVersionRevision( _ver_build );

		## Update the designation
		self.SetVersionDesignation( _designation );


	##
	## ToString Method - Printing data using print / str( ... ) is for debugging so we want to include the important information which defines it as a Version so it can't be confused as something else which may have a similar format..
	##
	def __str__( self ):
		return 'Version( ' + self.GetVersion( ) + ' )';


	##
	## Compares the string, rather than numbers, for equality... If not equal, it tries number ( in case the file was edited or a phantom character exists ) Compares the version to see if self == _compare
	## Note: This should be a base comparer - We only need 2... gt and eq or lt and eq.. Because the others can be derived from these..
	## @Return: <TYPE_BOOL> If the current version is equal to the argued version...
	##
	def __eq__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) == _compare.GetValue( self ) );


	##
	## Compares the version to see if self > _compare
	## Note: This should be a base comparer - We only need 2... gt and eq or lt and eq.. Because the others can be derived from these..
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __gt__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) > _compare.GetValue( self ) );


	##
	## Compares the version to see if self >= _compare
	## Note: This should actually call ( with self and _compare being a number object ) self.__gt__( _compare ) or self.__eq__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __ge__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) >= _compare.GetValue( self ) );


	##
	## Returns true if the current version is less than the argued version... Compares the version to see if self < _compare
	## Note: This should actually call ( with self and _compare being a number object ) ( Note: We use previously created helpers to shorten solving this ) not self.__ge__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __lt__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) < _compare.GetValue( self ) );



	##
	## Returns true if the current version is less than or equal to the argued version... Compares the version to see if self <= _compare
	## Note: This should actually call ( with self and _compare being a number object ) ( Note: We use previously created helpers to shorten solving this ) not self.__gt__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than or equal to argued version
	##
	def __le__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) <= _compare.GetValue( self ) );


	##
	## Adds two versions together - this simply performs the math from each octet on each other octet - I may change how this operates later... - Note: Since the revisions number is reset each major / minor version, I may just reset it to on adding..
	##
	def __add__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *self.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );
		## _version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		## _major, _minor, _build								= _version_a.GetAllOctets( );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major + _version_major, _minor + _version_minor, _build + _version_build;
		## _version_a.SetVersion( str( _major + _version_major ) + '.' + str( _minor + _version_minor ) + '.' + str( _build + _version_build ) );
		_version_a.SetVersion( _major + _version_major, _minor + _version_minor, _build + _version_build );
		## _major = _major + _version_major;
		## _minor = _minor + _version_minor;
		## _build = _build + _version_build;

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Subtracts a version from the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __sub__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major - _version_major, _minor - _version_minor, _build - _version_build;
		## _version_a.SetVersion( str( _major - _version_major ) + '.' + str( _minor - _version_minor ) + '.' + str( _build - _version_build ) );
		_version_a.SetVersion( _major - _version_major, _minor - _version_minor, _build - _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Multiplies a version by the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __mul__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major * _version_major, _minor * _version_minor, _build * _version_build;
		## _version_a.SetVersion( str( _major * _version_major ) + '.' + str( _minor * _version_minor ) + '.' + str( _build * _version_build ) );
		_version_a.SetVersion( _major * _version_major, _minor * _version_minor, _build * _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Divides a version by the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __div__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major / _version_major, _minor / _version_minor, _build / _version_build;
		## _version_a.SetVersion( str( _major / _version_major ) + '.' + str( _minor / _version_minor ) + '.' + str( _build / _version_build ) );
		_version_a.SetVersion( _major / _version_major, _minor / _version_minor, _build / _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Raises a version to the power of the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __pow__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		## _version_a = Version( str( *self.GetAllOctets( ).join( '.' ) ) );
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a.SetVersion( str( _major ** _version_major ) + '.' + str( _minor ** _version_minor ) + '.' + str( _build ** _version_build ) );
		_version_a.SetVersion( _major ** _version_major, _minor ** _version_minor, _build ** _version_build );
		## _version_a = Version( _major, _minor, _build );

		##
		## _version_a.major, _version_a.minor, _version_a.build = _major ** _version_major, _minor ** _version_minor, _build ** _version_build;

		##

		##
		return _version_a;


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';


		## Version Objects...
		_version_zero			= Version( '0.0.0' );
		_version_001			= Version( '0.0.1' );
		_version_010			= Version( '0.1.0' );
		_version_100			= Version( '1.0.0' );
		_version_011			= Version( '0.1.1' );
		_version_111			= Version( '1.1.1' );
		_version_002			= Version( '0.0.2' );
		_version_021			= Version( '0.2.1' );
		_version_212			= Version( '2.1.2' );
		_version_222			= Version( '2.2.2' );
		_version_123			= Version( '1.2.3' );
		_version_321			= Version( '3.2.1' );
		_version_33321456		= Version( '333.21.456' );
		_version_33322456		= Version( '333.22.456' );
		_version_10000000000	= Version( '10000.000.000' );

		_text					= '';

		##
		_text += debug.Header( 'Version.IsOlderThan( 0.0.0, 0.0.0 )', '', '', _depth_header );

		##
		def CompareExample( _version_a, _version_b, _gt = False, _ge = False, _lt = False, _le = False, _eq = False, _show_expected = False ):
			## Grab the Version Multipliers... - If all are single-digit then it'll be 100, 10 and 1... If double then 1000, 100, 1
			_a_mult_x, _a_mult_y, _a_mult_z = _version_a.GetMultipliers( );
			_b_mult_x, _b_mult_y, _b_mult_z = _version_b.GetMultipliers( );
			_w_mult_x, _w_mult_y, _w_mult_z = _version_a.GetMultipliers( _version_b );

			_text = ''
			_text += _depth_prefix + String.FormatColumn( 15, 'A Version:',			45, str( _version_a ),								15, 'B Version:',		35, str( _version_b ),									25, 'Using Octet Buffer:',		35, str( _version_a.GetVersionOctetBuffer( ) ) ) + '\n';
			## _text += _depth_prefix + String.FormatColumn( 15, 'A Version:',		45, str( _version_a.version ),						15, 'B Version:',		35, str( _version_b.version ),							25, 'Using Octet Buffer:',		35, str( _version_a.GetVersionOctetBuffer( ) ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'A Value:',			45, str( _version_a.GetValue( _version_b ) ),		15, 'B Value:',			35, str( _version_b.GetValue( _version_a ) ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'A Multipliers:',		15, str( _a_mult_x ),	15, str( _a_mult_y ),		15, str( _a_mult_z ),	15, 'B Multipliers:',									15, str( _b_mult_x ),			15, str( _b_mult_y ),			15, str( _b_mult_z ) ) + '\n';
			## _text += _depth_prefix + String.FormatColumn( 15, 'B Multipliers:',	15, str( _b_mult_x ),	15, str( _b_mult_y ),		15, str( _b_mult_z ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'W Multipliers:',		15, str( _w_mult_x ),	15, str( _w_mult_y ),		15, str( _w_mult_z ) ) + '\n';
			## _text += _depth_prefix + 'A Multipliers: ' + str( _a_mult_x ) + ', ' + str( _a_mult_y ) + ', ' + str( _a_mult_z ) + '\n';
			## _text += _depth_prefix + 'B Multipliers: ' + str( _b_mult_x ) + ', ' + str( _b_mult_y ) + ', ' + str( _b_mult_z ) + '\n';
			## _text += _depth_prefix + 'W Multipliers: ' + str( _w_mult_x ) + ', ' + str( _w_mult_y ) + ', ' + str( _w_mult_z ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' IsOlderThan ' + str( _version_b ) + ' == ' + str( _version_a.IsOlderThan( _version_b ) ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' IsNewerThan ' + str( _version_b ) + ' == ' + str( _version_a.IsNewerThan( _version_b ) ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + '  > ' + str( _version_b ) + ' == ' + str( _version_a > _version_b ) + ( '', '\t\tExpected: ' + str( _gt ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _gt == ( _version_a > _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' >= ' + str( _version_b ) + ' == ' + str( _version_a >= _version_b ) + ( '', '\t\tExpected: ' + str( _ge ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _ge == ( _version_a >= _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + '  < ' + str( _version_b ) + ' == ' + str( _version_a < _version_b ) + ( '', '\t\tExpected: ' + str( _lt ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _lt == ( _version_a < _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' <= ' + str( _version_b ) + ' == ' + str( _version_a <= _version_b ) + ( '', '\t\tExpected: ' + str( _le ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _le == ( _version_a <= _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' == ' + str( _version_b ) + ' == ' + str( _version_a == _version_b ) + ( '', '\t\tExpected: ' + str( _eq ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _eq == ( _version_a == _version_b ) ) ] + '\n';
			_text += _depth_prefix + '\n';
			return _text;

		##																	>		>=		<		<=		==
		_text += CompareExample( _version_zero,	_version_zero,				False,	True,	False,	True,	True );
		_text += CompareExample( _version_zero,	_version_001,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_001,	_version_001,				False,	True,	False,	True,	True );
		_text += CompareExample( _version_010,	_version_100,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_111,	_version_100,				True,	True,	False,	False,	False );
		_text += CompareExample( _version_111,	_version_212,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_123,	_version_212,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_123,	_version_321,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_321,	_version_123,				True,	True,	False,	False,	False );
		_text += CompareExample( _version_123,	_version_33322456,			False,	False,	True,	True,	False );
		_text += CompareExample( _version_33321456,	_version_33322456,		False,	False,	True,	True,	False );
		_text += CompareExample( _version_33322456,	_version_33321456,		True,	True,	False,	False,	False );
		_text += CompareExample( _version_33322456,	_version_10000000000,	False,	False,	True,	True,	False );

		_text += '\n\n';

		_version = _version_33321456;

		## version
		## version_major			main			primary			major
		## version_minor			minor			sub				secondary
		## version_revision			revision		build
		## version_octet_buffer		buffer


		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version: ', 50, str( _version ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version: ', 50, str( _version.version ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_major: ', 50, str( _version.version_major ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_minor: ', 50, str( _version.version_minor ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_revision: ', 50, str( _version.version_revision ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.major: ', 50, str( _version.major ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.minor: ', 50, str( _version.minor ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.revision: ', 50, str( _version.revision ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.main: ', 50, str( _version.main ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.sub: ', 50, str( _version.sub ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.patch: ', 50, str( _version.build ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.primary: ', 50, str( _version.primary ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.secondary: ', 50, str( _version.secondary ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.patch: ', 50, str( _version.patch ) ) + '\n';

		_text += '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> + ', 30, _version_33321456, 10, '+', 30, _version_321, 10, '==', 50, _version + _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> - ', 30, _version_33321456, 10, '-', 30, _version_321, 10, '==', 50, _version - _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> * ', 30, _version_33321456, 10, '*', 30, _version_321, 10, '==', 50, _version * _version_321 ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> / ', 30, _version_33321456, 10, '/', 50, _version_321, 10, '==', 50, _version / _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> ** ', 30, _version_33321456, 10, '**', 30, _version_321, 10, '==', 50, _version ** _version_321 ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> + ', 30, _version_33321456, 10, '+', 50, _version_321, 10, '==', 50, _version + _version_321 ) + '\n';
		_text += '\n';
		_text += '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version.__p: ', 250, str( _version.__p ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._pitch: ', 250, str( _version._pitch ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._p: ', 250, str( _version._p ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._yaw: ', 250, str( _version._yaw ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._y: ', 250, str( _version._y ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._roll: ', 250, str( __versionang._roll ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._r: ', 250, str( _version._r ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version.GetYAccessor( ): ', 250, str( _version.GetYAccessor( ) ) ) + '\n';

		_text += '\t\t##\n'
		_text += '\t\t## Note: I may alter the behavior to always reset smaller octets regardless of the change - except if equal to... So if I change up or down, the lower ones will be reset to 0.. If I set to the same value, then nothing changes..\n';
		_text += '\t\t## Note: I made that change.. We will see how it goes - I may revert it... Actually, I will go ahead and create a controlled toggle so that the behavior can be defined as you see fit - by default, it will reset lower octets if the value of a higher octet is changed to something different..\n';
		_text += '\t\t##\n'
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Before changes ', 30, _version_321 ) + '\n';
		_version_321.minor = 10;
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Minor to 10, expect 3.10.0', 30, _version_321 ) + '\n';

		_version_321.major = 11;
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 11, expect 11.0.0 ', 30, _version_321 ) + '\n';
		_version_321.SetMinor( 12 );
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Minor to 12, expect 11.12.0 ', 30, _version_321 ) + '\n';

		_version_321.SetMajor( 9 );
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 9 ( < ), expect 9.12.0 ', 30, _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 9 ( < ), expect 9.0.0 ', 30, _version_321 ) + '\n';



		_text += _depth_prefix + '\n';
		_text += debug.Footer( ) + '\n';



		## Unload all examples...
		_version_zero	= None;
		_version_001	= None;
		_version_010	= None;
		_version_100	= None;
		_version_011	= None;
		_version_111	= None;
		_version_002	= None;
		_version_021	= None;
		_version_212	= None;
		_version_222	= None;


		## _text += debug.Header( 'Util.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += debug.Footer( ) + '\n';

		return _text;


	##
	## Returns the values from each octet ( I could use major / primary / minor / secondary / build / revision / and more or the Get*( ) helpers...
	##
	def GetAllOctets( self ):
		return self.major, self.minor, self.patch;


	##
	## Returns the base multiplier for this version...
	##
	def GetMultipliersEx( self ):
		## Grab all values
		_major			= self.GetVersionMajor( );
		_minor			= self.GetVersionMinor( );
		_revision		= self.GetVersionRevision( );

		## Grab the length of all values...
		_major_len		= len( str( _major ) );
		_minor_len		= len( str( _minor ) );
		_revision_len	= len( str( _revision ) );

		## Return values to ensure there can be no collisions
		return ( 10 ** ( _revision_len + _minor_len + ( self.GetVersionOctetBuffer( ) * 2 ) ) ), ( 10 ** ( _revision_len + self.GetVersionOctetBuffer( ) ) ), ( 1 );


	##
	## Returns the multiplier either by itself, or when being compared with another version...
	##
	def GetMultipliers( self, _compare = None ):
		## Grab the base multipliers...
		_mult_x, _mult_y, _mult_z = self.GetMultipliersEx( )
		if ( _compare == None ):
			return _mult_x, _mult_y, _mult_z;

		## If compare is assigned, we need the max of all...
		_mult_b_x, _mult_b_y, _mult_b_z = _compare.GetMultipliersEx( );

		## Return the maximum multiplier to use... This is so compares can be on the same page...
		return max( _mult_x, _mult_b_x ), max( _mult_y, _mult_b_y ), max( _mult_z, _mult_b_z );


	##
	## Returns the Version Value after being multiplied...
	##
	def GetValue( self, _compare = None ):
		## Grab the multipliers
		_mult_x, _mult_y, _mult_z = self.GetMultipliers( _compare );

		## Rerturn the single-numerical value for the version..
		return int( ( self.GetVersionMajor( ) * _mult_x ) + ( self.GetVersionMinor( ) * _mult_y ) + ( self.GetVersionRevision( ) * _mult_z ) );


	##
	## Returns whether or not _version is newer than _compare
	##
	def IsNewerThan( self, _compare = '0.0.0' ):
		return self.__gt__( _compare );
		## return ( not _version == _compare and not _version.IsOlderThan( _compare ) )


	##
	## Returns whther or not _version is older than _compare
	##
	def IsOlderThan( self, _compare = '0.0.0' ):
		return self.__lt__( _compare );




	## ## Old and new version.. Old uses callback so whe SetVersion is called, the Major / Minor / Revision values would be updated through the callback...
	## ##	With the new, GetVersion uses Major / Minor / Revision Getters to return the value, and Set uses __init__ which had to process the data anyway, so instead of repeating code, there is actually less...
	## ##	And, now I'm not storing Version value so there is less overhead - and the data isn't being stored twice..
	## __version								= AccessorFuncBase( parent = VersionBase,	key = 'version',				keys = ( 'x' ),												name = 'Version',					names = ( ),											default = '0.0.0',							getter_prefix = 'Get',			documentation = 'Version Value',				allowed_types = ( TYPE_STRING ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _current, _new, _has_updated: this.Unpack( ) ) }															);

	## ##
	## ## Version Initializer - This updates all octet values based on the data being passed through meaning it can also be used as a Setter...
	## ##
	## def __init__( self, _version = '0.0.0', _minor = None, _revision = None ):

	## 	## Old Init Code - The old one used SetVersion which, when called, called a callback on_set which then updated all 3 Major / Minor / Revision stored data...
	## 	## 	However, Version was also stored ( I could've used a Getter the same way I am now to ensure I used the 3 octets, but version would still be a string of the data with increased overhead - by  )

	## 	self.SetVersion( _version );
	## 	self.version = _version;

	## 	##
	## 	if ( isstring( _version ) ):
	## 		print( 'Version -> SetVersion: ' + str( _version ) );
	## 		self.SetVersion( _version );
	## 		## self.version = _version
	## 		## self.Unpack( );
	## 	elif( isnumber( _version ) and isnumber( _minor ) and isnumber( _revision ) ):
	## 		self.SetVersion( str( _version ) + '.' + str( _minor ) + '.' + str( _revision ) );
	## 	elif( isstring( _version ) and isstring( _minor ) and isstring( _revision ) ):
	## 		self.SetVersion( _version + '.' + _minor + '.' + _revision );
	## 		## self.version = str( _version ) + '.' + str( _minor ) + '.' + str( _revision )

	## 		## Task: Maybe add an alternative on_set callback for this case.. If numeric then avoid - or an alternative setter to allow multiple args, joined on a delimeter or something...
	## 		## 	self.SetVersionMajor( _version );
	## 		## 	self.SetVersionMinor( _minor );
	## 		## 	self.SetVersionRevision( _revision );
	## 		## 	## self.version_major		= _version;
	## 		## 	## self.version_minor		= _minor;
	## 		## 	## self.version_revision	= _revisionw;
	## 	else:
	## 		raise Exception( 'AcecoolLib.Version->__init__ Error: Supplied arguments aren\'t all numerical, or the first isn\'t a string containing all version octets. Types are: ( ' + str( type( _version ) ) + ', ' + str( type( _minor ) ) + ', ' + str( type( _revision ) ) + ' )' );


	## ##
	## ##
	## ##
	## def GetAll( _compare = '0.0.0' ):
	## 	##
	## 	_version			= '0.0.0';
	## 	_version_major, _version_minor, _version_revision		= 0, 0, 0;

	## 	##
	## 	if ( isstring( _compare ) ):
	## 		_version = _compare;
	## 		_version_major, _version_minor, _version_revision = Version.UnpackEx( _compare );
	## 	elif ( isinstance( _compare, Version ) ):
	## 		_version = _compare.version
	## 		_version_major, _version_minor, _version_revision = _compare.Unpack( );

	## 	##
	## 	return _version, _version_major, _version_minor, _version_revision;


	## ##
	## ## Old Version System
	## ##
	## def UnpackEx( _ver = '0.0.0' ):
	## 	##
	## 	_version = '0.0.0';

	## 	##
	## 	if ( isstring( _ver ) ):
	## 		_version = _ver;
	## 	elif ( isinstance( _ver, Version ) ):
	## 		_version = _ver.version;

	## 	##
	## 	_versions 	= _version.split( '.' );

	## 	##
	## 	_version_major		= 0 + int( ( 0, _versions[ 0 ] )[ _versions[ 0 ] != None and isstring( _versions[ 0 ] ) ] );
	## 	_version_minor		= 0 + int( ( 0, _versions[ 1 ] )[ _versions[ 1 ] != None and isstring( _versions[ 1 ] ) ] );
	## 	_version_revision	= 0 + int( ( 0, _versions[ 2 ] )[ _versions[ 2 ] != None and isstring( _versions[ 2 ] ) ] );

	## 	##
	## 	return _version_major, _version_minor, _version_revision;


	## ##
	## ## Old Version System
	## ##
	## def Unpack( self ):
	## 	##
	## 	_major, _minor, _revision = Version.UnpackEx( self.GetVersion( ) );
	## 	self.SetVersionMajor( _major );
	## 	self.SetVersionMinor( _minor );
	## 	self.SetVersionRevision( _revision );

	## 	##
	## 	print( 'Version -> Unpack -> ' + str( self ) )

	## 	##
	## 	return _major, _minor, _revision;