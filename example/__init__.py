##
## Acecool Python Library - Examples App - Josh 'Acecool' Moser
##
## Note: When you run this package from Python, this should be the file which executes... Have it launch a helper app, or api guide, etc.. whatever...
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\n\t> AcecoolLib.example' );


##
## Imports
##
from ..__global__ import *;
## from .. import text as Text;
from .. import debug;
## from .. import dict;
## from .. import Examples;
## from .. import steam;
## from .. import string;

## from .. import Steam;

##
## print( '\tAcecoolLib.examples - Imported' );


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';


	return _text;




##
##
##
def GetExample( _class ):
	##
	_example		= getattr( _class, '__example__', None );
	_name			= getattr( _class, '__name__', 'AcecoolLib.debug' );

	##
	_accessor_db	= getattr( _class, 'accessor_func_db', None );

	## If __example__ exists, then great..
	if ( _example != None ):
		if ( callable( _example ) ):
			return debug.Header( _name + ': Examples' ) + str( _example( ) ) + '\n------- Uses AccessorFuncDB? -------\n' + str( _accessor_db );
		## else:
		## 	return debug.Header( _name + ': Examples' ) + '\tExample Not Available\n\n\n';
	## else:
	## 	return debug.Header( _name + ': Examples' ) + '\tExample Not Available\n\n\n';

	return None;

##
##
##
def Show( ):
	##
	## Despite not being named __globals__, since I import *, __example__ should be loaded into the global / local chain...
	print( GetExample( locals( ) ) );
	print( GetExample( Acecool ) );
	print( GetExample( Dict ) );
	print( GetExample( String ) );
	print( GetExample( Steam ) );


##
##
##
def GetAll( _data ):
	## for _, _value in enumerate( _text_args ):
	## 	_print.append( str( _value ) );



	## for _index in range( _varargs ):
	## 	## Grab the key / value for our index..
	## 	_key = _args[ _index ];

	## 	## Execute the callback with the key / value...
	## 	_callback( self, _key );

	##
	print( GetExample( _data ) );
	## print( GetExample( _globals ) );

	for _value in _data:
		_ex_value = GetExample( _value );
		if ( _ex_value != None ):
			print( str( _value ) + ' -- ' + str( _ex_value ) );

		## _ex_key = GetExample( _key )
		## if ( _ex_key != None ):
		## 	print( str( _key ) + ' -- ' + str( _ex_key ) );

		try:
			if ( _value and not isinstance( _value, str ) and getattr( _value, 'items' ) ):
				GetAll( _value );
		except:
			pass;

		## try:
		## 	if ( _key and not isinstance( _key, str ) and getattr( _key, 'items' ) ):
		## 		GetAll( _value );
		## except:
		## 	pass;

	## for _key, _value in _globals.items( ):
	## 	print( str( _value ) + ' -- ' + str( GetExample( _value ) ) );
	## 	print( str( _key ) + ' -- ' + str( GetExample( _key ) ) );

	## for _key, _value in enumerate( _locals ):

	## for _key, _value in enumerate( _globals ):
	## 	print( str( _value ) + ' -- ' + str( GetExample( _value ) ) );
	## 	print( str( _key ) + ' -- ' + str( GetExample( _key ) ) );