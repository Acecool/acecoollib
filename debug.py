##
## Acecool's Python Library - Debug Library - Josh 'Acecool' Moser
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##

## This library contains debugging functionality such as simple output helpers. Some of which are contained in other libraries, but are brought into the debugging

##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.debug' );


##
import sys;
from functools import wraps;

##
from .__global__ import *;
from . import string as string;
from . import text as text;


##
##
##
def TraceFunc( _frame, _event, _arg, _indent = [ 0 ] ):
	##
	if ( _event == 'call' ):
		##
		_indent[ 0 ] += 1;

		##
		print( '\t' * _indent[ 0 ] + '> call function', str( _frame.f_code.co_name ) + ' -- ' + str( _frame.f_code ) );
	elif ( _event == 'return' ):
		## print( '<' + '-' * _indent[ 0 ], 'exit function', _frame.f_code.co_name );
		print( '\t' * _indent[ 0 ] + '/', 'exit function', _frame.f_code.co_name );
		_indent[ 0 ] -= 1;

	##
	return TraceFunc;


## ##
## ## Use as a decorator on functions that should be traced. Several
## ## functions can be decorated - they will all be indented according
## ## to their call depth.
## ##
## class TraceCalls( object ):
## 	##
## 	##
## 	##
##     def __init__(self, stream=sys.stdout, indent_step=2, show_ret=False):
##         self.stream = stream
##         self.indent_step = indent_step
##         self.show_ret = show_ret

##         # This is a class attribute since we want to share the indentation
##         # level between different traced functions, in case they call
##         # each other.
##         TraceCalls.cur_indent = 0


## 	##
## 	##
## 	##
##     def __call__(self, fn):
##         @wraps(fn)
##         def wrapper(*args, **kwargs):
##             indent = ' ' * TraceCalls.cur_indent
##             argstr = ', '.join(
##                 [repr(a) for a in args] +
##                 ["%s=%s" % (a, repr(b)) for a, b in kwargs.items()])
##             self.stream.write('%s%s(%s)\n' % (indent, fn.__name__, argstr))

##             TraceCalls.cur_indent += self.indent_step
##             ret = fn(*args, **kwargs)
##             TraceCalls.cur_indent -= self.indent_step

##             if self.show_ret:
##                 self.stream.write('%s--> %s\n' % (indent, ret))
##             return ret


##
##
##
def EnableTrace( ):
	sys.settrace( TraceFunc );


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	## ◙
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';
	_text += '		## Create a 3 line piped text which looks something like this:	X	Text Appears Here	X ---- Redirects to AcecoolLib.text.PipedHeader( ... );';
	_text += '		AcecoolLib.debug.PipedHeader( _text, _pipes = \'X\', _buffer = 4, _buffer_chars = \' \' );\n';
	_text += '\n';
	_text += '		## Simple Text-Based Header... Will be moved to Text / String package soon...\n';
	_text += '		AcecoolLib.debug.Header( _header = \'\', _prefix = \'\', _suffix = \'\', _depth = 0 );\n';
	_text += '\n';
	_text += '		## Simple Text-Based Footer... Will be moved to Text / String package soon...\n';
	_text += '		AcecoolLib.debug.Footer( _prefix = \'\', _suffix = \'\\n\\n\', _depth = 1 );\n';
	_text += '\n';
	_text += '		## Text Replacers used in _text.format( **AcecoolLib.Acecool.Replacers( _depth_prefix ) );\n';
	_text += '		AcecoolLib.debug.Replacers( _depth_prefix = \'\' );\n';
	_text += '\n';
	_text += '		## This function - used to list all functions, and give a brief demonstration, or information as to their usage and / or purpose...\n';
	_text += '		AcecoolLib.debug.__example__( _depth = 1 );\n';
	_text += '\n';
	_text += '\n';
	_text += '\n';
	_text += '\n';

	## ## _depth_prefix = _depth * '\t';
	## ## _depth_prefix +;
	## ## _depth_prefix

	## ##
	## _examples = Examples( )

	## ##
	## print( _examples.GetAll( ) );

	return _text;


##																◙						◙
## Create a 3 line piped text which looks something like this:	◙	Text Appears Here	◙
##																◙						◙
def PipedHeader( _text, _pipes = '◙', _buffer = 4, _buffer_chars = ' ' ):
	return text.FormatPipedHeader( _text, _pipes, _buffer, _buffer_chars );


##
## Header
##
def Header( _header = '', _prefix = '', _suffix = '', _depth = 0 ):
	##
	_depth_prefix = _depth * '\t';

	##
	_col_key = 25;
	_col_eq = 25;
	_col_res = 25;

	##
	_text = _depth_prefix + '##\n';
	_text += _depth_prefix + '## ' + _header + '\n';
	_text += _depth_prefix + '##\n';
	_text += '' + _depth_prefix + _prefix + string.FormatColumnEx( '-', _col_key + _col_eq + _col_res, '' ) + _suffix + '\n';

	return _text.format( **Replacers( _depth_prefix ) );


##
## Footer
##
def Footer( _prefix = '', _suffix = '\n\n', _depth = 1 ):
	##
	_depth_prefix = _depth * '\t';

	##
	_col_key = 25;
	_col_eq = 25;
	_col_res = 25;

	##
	_text = _depth_prefix + _prefix + string.FormatColumnEx( '~', _col_key + _col_eq + _col_res, '' ) + _suffix;

	return _text.format( **Replacers( _depth_prefix ) );


##
## Replacers for easier additions to comment headers, etc...
##
def Replacers( _depth_prefix = '' ):
	##
	_br						= '\n';
	_tab					= '\t';
	_space					= ' ';

	##
	_comment_base			= '##';
	_comment				= _comment_base + _space;
	_comment_tab			= _comment_base + _tab;
	_comment_br				= _comment_base + _br;

	##
	_note_base				= 'Note:';
	_note					= _note_base + _space;
	_note_tab				= _note_base + _tab;

	##
	_hierarchy_base			= 'Hierarchy:';
	_hierarchy				= _hierarchy_base + _space;
	_hierarchy_tab			= _hierarchy_base + _tab;

	##
	_example_base			= 'ie:';
	_example				= _example_base + _space;
	_example_tab			= _example_base + _tab;

	##
	_depth_comment_base		= _br + _depth_prefix + _comment_base;
	_depth_comment			= _depth_comment_base + _space;
	_depth_comment_tab		= _depth_comment_base + _tab;
	_depth_comment_br		= _depth_comment_base + _br;


	##
	_comment_note			= _comment + _note;
	_comment_hierarchy		= _comment + _hierarchy;

	##
	_depth_note_base		= _depth_comment + _note_base;
	_depth_note				= _depth_comment + _note;
	_depth_note_tab			= _depth_comment + _note_tab;

	##
	_depth_example_base		= _depth_comment_tab + _example_base;
	_depth_example			= _depth_comment_tab + _example;
	_depth_example_tab		= _depth_comment_tab + _note_tab;

	##
	_footer					= _depth_comment_br + _depth_prefix;

	##
	_replacers = {
		##
		'br':						_br,
		'soace':					_space,

		##
		'tab':						_tab,
		'tab2':						_tab * 2,
		'tab3':						_tab * 3,
		'tab4':						_tab * 4,
		'tab5':						_tab * 5,

		##
		'depth':					_depth_prefix,

		##
		'note':						_note,
		'note_base':				_note_base,
		'note_tab':					_note_tab,

		##
		'hierarchy':				_hierarchy,
		'hierarchy_base':			_hierarchy_base,
		'hierarchy_tab':			_hierarchy_tab,

		##
		'example':					_example,
		'example_base':				_example_base,
		'example_tab':				_example_tab,

		##
		'depth_example':			_depth_example,
		'depth_example_base':		_depth_example_base,
		'depth_example_tab':		_depth_example_tab,


		##
		'comment':					_comment,
		'comment_base':				_comment_base,
		'comment_tab':				_comment_tab,
		'comment_note':				_comment_note,
		'comment_hierarchy':		_comment_hierarchy,

		##
		'depth_comment':			_depth_comment,
		'depth_comment_base':		_depth_comment_base,
		'depth_comment_tab':		_depth_comment_tab,

		##
		'depth_note':				_depth_note,
		'depth_note_tab':			_depth_note_tab,


		##
		'footer':					_footer,


	};

	##
	return _replacers;