##
## Acecool's Python Library - AcecoolLib Conclusive Includer - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\nAcecoolLib - Importing...\n' );

##
## from . import *
## from . import __all__
## from _AcecoolLib.Text import *
## from _AcecoolLib.Text import __all__

## Globals
from .__global__ import *;

## ##
## ## from .Acecool import *;
## from . import Acecool;
from . import Acecool;
from . import ai;
from . import debug;
from . import dict;
from . import example;
from . import file;
from . import folder;
from . import function;
from . import list;
from . import logic;
from . import math;
from . import python;
from . import string;
from . import system;
from . import table;
from . import text;
from . import tuple;
from . import version;

## Packages
from . import database;
from . import datatypes;
from . import examples;
from . import objects;
from . import settings;
from . import steam;
from . import time;
from . import util;



##
##
##

##
print( '\nAcecoolLib - Imported\n' );








##
##
##
def GetExample( _class ):
	##
	_example		= getattr( _class, '__example__', None );
	_name			= getattr( _class, '__name__', 'AcecoolLib.debug' );

	##
	_accessor_db	= getattr( _class, 'accessor_func_db', None );

	## If __example__ exists, then great..
	if ( _example != None ):
		if ( callable( _example ) ):
			return debug.Header( _name + ': Examples' ) + str( _example( ) ) + '\n------- Uses AccessorFuncDB? -------\n' + str( _accessor_db );
		## else:
		## 	return debug.Header( _name + ': Examples' ) + '\tExample Not Available\n\n\n';
	## else:
	## 	return debug.Header( _name + ': Examples' ) + '\tExample Not Available\n\n\n';

	return None;


##
## Examples...
##
def Examples( ):
	##
	## Despite not being named __globals__, since I import *, __example__ should be loaded into the global / local chain...
	print( GetExample( locals( ) ) );
	print( GetExample( Acecool ) );
	print( GetExample( debug ) );
	print( GetExample( dict ) );
	print( GetExample( example ) );
	print( GetExample( file ) );
	print( GetExample( folder ) );
	print( GetExample( function ) );
	print( GetExample( list ) );
	print( GetExample( logic ) );
	print( GetExample( math ) );
	print( GetExample( python ) );
	print( GetExample( string ) );
	print( GetExample( system ) );
	print( GetExample( table ) );
	print( GetExample( text ) );
	print( GetExample( tuple ) );
	print( GetExample( version ) );
	print( GetExample( database ) );
	print( GetExample( datatypes ) );
	print( GetExample( examples ) );
	print( GetExample( objects ) );
	print( GetExample( settings ) );
	print( GetExample( steam ) );
	print( GetExample( time ) );
	print( GetExample( util ) );
