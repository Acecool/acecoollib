##
## Acecool's Python Library - steam Package - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\n\t> AcecoolLib.steam' );

## Globals
from ..__global__ import *;

##
from .. import debug;

## Steam Exception Object
from . import SteamException;
## import AcecoolLib.Steam.SteamException;

## String
## import AcecoolLib.String;
from .. import string as String;
## from SteamException import * as SteamException;

##
## print( '\tAcecoolLib.steam - Imported' );




##
## Steam API - Acecool's Edition...
##


##
## Library Helpers / Defaults / ENUMs, etc...
##
OWNER_STEAMID32			= 'STEAM_0:1:4173055';
DEFAULT_STEAMID32		= 'STEAM_0:0:0';
DEFAULT_STEAMID64		= 0;
DEFAULT_STEAMID3		= '[U:0:0]';
DEFAULT_BOT_STEAMID32	= 'BOT';
DEFAULT_BOT_STEAMID64	= 0;
DEFAULT_BOT_STEAMID3	= 'BOT';


##
##
##
def Notify( _text = '' ):
	print( 'AcecoolLib.Steam.Notify -> ' + str( _text ) );


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	## Begin Examples
	## _text = 'AcecoolLib.Steam.* Examples --------------------------------------------------------------------------\n\n\n';



	##
	## AcecoolLib.Steam.* Globals
	##
	_text += '	AcecoolLib.Steam Globals --------------------------------------------------------------------------\n';
	_text += '\n';

	_text += '		OWNER_STEAMID32				= ' + str( OWNER_STEAMID32 ) + '\n';
	_text += '		DEFAULT_STEAMID32			= ' + str( DEFAULT_STEAMID32 ) + '\n';
	_text += '		DEFAULT_STEAMID64			= ' + str( DEFAULT_STEAMID64 ) + '\n';
	_text += '		DEFAULT_STEAMID3			= ' + str( DEFAULT_STEAMID3 ) + '\n';
	_text += '		DEFAULT_BOT_STEAMID32		= ' + str( DEFAULT_BOT_STEAMID32 ) + '\n';
	_text += '		DEFAULT_BOT_STEAMID64		= ' + str( DEFAULT_BOT_STEAMID64 ) + '\n';
	_text += '		DEFAULT_BOT_STEAMID3		= ' + str( DEFAULT_BOT_STEAMID3 ) + '\n';

	_text += '\n';
	_text += '	/ AcecoolLib.Steam Globals --------------------------------------------------------------------------\n\n\n';



	##
	## AcecoolLib.Steam.* Functions List
	##
	_text += '	AcecoolLib.Steam.* Functions List --------------------------------------------------------------------------\n';
	_text += '\n';

	_text += '		## Examples Helper -- Optional argument _steamid allows you to sustitute the default OWNER_STEAMID32 with something else to verify everything is working...\n';
	_text += '		AcecoolLib.Steam.Example( [ _steamid ] )\n\n';

	_text += '		## Debugging Print Function which points back to this file...\n';
	_text += '		AcecoolLib.Steam.Notify( [ _text ] )\n\n';

	_text += '		## Helper, returns the default values in case of Console / Bot\n';
	_text += '		AcecoolLib.Steam.TranslateSteamIDDefaults( [ _is_bot ] )\n\n';

	_text += '		## Translates ANY type of SteamID ( 32, 64, 3 AKA U ) into all 3 and outputs them in that order: 32, 64, U -- If _tab is True, then it will be returned as a Dictionary using steamid32, steamid64, and steamidu\n';
	_text += '		AcecooLlib.Steam.TranslateSteamID( _steamid [ , _tab ] )\n\n';

	_text += '		## Converts a steam id to 64 variant from a 32 variant\n';
	_text += '		AcecoolLib.Steam.SteamIDTo64( [ _steamid32 ] )\n\n';

	_text += '		## Converts a steam id from a 64 variant to the 32 variant\n';
	_text += '		AcecoolLib.Steam.SteamIDFrom64( [ _steamid64 ] )\n';

	_text += '\n';
	_text += '	/ AcecoolLib.Steam.* Functions List --------------------------------------------------------------------------\n\n\n';



	##
	## AcecoolLib.Steam.TranslateSteamID( ... ) Example
	##

	## Function call...
	_arg_steam = OWNER_STEAMID32; # ( OWNER_STEAMID32, _steam )[ ( _steam != None ) ]; # ( false, true )[ cond ]
	_steamid32, _steamid64, _steamid3u = TranslateSteamID( _arg_steam );
	_tab = TranslateSteamID( _arg_steam, True );

	## Example Output...
	_text += '	AcecoolLib.Steam.TranslateSteamID Example --------------------------------------------------------------------------\n';
	_text += '		Note: SteamID64 has yet to be ported due to base64 encode / decode functionality not yet implemented...\n';
	_text += '\n';
	_text += '		## TranslateSteamID: Input takes ANY type of SteamID ( 32 / 64 / 3 AKA U ) and always outputs all 3, in this order: 32, 64, 3 AKA U...\n';
	_text += '		_steamid, _steamid64, _steamid3 = AcecoolLib.Steam.TranslateSteamID( "' + _arg_steam + '" );\n';
	_text += '\n';
	_text += '			_steamid32							= ' + str( _steamid32 ) + '\n';
	_text += '			_steamid64							= ' + str( _steamid64 ) + '\n';
	_text += '			_steamid3u							= ' + str( _steamid3u ) + '\n';
	_text += '\n';

	_text += '\n';
	_text += '		## TranslateSteamID: Input takes ANY type of SteamID ( 32 / 64 / 3 AKA U ) and always outputs all 3, in this order: 32, 64, 3 AKA U...\n';
	_text += '		_tab = AcecoolLib.Steam.TranslateSteamID( "' + _arg_steam + '", True );\n';
	_text += '\n';
	_text += '			_tab.get( \'steamid32\', \'Error\' )	= ' + str( _tab.get( 'steamid32', 'Error' ) ) + '\n';
	_text += '			_tab.get( \'steamid64\', \'Error\' )	= ' + str( _tab.get( 'steamid64', 'Error' ) ) + '\n';
	_text += '			_tab.get( \'steamidu\', \'Error\' )		= ' + str( _tab.get( 'steamidu', 'Error' ) ) + '\n';
	_text += '\n';
	_text += '			... or ...\n';
	_text += '\n';
	_text += '			_tab[ \'steamid32\' ]					= ' + str( _tab[ 'steamid32' ] ) + '\n';
	_text += '			_tab[ \'steamid64\' ]					= ' + str( _tab[ 'steamid64' ] ) + '\n';
	_text += '			_tab[ \'steamidu\' ]					= ' + str( _tab[ 'steamidu' ] ) + '\n';
	_text += '\n';



	_text += '	/ AcecoolLib.Steam.TranslateSteamID Example --------------------------------------------------------------------------\n\n\n';

	## End Examples
	_text += '/ AcecoolLib.Steam.* Examples --------------------------------------------------------------------------\n\n';


	return _text;



##
##
##
def TranslateSteamIDDefaults( _bot = None ):
	## If we're supposed to return the bot defaults - return them...
	if ( _bot ):
		return DEFAULT_BOT_STEAMID32, DEFAULT_BOT_STEAMID64, DEFAULT_BOT_STEAMID3;

	## Return standard defaults
	return DEFAULT_STEAMID32, DEFAULT_STEAMID64, DEFAULT_STEAMID3;


##
## Converts a steam id to 64 variant from a 32 variant
##
def SteamIDTo64( _id = None ):
	## If the ID isn't set, raise an exception?
	if ( not _id ):
		raise SteamException( 'SteamIDTo64 ->  Issue, _id has not been set!' );



##
## Converts a steam id from a 64 variant to the 32 variant
##
def SteamIDFrom64( _id = None ):
	## If the ID isn't set, raise an exception?
	if ( not _id ):
		raise SteamException( 'SteamIDFrom64 -> Issue, _id has not been set!' );

	return



##
## Takes any type of SteamID ( 32, 64, or 3 / U ) Variant, and outputs in this order ( 32, 64, 3 / U ) ie: STEAM_0:1:4173055, 76561197968611839, [U:1:8346111]
## Translate input SteamID, SteamID64 or SteamU into SteamID, SteamID64 AND SteamU. Return in same order 32, 64, U :: 'STEAM_0:1:4173055', 76561197968611839, '[U:1:8346111]'
##
## steamID			STEAM_0:1:4173055
## steamID3			[U:1:8346111]
## steamID64		76561197968611839
## http://steamcommunity.com/id/Acecool
## http://steamcommunity.com/profiles/76561197968611839
##
def TranslateSteamID( _id = None, _tab = False ):
	## SteamID Scope Trackers
	_steamid32				= _id;
	_steamid64				= -1;
	_steamuid				= '';

	##
	_id_isstring			= isstring( _id );
	_id_isnumber			= isnumber( _id );

	## If the ID isn't set, raise an exception?
	if ( not _id ):
		raise SteamException( 'TranslateSteamID -> Any type of SteamID was not provided... id == None...' );
		return TranslateSteamIDDefault;

	## If we've used an entity...
	if ( not _id_isstring and not isnumber( _id ) ):
		raise SteamException( 'TranslateSteamID -> Entities ( Player or otherwise ) sre not supported in this version of Steam.TranslateSteamID...' );
		return TranslateSteamIDDefaults( True );


	## If we're looking at a bot via either using BOT, 0 or any non-player entity...
	## Note: Determine whether or not I should create the Player Object so it could be imported... Likely unnecessary.. -- or ( not _id_isstring and Steam.IsValidEntity( _id ) and not Steam.IsValidPlayer( _id ) )
	if ( ( _id_isstring and _id.upper( ) == 'BOT' ) or ( _id == 0 ) ):
		return TranslateSteamIDDefaults( True );

	## Helpers - Extract all numbers from the _id and determine if they are the same.. If they are then _id == SteamID64
	_steam_digits			= String.ExtractNumbers( _id );
	_is_len_same			= len( str( _id ) ) == len( str( _steam_digits ) );

	## If _id Input is SteamID3/U, ensure it is moved to correct var and calculate Steam32.
	if ( _id[ 1 : 2 ].upper( ) == 'U' ):
		## If UID was input, ensure it is moved to correct var and calculate Steam32.
		_steamuid			= _id;

		## Remove first number from 1x after strip, before: [U:1:x] and convert to number.
		_id					= tonumber( string.sub( _steam_digits, 2 ) );

		## Even or odd?
		_is_even			= ( _id % 2 == 0 );
		_id					= math.floor( _id / 2 );

		## Set _steamid32 since we calculated it, and because we have the steamU we only need 64. Do it, and return.
		_steamid32			= 'STEAM_0:' + ( '0' if ( _is_even	) else '1' ) + ':' + _id;
		_steamid64			= SteamIDTo64( _steamid32 );


	else:
		## We're left with either 32 or 64 as options for input... A simple solution to verify 64bit input is by using the length of the string after removing all non-digits - if they remain the same, then the input is SteamID64..
		## If they are not the same length, generate 64 from 32 as it isn't 32... If they are the same len, then it is 64 so assign _id to 64... For 32, it's the opposite..
		## Because we are in else... It isn't U... So, because it isn't u, and len is dif, it must be 32 so generate 64 from 32... For 32, same... generate from 64 if len is same, otherwise keep 32 the same... which is _id from when I set at the start...
		_steamid64			= SteamIDTo64( _steamid32 ) if ( not _is_len_same ) else _id;
		_steamid32			= SteamIDFrom64( _steamid64 ) if ( _is_len_same ) else _steamid32;

		## Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
		if ( _steamuid == '' ):
			##
			_uidX			= _steamid32[ 8 : 9 ]; #tonumber( string.sub( _steamid32, 9, 9 ) );

			##
			_uidY			= _steamid32[ 10 : ]; #tonumber( string.sub( _steamid32, 11 ) );

			##
			if ( _uidY and _uidX ):
				##
				_uidY		= ( _uidY * 2 ) + _uidX;

				##
				_steamuid	= '[U:1:' + _uidY + ']';



	## We're done.. Output in the static order regardless of input. Another option is to return a table...
	if ( _tab ):
		return {
			'steamid32': _steamid32,
			'steamid64': _steamid64,
			'steamidu': _steamuid
		};
	else:
		return _steamid32, _steamid64, _steamuid;