##
## - Josh 'Acecool' Moser
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.steam.SteamException' );

##
from .. import debug;
from ..example import GetExample;

##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( SteamException );

	return _text;


##
##
##
class SteamException( Exception ):
	##
	##
	##
	def __example__( _depth = 1 ):
		return '';


	##
	##
	##
	def __init__( self, *_args ):
		super( ).__init__( *_args );

		##
		self.Notify( 'Testing' );


	##
	##
	##
	def Notify( _text = '' ):
		print( 'AcecoolLib.Steam.SteamException.Notify -> ' + str( _text ) );