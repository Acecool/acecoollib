##
## - Josh 'Acecool' Moser
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.steam.SteamAPI' );

##
from ..__globals__ import *

##
from .. import debug;
## from SteamException import * as SteamException;
from ..example import GetExample;

##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Steam );

	return _text;


##
## Steam API - Acecool's Edition...
##
class Steam( ):
	##
	## Library Helpers / Defaults / ENUMs, etc...
	##
	DEFAULT_STEAMID32		= 'STEAM_0:0:0';
	DEFAULT_STEAMID64		= 0;
	DEFAULT_STEAMID3		= '[U:0:0]';
	DEFAULT_BOT_STEAMID32	= 'BOT';
	DEFAULT_BOT_STEAMID64	= 0;
	DEFAULT_BOT_STEAMID3	= 'BOT';


	##
	##
	##
	def __example__( _depth = 1 ):
		return '';


	##
	##
	##
	def Notify( _text = '' ):
		print( 'AcecoolLib.Steam.Notify -> ' + str( _text ) );


	##
	##
	##
	def TranslateSteamIDDefaults( _bot = None ):
		## If we're supposed to return the bot defaults - return them...
		if ( _bot ):
			return Steam.DEFAULT_BOT_STEAMID32, Steam.DEFAULT_BOT_STEAMID64, Steam.DEFAULT_BOT_STEAMID3;

		## Return standard defaults
		return Steam.DEFAULT_STEAMID32, Steam.DEFAULT_STEAMID64, Steam.DEFAULT_STEAMID3;


	##
	## Converts a SteamID64 from a SteamID32
	##
	def SteamIDTo64( _id = None ):
		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'SteamIDTo64 ->  Issue, _id has not been set!' );



	##
	## Converts a SteamID32 from a SteamID64
	##
	def SteamIDFrom64( _id = None ):
		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'SteamIDFrom64 -> Issue, _id has not been set!' );



	##
	## Takes any type of SteamID ( 32, 64, or 3 / U ) Variant, and outputs in this order ( 32, 64, 3 / U ) ie: STEAM_0:1:4173055, 76561197968611839, [U:1:8346111]
	## Translate input SteamID, SteamID64 or SteamU into SteamID, SteamID64 AND SteamU. Return in same order 32, 64, U :: 'STEAM_0:1:4173055', 76561197968611839, '[U:1:8346111]'
	##
	## steamID			STEAM_0:1:4173055
	## steamID3			[U:1:8346111]
	## steamID64		76561197968611839
	## http://steamcommunity.com/id/Acecool
	## http://steamcommunity.com/profiles/76561197968611839
	##
	def TranslateSteamID( _id = None ):
		## SteamID Scope Trackers
		_steamid32				= _id;
		_steamid64				= -1;
		_steamid3U				= '';

		##
		_id_isstring			= isstring( _id );
		_id_isnumber			= isnumber( _id );

		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'TranslateSteamID -> Any type of SteamID was not provided... id == None...' );
			return Steam.TranslateSteamIDDefault;

		## If we've used an entity...
		if ( not _id_isstring and not isnumber( _id ) ):
			raise SteamException( 'TranslateSteamID -> Entities ( Player or otherwise ) sre not supported in this version of Steam.TranslateSteamID...' );
			return Steam.TranslateSteamIDDefaults( True );


		## If we're looking at a bot via either using BOT, 0 or any non-player entity...
		## Note: Determine whether or not I should create the Player Object so it could be imported... Likely unnecessary.. -- or ( not _id_isstring and Steam.IsValidEntity( _id ) and not Steam.IsValidPlayer( _id ) )
		if ( ( _id_isstring and _id.upper( ) == 'BOT' ) or ( _id == 0 ) ):
			return Steam.TranslateSteamIDDefaults( True );

		## Helpers - Extract all numbers from the _id and determine if they are the same.. If they are then _id == SteamID64
		_steam_digits			= String.ExtractNumbers( _id );
		_is_len_same			= len( str( _id ) ) == len( str( _steam_digits ) );

		## If _id Input is SteamID3/U, ensure it is moved to correct var and calculate Steam32.
		if ( _id[ 1 : 2 ].upper( ) == 'U' ):
			## If UID was input, ensure it is moved to correct var and calculate Steam32.
			_steamid3U			= _id;

			## Remove first number from 1x after strip, before: [U:1:x] and convert to number.
			_id					= tonumber( string.sub( _steam_digits, 2 ) );

			## Even or odd?
			_is_even			= ( _id % 2 == 0 );
			_id					= math.floor( _id / 2 );

			## Set _steamid32 since we calculated it, and because we have the steamU we only need 64. Do it, and return.
			_steamid32 = 'STEAM_0:' + ( '0' if ( _is_even	) else '1' ) + ':' + _id;
			_steamid64 = Steam.SteamIDTo64( _steamid32 );


		else:
			## We're left with either 32 or 64 as options for input... A simple solution to verify 64bit input is by using the length of the string after removing all non-digits - if they remain the same, then the input is SteamID64..
			## If they are not the same length, generate 64 from 32 as it isn't 32... If they are the same len, then it is 64 so assign _id to 64... For 32, it's the opposite..
			## Because we are in else... It isn't U... So, because it isn't u, and len is dif, it must be 32 so generate 64 from 32... For 32, same... generate from 64 if len is same, otherwise keep 32 the same... which is _id from when I set at the start...
			_steamid64 = Steam.SteamIDTo64( _steamid32 ) if ( not _is_len_same ) else _id;
			_steamid32 = Steam.SteamIDFrom64( _steamid64 ) if ( _is_len_same ) else _steamid32;

			## Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
			if ( _steamid3U == '' ):
				_uidX = _steamid32[ 8 : 9 ]; #tonumber( string.sub( _steamid32, 9, 9 ) );
				_uidY = _steamid32[ 10 : ]; #tonumber( string.sub( _steamid32, 11 ) );
				if ( _uidY and _uidX ):
					_uidY = ( _uidY * 2 ) + _uidX;
					_steamid3U = '[U:1:' + _uidY + ']';



		## We're done..
		return _steamid32, _steamid64, _steamuid;