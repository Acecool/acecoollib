##
## Acecool's Python Library - Database Object - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- Importing AcecoolLib.database.sqlite' );

##
from .. import debug;

##
from .. import text as Text;
from ..example import GetExample;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Database );

	return _text;


##
##
##
class DatabaseBase( ):
	pass;
class Database( DatabaseBase ):
	##
	##
	##
	__name__ = 'Database';


	##
	## Module Example Information!
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		##
		_text = '\n';
		_text += '	Table of Contents:\n';
		_text += '\n';

		## _text += debug.Header( 'Database.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += debug.Footer( ) + '\n';


		_text += debug.Header( 'Database.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += debug.Footer( ) + '\n';


		## _text += debug.Header( 'Database.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += debug.Footer( ) + '\n';

		return _text;


	##
	##
	##
	def __init__( self, _name = 'Acecool', _path = '' ):
		self.connection		= sqlite3.connect( _path + _name + '.db' );
		self.cursor			= self.connection.cursor( );



	##
	##
	##
	def Query( self, _query, _values, _no_commit = False ):
		self.cursor.execute( _query, _values );

		if ( not _no_commit ):
			self.connection.commit( );


	##
	##
	##
	def Fetch( self, _table, _rows, _values = None ):
		## self.Query( 'CREATE TABLE IF NOT EXISTS ' + _table + ' ( ' + _rows + ' )' );
		## _values = ('RHAT',);
		self.cursor.execute( 'SELECT * FROM ' + _table + ' WHERE ' + _rows, _values );

		return self.cursor.fetchone( );


	##
	##
	##
	## def Update( self, _table, _where, _where_data, _values, _columns = 1 ):
	def Update( self, _table, _rows, _values = None ):
		self.cursor.execute( 'UPDATE ' + _table + ' SET ' + _rows + '', _values );
		self.connection.commit( );

		## c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases);
		## _column_qs = '?,' * _columns;
		## _column_qs = _column_qs[ : len( _column_qs ) - 1 ];
		## self.cursor.execute( 'UPDATE ' + _table + ' VALUES ( ' + _column_qs + ' ) WHERE ' + _where + ' = ' + _where_data + ' LIMIT 1', _values );


	##
	##
	##
	def CreateTableIfNotExists( self, _table, _rows, _values = None ):
		self.cursor.execute( 'CREATE TABLE IF NOT EXISTS ' + _table + ' ( ' + _rows + ' )' );
		self.connection.commit( );




	##
	##
	##
	def Close( self ):
		self.cursor.close( );