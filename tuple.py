
##
## Acecool's Python Library - Tuple Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.tuple' );

## Globals
from .__global__ import *;

##
from . import debug;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;

##
## Tuple Specific Data-Type Helpers
##
## class TupleBase( ):
## ## 	pass;
## class Tuple:
## 	##
## 	##
## 	##
## 	__name__ = 'Tuple';


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';


	## _text += debug.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += debug.Footer( ) + '\n';


	_text += debug.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += debug.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += debug.Footer( ) + '\n';

	return _text;


##
##
##
def Merge( _a, _b ):
	## _dict = _a.copy( );
	## _dict.update( _b );
	## return _dict;
	return _a + _b;


##
## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
##
def SetupQuickLookupTable( _data = None ):
	return List.SetupQuickLookupTable( _data );