##
## Acecool's Python Library - Queue Data-Type - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- Importing AcecoolLib.datatypes.queue' );

##
from .. import debug;

##
from .stack import Stack as Stack;
from ..example import GetExample;




##
## Module Example Information!


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';


	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Queue );

	return _text;


##
## A Queue is like a stack, but instead of only being able to remove the last element added which is ( First in Last out or Last In First Out ) we take items out in first come first serve or ( First In First Out ) order as you'd expect with a Queue such as a line at a Store checkout lane...
##
class Queue( Stack ):
	##
	##
	##
	def __example__( _depth = 1 ):
		return '';


	##
	##
	##
	def Pop( self ):
		return super( ).Pop( 0 );