##
## Acecool's Python Library - Stack Data-Type - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- Importing AcecoolLib.datatypes.stack' );

##
from .. import debug;
from ..example import GetExample;

##


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text = GetExample( Stack );

	return _text;


##
## A Stack is exactly as it sounds. Imagine a stack of papers on your desk and if you add a piece of paper, you can't grab any other piece of paper until the top one is removed. This is a stack.
##
class Stack( ):
	##
	##
	##
	stack_data = None;


	##
	##
	##
	def __example__( _depth = 1 ):
		return '';


	##
	##
	##
	def __init__( self, _stack = None ):
		##
		self.stack_data = [ ];

		##
		if ( _stack != None and IsList( _stack ) ):
			self.stack_data = _stack;


	##
	##
	##
	def Add( self, _value = None ):
		if ( _value != None ):
			_stack.append( );


	##
	##
	##
	def Pop( self ):
		return _stack.pop( len( self.stack_data ) - 1 );