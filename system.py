##
## Acecool's Python Library - System Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.system' );

## Globals
from .__global__ import *;

##
from . import debug;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';



	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';


	return _text;


## ##
## ##
## ##
## class System( ):


##
## Returns the Computer Operating System as Windows, Linux or Darwin for MAC
##
def GetOS( ):
	return platform.system( );


##
## Returns the Computer Name
##
def GetName( ):
	return platform.node( );


##
## Returns the Distribution information...
##
def GetDistribution( ):
	return platform.linux_distribution( );


##
## Returns the Processor Name
##
def GetProcessorName( ):
	return platform.processor( );


##
## Returns the Machine Type
##
def GetMachineType( ):
	return platform.machine( );


##
## Returns the Version
##
def GetVersion( ):
	return platform.version( );


##
## Returns the Release
##
def GetVersion( ):
	return platform.release( );


##
## Helper - Returns the path delimeter for the operating system... \\ for Windows, / for everything else...
##
def GetPathDelimiter( ):
	return ( '/', '\\' )[ System.GetOS( ) == 'Windows' ];