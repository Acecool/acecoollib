##
## Acecool Python Library - Examples App - Josh 'Acecool' Moser
##
## Note: When you run this package from Python, this should be the file which executes... Have it launch a helper app, or api guide, etc.. whatever...
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\n\t> AcecoolLib.examples' );


##
## Imports
##
from ..__global__ import *;

##
from .. import debug;
from .. import text as Text;
## from .. import dict;
## from .. import Examples;
## from .. import steam;
## from .. import string;

## from .. import Steam;

##
## print( '\tAcecoolLib.examples - Imported' );


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';


	return _text;


##
##
##
def GetExample( _class ):
	##
	_example		= getattr( _class, '__example__', None );
	_name			= getattr( _class, '__name__', 'AcecoolLib.__global__' );

	##
	_accessor_db	= getattr( _class, 'accessor_func_db', None );

	## If __example__ exists, then great..
	if ( _example != None ):
		if ( callable( _example ) ):
			return Text.DebugHeader( _name + ': Examples' ) + str( _example( ) ) + '\n------- AccessorFuncDB -------\n' + str( _accessor_db );
		else:
			return Text.DebugHeader( _name + ': Examples' ) + '\tExample Not Available\n\n\n';