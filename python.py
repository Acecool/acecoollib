##
## Acecool's Python Library - Python Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.python' );

## Globals
from .__global__ import *;

##
from . import debug;

##
import gc;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';



	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';




	return _text;


##
##
##
## class Python( ):


##
## Helper - Returns a list of the magic function names ready for use in tmLanguage or sublime-syntax files...
##
def GetMagicFunctionNames( _max_line_len = 100, _add_prefix_suffix_tabs = True ):
	_prefix	= '''	magic-function-names:
		- match: |-
				(?x)\b(__(?:
					'''
	_suffix	= '''
				)__)\b
			comment: these methods have magic interpretation by python and are generally called indirectly through syntactic constructs
			scope: support.function.magic.python'''

	## Accumulate the data...
	_data	= sorted( { ( _attr[ 2: -2 ] ) for _object in gc.get_objects( ) for _i, _attr in enumerate( dir( _object ) ) if _attr.startswith( '__' ) } );

	## Set up helpers.
	_words	= '';
	_len	= 0;

	## For each word
	for _word in _data: #.split( '|' ):
		## Add a pipe to the word
		_word = _word + '|';

		## Add the word
		_words = _words + _word;

		## Count the length of the word and add it to a counter
		_len = _len + len( _word );

		## If we've accurumulated more than _max_line_len characters since the last pip
		if ( _len > _max_line_len ):
			## Add a new line
			_words = _words + '\n' + ( '', '					' )[ _add_prefix_suffix_tabs ];

			## Reset the counter
			_len = 0;

	## Return the words without the last pipe...
	return ( '', _prefix )[ _add_prefix_suffix_tabs ] + _words[ : - 1 ] + ( '', _suffix )[ _add_prefix_suffix_tabs ];