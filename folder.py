##
## Acecool's Python Library - Folder Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.folder' );

## Globals
from .__global__ import *;

##
from . import debug;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;

##
## Folder Functions
##
## class FolderBase( ):
## 	pass;
## class Folder:
## 	##
## 	##
## 	##
## 	__name__ = 'Folder';


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';


	## _text += debug.Header( 'Folder.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += debug.Footer( ) + '\n';


	_text += debug.Header( 'Folder.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += debug.Header( 'Folder.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += debug.Footer( ) + '\n';


	return _text;


##
## Returns the proper path delimiter for the operating system...
##
def GetPathDelimiter( ):
	return System.GetPathDelimiter( );


##
## Returns the parent path - Note: If there is a filename attached, then that will be removed..
##
def GetParent( _path ):
	return os.path.basename( _path );


##
## Get Path Components - Returns full-path ( which is what is supplied ), the path without the file, the filename, the base filename, the file extension.
##
def GetComponents( _path, _snip_packages = False ):
	## The full path ( 'C:/Program Files/Sublime Text 3/sublime_text.exe' )
	## _path

	## Returns ( 'C:/Program Files/Sublime Text 3' )
	_path_base, _filename				= os.path.split( _path );

	## Returns ( 'sublime_text', '.exe' )
	_file_base, _file_ext				= os.path.splitext( _filename );

	## Returns ( 'exe' )
	_file_ext							= _file_ext[ 1 :  ] if ( _file_ext[ 0 : 1 ] == '.' ) else _file_ext;

	## If path starts with Packages/ then remove it. Otherwise return the original path.
	if ( _snip_packages ):
		_path[ len( 'Packages' + System.GetPathDelimiter( ) ) : ] if ( _path.startswith( 'Packages' + System.GetPathDelimiter( ) ) ) else _path

	##
	return _path, _path_base, _filename, _file_base, _file_ext;


##
## Helper - Returns whether or not the file / folder exists by checking the timestamp...
##
def Exists( _path ):
	return File.GetTimeStamp( _path ) > -1;


##
## Creates all of the important folders in the User folder and either generates files, copies them, etc... Then the plugin can be 1 folder and install outwards to what is needed.. including replacing CodeMap files...
##
def Create( *_paths ):
	##
	if ( _paths == None or not len( _paths ) > 0 ):
		print( 'AcecoolLib -> Folder -> Create -> Can not create folder - _paths is None or has no entries.' );
		return False;

	##
	_errors = '';

	##
	for _path in _paths:
		## print( 'AcecoolLib -> Folder -> Create -> Trying to create file: ' + _path );
		if not len( _path ) > 0:
			continue;

		## Make sure the folder structure exists..
		try:
			## print( 'AcecoolLib -> Folder -> Create -> Trying to create file...' );
			## Attempt to create the folder
			os.makedirs( _path );

			## print( 'AcecoolLib -> Folder -> Create -> File created successfully!!!' );
		except OSError as _error:
			_errors += str( _error ) + '\n';
		except:
			_errors += 'Other Exception!\n';
			## if e.errno != errno.EEXIST:
		##	raise;

	if ( _errors == '' ):
		return True;
	else:
		print( 'AcecoolLib -> Folder -> Create -> Errors: ' + _errors );
		return False;


##
##
##
def ListFiles( _path, _limit_endswith = None ):
	##
	_list = os.listdir( _path );

	## If we want to limit the list then we need to
	if ( _limit_endswith != None ):
		## Create a new list
		_new = [ ];

		## Process every file in the original list
		for _file in _list:
			## And if the file ( case insensitive ) ends with our limiting string then
			if ( _file.lower( ).endswith( _limit_endswith.lower( ) ) ):
				## We add it to the new list
				_new.append( _file );

		## and then we return the new list...
		return _new;

	## Return the list of all files
	return _list;



##
##
##
def Rename( _path, _new ):
	pass;


##
##
##
def Remove( _path ):
	return File.Remove( _path );


##
##
##
def SafeName( _name = '' ):
	return File.GetSafeName( _name );


##
##
##
def GetSafeName( _name = '' ):
	return File.GetSafeName( _name );


##
##
##
def GetExt( _name = '' ):
	return File.GetExt( _name );