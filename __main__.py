##
## Acecool Python Library - Runtime - Josh 'Acecool' Moser
##
## Note: When you run this package from Python, this should be the file which executes... Have it launch a helper app, or api guide, etc.. whatever...
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
## import function


##
print( '\t- AcecoolLib.__main__' );

## ## Globals
## from .__global__ import *;

##
from .. import debug;

## ##
## from . import Acecool;

## ##
## from . import Dict;

## ##
## from . import Examples;

## ## Steam
## from . import Steam;

## ## String / Text
## from . import String;
## ## from . import Text;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';




	return _text;



##
## If we are trying to execute this package from python, give them something different...
##
if ( __name__ == '__main__' ):
	##
	print( '\t- Executing __main__' );

	print( '' );
	print( '##' );
	print( '## Acecool\'s Python Library - Josh \'Acecool\' Moser' );
	print( '##' );
	print( '' );
	print( 'Welcome to the Interactive Library Developer Guide' );
	print( '' );
	print( '\tThe guide hasn\'t been completed yet!' );
	print( '' );
	print( '\tUntil the guide has been completed, as every library package contains a \n\tgenerate_example helper-function, these will be called by order of package, \n\tand alphabetically. This will show proper usage with real-world examples and \n\tproper output.' );
	print( '' );
	print( 'I hope this helps, enjoy!' );
	print( '' );
	print( '' );
	print( '' );




## ## Examples.Show( );

## print( Examples.Show( ) );