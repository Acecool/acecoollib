##
## Acecool's Python Library - Table Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.table' );

## Globals
from .__global__ import *;

##
from . import debug;

##
from . import list as List;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;


## ##
## ## Generic Table Helpers
## ##
## ## class TableBase( ):
## ## 	pass;
## class Table:
## 	##
## 	##
## 	##
## 	__name__ = 'Table';


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';


	## _text += debug.Header( 'Table.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += debug.Footer( ) + '\n';


	_text += debug.Header( 'Table.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += debug.Header( 'Table.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += debug.Footer( ) + '\n';

	return _text;


##
## Takes a table of keys and a table of values and turns them into a single table of keys = values...
##
def MapKeysToValues( _keys, _values ):
	if ( not ( IsTable( _keys ) and IsTable( _values ) and len( _keys ) == len( _values ) ) ):
		return False;

	##
	_map = { };

	##
	for _key, _value in enumerate( _keys ):
		_map[ _value ] = _values[ _key ];


	return _map;


##
## Takes a mapped table of keys to values and adds additional elements to it - uses a copy of it to prevent changing the original table...
##
def MapKeysValuesToExtra( _map, _extras = None ):
	##
	_map = _map.copy( );

	##
	if ( _extras != None ):
		_map.update( _extras );

	##
	return _map;


##
##
##
def Merge( _a, _b ):
	##
	if ( type( _a ) != type( b ) ):
		raise Exception( 'MergeTypeException: Both Tables need to be the same type: Tuple, List, or Dict - They are: ' + str( type( _a ) ) + ' and ' + str( type( _b ) ) );

	##
	if ( isinstance( _a, { } ) ):
		return Dict.Merge( _a, _b );

	##
	if ( isinstance( _a, [ ] ) ):
		return List.Merge( _a, _b );

	##
	if ( isinstance( _a, ( ) ) ):
		return Tuple.Merge( _a, _b );

	##
	raise Exception( 'MergeException: The data is not a table type Tuple, List or Dict... Can not merge!' );


##
##
##
def GetValue( _args, _args_count, _index, _default ):
	_arg = ''; ## _index += 1;

	if ( _default != '' or str( _default ) != 'nil' ) and _arg == '':
		_arg = _default;
	if ( _args_count > _index and str( _args[ _index ] ) != 'nil' ):
		_arg = str( _args[ _index ] );

	return _arg;


##
## Takes a Dictionary ( uses { } ) and returns a tuple ( uses [ ] ) map which is sorted - basic index values convert to string key so
##
## TODO: Create this...
##
## ## Usage:
##
## _dict = { "My":{ "z":True, "a":False }, "ABC":{ "z":False, "a":True } }
##
## ## This becomes [ 1: "ABC", 2: "My" ]
## _tuple_map = Table.DictToTupleMap( _dict, SORT_METHOD_A_Z )
##
## ## So, in our loop we can use range which does 1, then 2 - it means we need a lengthier call.. Or we can use a for-each method...
## for _i in range( len( _tuple_map ) ):
##	## _tuple_map for 1 is ABC so we convert it... - Returns ABC as { "z":False, "a":True } then My as { "z":True, "a":False }
##	_dict_data = _dict[ _tuple_map[ _i ] ]
##
##
def DictToTupleMap( _dict ):
	## Create our Tuple
	_tuple = [ ];

	## Go through the dictionary one at a time...
	for _key, _value in enumerate( _dict ):
		print( 'Tuple > Dict[ ' + str( _key ) + ' ] = ' + str( _value ) );



##
## Redesign of the code below...
##
def FromCodeDefinitionToComponents( _code, _args_delimeter = ',', _start_paren_char = '(', _finish_paren_char = ')' ):
	## Determine how many parens are there...
	_start_char_count = _code.count( _start_paren_char );
	_end_char_count = _code.count( _finish_paren_char );

	##
	## Notes: If function CALL then preserve args ( quotes, etc.. except white-space between args ) if definition then allow skip chars...
	##

	## If we have the same number of each...
	if ( _start_char_count == _end_char_count ):
		## If we have 1 of each then
		if ( _start_char_count == 1 ):
			## Because there is 1 of each we can break it down into component form by means of <FUNC/CLASS-COMBO> <NAME><START><ARGS><END> or <NAME> = <FUNC/CLASS-COMBO><START><ARGS><END> by simply splitting the string up...

			## Just in case we're not starting from 0...
			_begin = String.indexOf( _start_paren_char, _text );
			_finish = String.indexOf( _finish_paren_char, _text );


		## More than one of each with the same count - we need to determine scope...
		else:
			## We have to process each character... Depth 1 means function.. Depth > 1 means part of an arg so preserve everything... It can be split using another call... Everything outside of <START> and <END> is left to be the name / id... we only break it into component form, nothing else..
			print( );
	else:
		## We have an odd number of parens - ie
		##
		if ( _start_char_count > 1 or _end_char_count > 1 ):
			print( );


##
## Table.FromFunctionArgsString - Function to take a string of function arguments and turn it into a table
##
## TODO: Change so depth 0 chars aren't used, then primary_depth 1 args are taken, depth 2 is returned in a different table, 3 and so on... but 0 is ignored... { }s and [ ]s count as a different type of depth ( table, or so on )..
## Note: Spaces should be allowed in after the first char and before the last to allow for x = y default arg value systems...
##
def FromFunctionArgsString( _text ):
	if not Util.isset( _text ):
		return '', 0;

	##
	## Config
	##

	## Chars and Cap ( Starting and Ending ) Chars we want to skip...
	_cfg_skip_chars = { };
	_cfg_skip_cap_chars = { };

	## Chars to skip at ALL times regardless of location within the string...
	## _cfg_skip_chars[ "{" ] = True;
	## _cfg_skip_chars[ "}" ] = True;

	## Chars we want to skip regardless of where they are in the string...
	_cfg_skip_cap_chars[ ' ' ] = True;
	_cfg_skip_cap_chars[ "'" ] = True;
	_cfg_skip_cap_chars[ '"' ] = True;

	## Tracks the current "Depth" based on brackets / braces
	_depth			= 0;

	## Tracks the number of words we've accumulated in our table
	_args_count		= 0;

	## Tracks the number of chars in the current word
	_char_count		= 0;

	## Holds the current word until it is ready to be imploded as a string and inserted into our words table..
	_arg			= [ ];
	_arg_raw		= [ ];

	## Holds all of the arguments we've accumulated
	_args			= [ ];
	_args_raw		= [ ];

	## Current Arg Flags
	_arg_isstring	= False;

	## Just in case we're not starting from 0...
	_begin = String.IndexOf( '(', _text );
	_finish = String.IndexOf( ')', _text );

	## Function declarations can be an argument - make sure these aren't included...
	_begin_func = String.IndexOf( 'function(', _text );
	## _finish = Logic.TernaryFunc( _finish <= len( _text ), _finish, len( _text ) );

	## print( "Debugging: _text = '" + _text + "' / _begin = " + str( _begin ) + " / _finish = " + str( _finish ) );

	## Clean
	## if ( _begin > 0 ):
		## if ( _begin_func > 0 ):
		##	_text = String.SubStrEx( _text, _begin, _begin_func );
		##	_text = _text + 'FUNC_REF';
		## else:
	_text = String.SubStrEx( _text, _begin, _finish );

	## if ( ):
	##	_text = String.EndChars( _text, _finish );

	## trick to remove special-case of trailing chars
	for _key, _char in enumerate( _text + "," ):
		## Increment the key..
		## _key += 1;

		## Manage Brace Depth
		if _char == "{" or _char == "(" or _char == "[":
			_depth += 1;
		elif _char == "}" or _char == ")" or _char == "]":
			_depth -= 1;

		## If we've encountered a char we should skip, then skip it..
		if _cfg_skip_chars.get( _char, False ) == True:
			## _arg_raw.append( _char );
			_arg_raw.append( '*' );
			continue;

		## If we've reached a comma.. we turn out current work collected chars into an actual word for our table and reset the collector for the next word...
		if _char == "," and _depth < 1:
			## If We're not in a bracket
			if _depth == 0:
				## Assemble the word and strip left / right spaces...
				_arg = "" . join( _arg ).strip( );
				_arg_raw = "" . join( _arg_raw ).strip( );

				## Add the current word to our table of words
				_args.append( _arg );
				_args_raw.append( _arg_raw );

				## Increment our counter
				_args_count += 1;

				## Reset the word so we can begin assembling a new word
				_arg = [ ];
				_arg_raw = [ ];

				## Reset Arg Flags
				_char_count = 0;
				_arg_isstring = False;

		## else - ie If the char isn't a comma or the depth is >= 1 - meaning commas will only be added if depth is 1 or more and other data will be added all the time
		else:
			## Helpers... _head_char == first char of arg, _tail_char == last char of arg, _nil_depth == No Depth, _skip_char == Bad Character ( Determine whether or not to skip )
			_head_char = _char_count == 0;
			_tail_char = _text[_key:_key + 1] == ',';
			_nil_depth = _depth == 0;
			_skip_char = _cfg_skip_cap_chars.get( _char, False ) == False;

			## Helpers so we don't need to repeat _arg append... In short, if we're on a cap-character which is bad skip it...
			## If head_char or nil depth or tail char encountered... or not
			## if ( ( _head_char or _nil_depth and _tail_char ) or not ( _head_char or _nil_depth and _tail_char ) ) and _skip_char or not _nil_depth:
			if _skip_char or not _nil_depth:
				## Add the current character to our word string
				_arg.append( _char );
				_char_count += 1;

			## if _char == '"' or _char == "'" or _skip_char or not _nil_depth:
			## if not _nil_depth:
			_arg_raw.append( _char );

	return _args, _args_count, _args_raw;


##
##
##
def KeySet( *_varargs ):
	##
	_tab = { };

	##
	for _value in _varargs:
		## print( ' >>> ' + str( _value ) );
		_tab[ str( _value ) ] = True;

	##
	return _tab;


##
##
##
def HasKeySet( _tab, _key ):
	return _tab.get( str( _key ), False );

	##
	## if ( _key != None and _tab[ str( _key ) ] == True ):
		## return True;

	## return False;


##
## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
##
def SetupQuickLookupTable( _data = None ):
	if ( IsDict( _data ) ):
		return Dict.SetupQuickLookupTable( _data );
	if ( IsList( _data ) or IsTuple( _data ) or IsTableTypeSet( _data ) ):
		return List.SetupQuickLookupTable( _data );