##
## Acecool's Python Library - TimeKeeper System - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.time.timekeeper' );

##
from .. import debug;
from ..example import GetExample;

##



##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_text = '';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	##
	_text += GetExample( TimeKeeper );

	return _text;


##
## Stop Watch - Timing System
##
class TimeKeeperBase( ):
	pass;
class TimeKeeper( TimeKeeperBase ):
	##
	##
	##
	__name__ = 'TimeKeeper';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += debug.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += debug.Footer( ) + '\n';


		_text += debug.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += debug.Footer( ) + '\n';


		## _text += debug.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += debug.Footer( ) + '\n';

		return _text;


	##
	## One-Line Helpers
	##

	## Grab actual epoch timestamp from time... Should be in seconds since...
	def TimeStamp( self = None ):	return time.time( );

	## Grab actual epoch timestamp from time... Should be in Milli-Seconds since...
	## def TimeStampMS( self ):		return time.time( );

	## Grab actual epoch timestamp from time... Should be in Micro-Seconds since...
	## def TimeStampMicro( self ):		return time.time( );

	## Returns the length of time which has passed since the time keeper has started... if it hasn't then it'll use now as start and end time == 0
	def Len( self ):				return self.TimeEnded( ) - self.TimeStarted( );

	## Returns the Delta as a string
	def LenStr( self ):				return str( self.Len( ) );

	## Returns the Delta as a string
	def Elapsed( self ):			return self.Len( );

	## Returns the Delta as a string
	def ElapsedStr( self ):			return self.LenStr( );

	## Returns whether or not a Duration exists
	def HasDuration( self ):		return ( self.__duration__ == None or not IsNumber( self.__duration__ ) or IsNumber( self.__duration__ ) and self.__duration__ <= 0 );

	## Returns the TimeKeeper Duration - if one was given... otherwise returns the length of time which has passed since time has started...
	def Duration( self ):			return TernaryFunc( self.HasDuration( ), self.__duration__, self.Len( ) );

	## Returns the Delta as a string
	def DurationStr( self ):		return str( self.Duration( ) );

	## Returns the Delta as a string
	def DeltaStr( self ):			return str( self.Delta( ) );

	## Returns the timestamp as a string
	def TimeStampStr( self ):		return str( self.TimeStamp( ) );

	## Returns whether or not the timekeeper tracking has started...
	def HasStarted( self ):			return ( IsNumber( self.__timestamp_start__ ) );

	## Returns the Time Started or NOW
	def TimeStarted( self ):
		if ( self.HasStarted( ) ):
			return self.__timestamp_start__;

		return self.TimeStamp( );

	## Returns whether or not the timekeeper tracking has end...
	def HasEnded( self ):			return ( IsNumber( self.__timestamp_end__ ) );

	## Returns the Time Ended or NOW
	def TimeEnded( self ):
		if ( self.HasEnded( ) ):
			return self.__timestamp_end__;

		return self.TimeStamp( );

	##
	## def DeltaStr( self ):			return str( self.Delta( ) );

	##
	## def DeltaStr( self ):			return str( self.Delta( ) );


	##
	## On Initialization - if duration is given it is very useful for progress-bars, etc...
	## Task: Add Duration update system where I can time how long something has taken, and return how many more somethings it has to do to live-calculate how long the total somethings should take to process for a live-progressbar instead of timed...
	##
	def __init__( self, _duration = None, _name = None ):
		##
		self.__timekeeper_name__	= _name;
		## Sets up the duration var
		self.__duration__			= TernaryFunc( ( _duration != None and IsNumber( _duration ) and _duration > 0 ), _duration, None );

		## Set up when the timekeeper was created
		self.__timestamp_created__	= self.TimeStamp( );

		## Set up when the timekeeper started
		self.__timestamp_start__	= None;

		## End is set when Stop is called
		self.__timestamp_end__		= None;

		## Delta = last time GetDelta was called...
		self.__timestamp_delta__	= self.__timestamp_start__;

		## Save is used for XCM - Deprecated with Delta added...
		## self.__timestamp_save__		= None;


	##
	##
	##
	def __str__( self ):
		## String.FormatColumn( _width = 25, _text = '', *_varargs );
		## String.FormatSimpleColumn( _width = 25, *_varargs );

		##
		_text = ''

		##
		if ( self.Name( 50 - 17 ).strip( ) != 'N/A' ):
			_text += String.FormatSimpleColumn( 50, '' + self.Name( 50 - 17 ) + ' - ' );
		else:
			_text += 'N/A - ';

		##
		if ( self.DeltaStr( ) != '0.0' ):
			_text += String.FormatSimpleColumn( 20, 'Delta: ' + self.DeltaStr( ) + ' ' );

			##
		if ( self.HasStarted( ) ):
			_text += String.FormatSimpleColumn( 20, 'Elapsed: ' + self.LenStr( ) + ' ' );

		##
		return '[ TimeKeeper -> ' + _text + ' ]; ';


	##
	## Clearing of values for shutdown
	##
	def __unload__( ):
		self.__duration__			= None;
		self.__timestamp_start__	= None;
		self.__timestamp_end__		= None;
		self.__timestamp_delta__	= None;
		## self						= None;


	##
	## Returns the name of the TimeKeeper so we can keep track of differently named timekeepers...
	##
	def Name( self, _max = -1 ):
		##
		_text = TernaryFunc( IsString( self.__timekeeper_name__ ), self.__timekeeper_name__, 'N/A' );

		##
		if ( _max >= 0 ):
			return _text[ : _max ];

		##
		return _text;


	##
	## Starts the timer by setting a start-time - Returns self so I can create the timestamp, start it all in the same line, or chain events...
	##
	def Start( self ):
		##
		self.__timestamp_start__	= self.TimeStamp( );

		##
		return self;


	##
	## Stops the timer by setting an end time - Returns self so I can chain events...
	##
	def Stop( self, _update_delta = True ):
		##
		self.__timestamp_stop__		= self.TimeStamp( );

		##
		if ( _update_delta ):
			self.Delta( True );

		##
		return self;


	##
	## Returns the Delta Time since the last time this function was called
	##
	def Delta( self, _update = True ):
		_delta = TernaryFunc( self.__timestamp_delta__ == None, self.TimeStamp( ), self.__timestamp_delta__ )
		_time = self.TimeStamp( );

		if ( _update ):
			self.__timestamp_delta__ = self.TimeStamp( );

		## Return the ( NOW - LAST ) time for ▲
		return _time - _delta;


	##
	## Helper - Returns how long it took to execute / parse, etc.. the output generation so I can time caching system vs parsing..
	##
	def GetTimeDelta( self, _start = None, _end = None ):
		## If Start wasn't provided as an argument..
		if ( _start == None ):
			_start = self.__timestamp_start__;

		## If _end wasn't provided as an argument...
		if ( _end == None ):
			## If __timestamp_end__ isn't set, we grab the current timestamp... or _start is identical to _end... then we must be grabbing a STEP timestamp... recapture the timestamp for this time but don't update end...
			if ( self.__timestamp_end__ == None or _start == self.__timestamp_end__ ):
				## For loading the cached file..
				_end = self.GetTimeStamp( );
			else:
				## Otherwise we use it..
				_end = self.__timestamp_end__;

		## Return the DeltaTime...
		return _end - _start;