##
## Acecool's Python Library - Cooldown System - Josh 'Acecool' Moser
##
## Note: Decide on how to incorporate this... Either as an object accessor injection which wouldn't be difficult and would be useful.. but may require instance accessor as well... or instance injection.. Or, simple object.. ie: class blah( ):\n\tCooldown = Cooldown( );, and then self.Cooldown.Has( )... etc..
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task: Port Cooldown system
## Task: Choose whether to use an AccessorFunc Injection System - ie: Object.HasCooldown( _name, _time, _override )... Or, an Object Style system... _obj = Object( ); _obj.Cooldown = Cooldown( ); _obj.Cooldown.HasCooldown( _name, _time, _override );.... I like the injection style and could easily do __Cooldown = Cooldown( ObjectBase ); for injecting...
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.time.cooldown' );

##
from .. import debug;
from ..example import GetExample;

##


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Cooldown );

	return _text;


##
## Cooldown Object..
##
class CooldownBase( ):
	pass;
class Cooldown( CooldownBase ):
	##
	##
	##
	def __example__( _depth = 1 ):
		return '';
















## //
## // Simple Cooldown Helper for Entities - Networked Version ( Most cases you won't need networking for cooldowns - but it can be useful ) - Josh 'Acecool' Moser
## //
## // If using as stand-alone then it requires:  https://www.dropbox.com/s/sdi8f1k5tkh7oqy/sh_basic_networking_and_data_system.lua?dl=0
## //


## //
## // Task List
## //
## // Task: Determine whether or not using a timekeeper is worth it for this system... With the addition of new functions, it'd make this more complex, but a lot more usable... and it'd help standardize the timekeeper system...
## // Task: Note: It is worth it for keeping track of time fractions, etc.... Although, I may end up writing a simplified timekeeper system with JUST time calculations, and timekeeper use it... I think I've already started it by extended the time system... I could add time.SetStart, SetDuration, type calls like surface.SetDrawColor, etc...
## // Task:
## // Task:
## // Task:
## // Task:
## //


## //
## // Task List
## //
## // Task:
## // Task:
## // Task:
## // Task:
## // Task:
## // Task:
## //

## //
## // Configuration for this script...
## //

## // Default Cooldown Time - In SECONDS!
## COOLDOWN_DEFAULT_DURATION = 3;

## // How many percent ( this is multiplied by the duration of the cooldown ) should we use as a cap? So if we use CustomFraction for Fade In / Out - how large of a chunk should we use? 0.25 of 3 is 0.75 seconds - 0.166667 is 0.5 seconds..
## // Basically - if a _time isn't given for the custom Fraction builder then use this percent OF total duration... Also, it is 1 for time remaining custom to 0, and for elapsed custom it is 0 to 1.... and they start from beginning for the elapsed custom fraction and from the end for the remaining fraction - I will add one which starts in the center and will have 0 to 1 and 1 to 0 options soon!
## COOLDOWN_DEFAULT_CUSTOM_CAP_MOD	= 0.15

## // Should we force the unique names to be lowercase? This can help prevent issues with upper / lower typos but can cause problems of some being overwritten - ie ARMOR as a constant vs armor as the current value... In other words, if poor variable name choices are made, it can cause issues which is why it is set as false by default!
## COOLDOWN_FORCE_LOWERCASE = false;

## // The Cooldown category ( so the networking names don't collide...
## FLAG_COOLDOWN = FLAG_COOLDOWN || 'Cooldowns';
## FLAG_TIMEKEEPER = FLAG_TIMEKEEPER || 'Timekeepers';

## // Entity Meta-Table...
## local META_ENTITY = META_ENTITY || FindMetaTable( 'Entity' );


## //
## // Example System
## //


## //
## // Runs the Cooldown / Micro-Caching Example
## //
## concommand.Add( "example_ent_cooldown", function( _p, _cmd, _args, _args_text )
## 	// Grab the first arg we'll use as duration
## 	_duration = _args[ 1 ];

## 	// Convert it to a number...
## 	_duration_num = tonumber( _duration );

## 	// If the text entered is true then convert it to a boolean...
## 	if ( _duration == 'true' ) then _duration = true; end

## 	// If we didn't set it to true above, and if the value is a valid number, the length is the same or the length is more than 0 then set duration to the number we created from it...
## 	if ( !isbool( _duration ) && isnumber( _duration_num ) && string.len( _duration_num ) == string.len( _duration ) || string.len( _duration_num ) > 0 ) then _duration = _duration_num; end

## 	// If _duration doesn't set or using /?
## 	if ( !_duration || _duration == '/?' ) then
## 		print( '[ Cooldown Example ] Call example_ent_cooldown <Boolean or Number> ( without < >s, spaces, etc... - using True will set the cooldown to 3 seconds / default.. the actual value which can contain decimals or nothing meaning the HasCooldown will simply act as the HasCooldown function without creating if it does not exist... )' );
## 	end

## 	_p:GenerateExampleCooldown( _duration );
## end );


## //
## // Example
## //
## function META_ENTITY:GenerateExampleCooldown( _duration )
## 	// Rewrite _duration so it's the value we want...
## 	local _duration = ( ( isnumber( _duration ) && _duration || isbool( _duration ) ) && COOLDOWN_DEFAULT_DURATION || nil );

## 	// Return the values -
## 	//	Does a cooldown exist ( If one is created, ie second var is set to true or a positive number and one does NOT exist when called then it'll return false the first time called
## 	//		so code can execute and true the next if still active to prevent it from being called again ),
## 	//	the time in seconds remaining on the cooldown or 0, and the data table...
## 	// Note: The call will be checking if a cooldown named cooldown_example ( spaces can exist ) - if _duration is set as a number then that number is used to create a cooldown with that time limit... if set as a boolean then 3 seconds will be used or default value if changed... otherwise it'll be nil meaning it'll only be executed as a Getter without creating a cooldown even if one doesn't exist..
## 	// Note: I don't use _duration here because it would otherwise create a replacement meaning the if statement example wouldn't work - I call it here though so we can get all of the returns - by default... in an if statement only the first return matters..
## 	local _exists, _time_remaining, _data = _p:HasCooldown( 'cooldown_example' )

## 	// Some helpers... for the in-short description...
## 	local _target_str		= tostring( _p );
## 	local _time_str			= tostring( CurTime( ) );
## 	local _created_str		= tostring( !_exists && _duration && _duration > 0 );
## 	local _default_str		= tostring( COOLDOWN_DEFAULT_DURATION );
## 	local _duration_str		= tostring( ( isnumber( _duration ) && _duration || 0 ) );
## 	local _exists_str		= tostring( _exists );
## 	local _remaining_str	= tostring( _time_remaining );

## 	// Output the data to console... so you can see what's happening...
## 	print( '[ Cooldown Example ]' );
## 	print( 'Target Entity: ' .. _target_str );
## 	print( 'Time Called: ' .. _time_str );
## 	print( 'Has Cooldown: ' .. tostring( _exists ) );
## 	print( 'Cooldown Created: ' .. _created_str );
## 	print( 'Target Entity: ' .. _target_str );
## 	print( 'Duration Provided: ' .. _duration_str );
## 	print( );

## 	// Output the data to console... so you can see what's happening...
## 	print( '[ Cooldown Example - If BLOCK Test ]' )
## 	// If statement...
## 	if ( _p:HasCooldown( 'cooldown_example', _duration ) ) then
## 		print( 'COOLDOWN EXAMPLE IF STATEMENT CODE IS RUNNING!!!!' );
## 		print( 'The code inside the IF BLOCK has executed properly - if _duration is set, then if this example is called within the duration ( ' .. _default_str .. ' seconds if _duration is True, ie default time period, or ' .. _duration_str .. ' seconds if set.. ) then this will be missing from the output...' );
## 		print( 'COOLDOWN EXAMPLE IF STATEMENT CODE IS RUNNING!!!!' );
## 		print( );
## 	end
## 	print( '[ Cooldown Example - END If BLOCK Test ]' );
## 	print( );
## 	print( 'In Short:' );
## 	// The cooldown with the name "cooldown_example" exists meaning the code in an if block DID NOT EXECUTE
## 	print( 'The cooldown with the name "cooldown_example" ' .. ( ( _exists && 'exists' || 'does not exist or expired' ) ' meaning the code in the IF BLOCK ' .. ( _exists && 'DID NOT EXECUTE' || 'EXECUTED and is visible above these lines of code' ) .. ' ' .. ( _created && 'Also, the Cooldown WAS CREATED so the next time the HasCooldown line is called it will return true' || ( _exists && 'Because a cooldown exists, one was NOT created with the HasCooldown call' || '' ) ) .. ' for the entity ' .. _target_str .. '! Exists or does not exist with time remaining if exists - if exists then a new one did not get created otherwise go that round...'  ) .. ' ' );
## 	print( );
## 	print( 'Notes' );
## 	print( 'Has Cooldown: - if false and a time was provided then Cooldown Created will be true - the code within the code block WILL RUN the first time, but when HasCooldown is called again while it has not expired, the code in the code block will NOT RUN... This saves you a lot of time!'  );
## 	print( );
## 	print( 'Duration Provided: If number it is in seconds then that time will be used as the duration if creating the cooldown... if nil then the function will only act as a Getter and will NOT create the cooldown if it does not exist...  you would need to create it manually adding a line of code instead of simply using if ( _p:HasCooldown( "name", 3 ) ) then code end -- you would have to add _p:CreateCooldown( "name", 3 ); inside of the code area which may not seem to be a lot, but it adds up...' );

## end


## //
## // The TimeKeeper Plugin Code
## //


## // //
## // //
## // //
## // function META_ENTITY:HasTimekeeper( _name, _duration, _override )
## // 	//
## // 	local _timekeeper = self:GetTimekeeper( _name );

## // 	// Does cooldown data exist?
## // 	local _isvalid = _timekeeper != nil;

## // 	// Is the cooldown expired, if we have cooldown data...
## // 	local _time_remaining = _isvalid && _timekeeper:GetTimeRemaining( );

## // 	// Does a valid cooldown exist? We need cooldown data and it can't be expired...
## // 	local _bHasCooldown = ( _isvalid && _time_remaining > 0 ) && true || false;

## // 	// Has a valid cooldown expired - ie does data still exist in the data-table and the expiration has passed ( we don't need data to render a false / non-existent or 0 return for HasCooldown, and GetTimeRemaining, etc... )
## // 	local _bHasCooldownExpired = ( _isvalid && _time_remaining == 0 ) && true || false;

## // 	// Determines whether or not we create a cooldown - If a duration is set as a number ( positive ) or true ( uses default 3 second time ) and a cooldown doesn't exist ( because it expired ) in its place - OR it does and _override is set to non False / nil value ( to penalize those who spam )....
## // 	local _bCreateCooldown = ( ( isnumber( _duration ) || _duration == true ) && ( !_bHasCooldown || _override ) ) && true || false;

## // 	// If the cooldown is expired, and data exists - we remove it to keep the table / memory clear!!! We don't do it on expiry because that'd require a Think / Tick code ( High cost and unnecessary ) or scheduled timer ( which can fail )
## // 	if ( _bHasCooldownExpired ) then self:RemoveCooldown( _name ); end

## // 	// Create the cooldown if _duration was set and we either have no cooldown registered / active under the name supplied, or we want to _override the existing cooldown to reset the time...
## // 	if ( _bCreateCooldown ) then self:SetCooldown( _name, ( ( _duration > 0 ) && _duration || COOLDOWN_DEFAULT_DURATION ) ); end

## // 	// For Debugging
## // 	-- print( "Cooldown Data: ", _name, _duration, _bHasCooldown, _time_remaining );

## // 	// Return the data prior to created so that the if statement will not fail...
## // 	return _bHasCooldown, _time_remaining, _timekeeper;

## // 	//
## // 	if ( !_timekeeper || _ ) then
## // 		if ( _duration ) then
## // 			self:AddTimekeeper( _name, _duration, Acecool.C.timekeeper:New( _duration );
## // 		end

## // 		return false, nil;
## // 	end


## // end


## // //
## // //
## // //
## // function META_ENTITY:GetTimekeeper( _name )
## // 	return self:GetFlag( _name, nil, true, FLAG_TIMEKEEPER );
## // end




## //
## // The Plugin / Code...
## //


## //
## // Returns whether or not a cooldown exists; If _duration is set then if will create the Cooldown if it didn't exist!
## //
## // Returns whether or not a cooldown exists; If _duration is set then if will create the Cooldown if it didn't exist but will still return false so if ( !cooldown ) then return; end line can
## //	be used to allow the first action outside of cooldown to continue and if a cooldown exists, it prevents it. if _overwrite isset then regardless of whether or not a cooldown exists, it'll
## //	be created, overwriting the old one with the new data, which is useful for chat spam, etc.. where if you display a warning, you can reset the time as punishment ( for repeat offenders ).
## //
## function META_ENTITY:HasCooldown( _name, _duration, _override )
## 	// Grabs any data pertaining to this cooldown...
## 	local _data = self:GetCooldown( _name );

## 	// Does cooldown data exist?
## 	local _bHasCooldownData = ( _data && istable( _data ) ) && true || false;

## 	// Is the cooldown expired, if we have cooldown data...
## 	local _timeRemaining = self:GetCooldownTimeRemaining( _name );

## 	// Does a valid cooldown exist? We need cooldown data and it can't be expired...
## 	local _bHasCooldown = ( _bHasCooldownData && _timeRemaining > 0 ) && true || false;

## 	// Has a valid cooldown expired - ie does data still exist in the data-table and the expiration has passed ( we don't need data to render a false / non-existent or 0 return for HasCooldown, and GetTimeRemaining, etc... )
## 	local _bHasCooldownExpired = ( _bHasCooldownData && _timeRemaining == 0 ) && true || false;

## 	// Determines whether or not we create a cooldown - If a duration is set as a number ( positive ) or true ( uses default 3 second time ) and a cooldown doesn't exist ( because it expired ) in its place - OR it does and _override is set to non False / nil value ( to penalize those who spam )....
## 	local _bCreateCooldown = ( ( isnumber( _duration ) || _duration == true ) && ( !_bHasCooldown || _override ) ) && true || false;

## 	// If the cooldown is expired, and data exists - we remove it to keep the table / memory clear!!! We don't do it on expiry because that'd require a Think / Tick code ( High cost and unnecessary ) or scheduled timer ( which can fail )
## 	if ( _bHasCooldownExpired ) then self:RemoveCooldown( _name ); end

## 	// Create the cooldown if _duration was set and we either have no cooldown registered / active under the name supplied, or we want to _override the existing cooldown to reset the time...
## 	if ( _bCreateCooldown ) then self:SetCooldown( _name, ( ( _duration > 0 ) && _duration || COOLDOWN_DEFAULT_DURATION ) ); end

## 	// For Debugging
## 	-- print( "Cooldown Data: ", _name, _duration, _bHasCooldown, _timeRemaining );

## 	// Return the data prior to created so that the if statement will not fail...
## 	return _bHasCooldown, _timeRemaining, _data;
## end


## //
## // Helper - Returns the Cooldown Name after processing it through any filters... ie force it to lowercase, etc...
## //
## function META_ENTITY:GetCooldownName( _name )
## 	// Force the name to lowercase if set in the config
## 	if ( COOLDOWN_FORCE_LOWERCASE ) then
## 		_name = string.lower( _name );
## 	end

## 	return _name;
## end


## //
## // Removes a cooldown regardless of time-remaining
## //
## function META_ENTITY:RemoveCooldown( _name )
## 	self:SetCooldown( self:GetCooldownName( _name ), nil );
## end


## //
## // Returns the Cooldown Data Table, if it exists...
## //
## function META_ENTITY:GetCooldown( _name )
## 	return istable( _name ) && _name || self:GetFlag( self:GetCooldownName( _name ), nil, true, FLAG_COOLDOWN );
## end


## //
## // Creates a cooldown for a specific duration
## //
## function META_ENTITY:SetCooldown( _name, _duration )
## 	local _data = ( isnumber( _duration ) && { started = CurTime( ), duration = _duration } || nil );
## 	self:SetFlag( self:GetCooldownName( _name ), _data, true, FLAG_COOLDOWN );
## end


## //
## // Returns the Cooldown start-time
## //
## function META_ENTITY:GetCooldownStartTime( _name )
## 	local _data = self:GetCooldown( _name );
## 	return ( _data ) && _data.started || 0;
## end


## //
## // Returns the Cooldown duration
## //
## function META_ENTITY:GetCooldownDuration( _name )
## 	local _data = self:GetCooldown( _name );
## 	return ( _data ) && _data.duration || 0;
## end


## //
## // Elapsed = ( CurrentTime - StartedTime )
## //
## function META_ENTITY:GetCooldownTimeElapsed( _name )
## 	// Math.min to ensure the value never goes above GrowTime ( counting up means only worry about one way )
## 	return math.min( ( CurTime( ) - self:GetCooldownStartTime( _name ) ), self:GetCooldownDuration( _name ) );
## end


## //
## // Returns a fraction, 0 to 1, where 1 means 100% of duration of time has elapsed...
## //
## function META_ENTITY:GetCooldownTimeElapsedFraction( _name )
## 	// Math.min to ensure the value never goes above GrowTime ( counting up means only worry about one way )
## 	return math.min( self:GetCooldownTimeElapsed( _name ) / self:GetCooldownDuration( _name ), 1 );
## end


## //
## // Returns the fraction for a custom time-frame... so if the timer is 0 to 3 seconds, then this can say built me a fraction based on 0.5 seconds so less math you need to do.... since it is time-remaining it will work at the LAST BIT - time Elapsed with use the left / first time spent, this is the end / last bit of time so time remaining 0.5 == 1, time remaining 0.0 == 0... and possible reverse option..
## //
## function META_ENTITY:GetCooldownTimeElapsedCustomFraction( _name, _time )
## 	// Make sure _time is set, or use 0.25 of total duration by default...
## 	local _time = ( isnumber( _time ) && _time || self:GetCooldownDuration( _name ) * COOLDOWN_DEFAULT_CUSTOM_CAP_MOD );

## 	// For the fraction being 1 at 0 through 2.5 of 3 seconds, and 1 at 3 onwards... Remaining 0.5 / 0.5 = 1... 0 / 0.5 = 0... 3 / 0.5 min at 1...
## 	return math.Clamp( ( self:GetCooldownTimeElapsed( _name ) / _time ), 0, 1 );
## end


## //
## // Returns the fraction representation of time-spent-to-completion.. ie 1 to 0 with 0 being Cooldown is finished...
## //
## function META_ENTITY:GetCooldownTimeRemainingFraction( _name )
## 	return math.max( 1 - self:GetCooldownTimeElapsedFraction( _name ), 0 );
## end


## //
## // Returns the fraction for a custom time-frame... so if the timer is 0 to 3 seconds, then this can say built me a fraction based on 0.5 seconds so less math you need to do.... since it is time-remaining it will work at the LAST BIT - time Elapsed with use the left / first time spent, this is the end / last bit of time so time remaining 0.5 == 1, time remaining 0.0 == 0... and possible reverse option..
## //
## function META_ENTITY:GetCooldownTimeRemainingCustomFraction( _name, _time )
## 	// Make sure _time is set, or use 0.25 of total duration by default...
## 	local _time = ( isnumber( _time ) && _time || self:GetCooldownDuration( _name ) * COOLDOWN_DEFAULT_CUSTOM_CAP_MOD );

## 	// For the fraction being 1 at 0 through 2.5 of 3 seconds, and 1 at 3 onwards... Remaining 0.5 / 0.5 = 1... 0 / 0.5 = 0... 3 / 0.5 min at 1...
## 	return math.Clamp( ( self:GetCooldownTimeRemaining( _name ) / _time ), 0, 1 );
## end


## //
## // Remaining = ( GrowTime - Elapsed ) or ( GrowTime - ( CurrentTime - StartedTime ) )
## //
## function META_ENTITY:GetCooldownTimeRemaining( _name )
## 	// Math.max to ensure the value never goes below 0 ( counting down means only worry about one way )
## 	return math.max( self:GetCooldownDuration( _name ) - self:GetCooldownTimeElapsed( _name ), 0 );
## end