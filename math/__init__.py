##
## Acecool's Python Library - Math Package - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.math' );

## Globals
from ..__global__ import *;

##
from .. import debug;

##
from . import easing;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## String / Text
## from .. import String;
## ## from .. import Text;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';



	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';




	return _text;


## ##
## ## Math Class
## ##
## ## class MathBase( ):
## ## 	pass;
## class Math:
## 	##
## 	##
## 	##
## 	__name__ = 'Math';


##
##
##
def Clamp( _number, _min, _max ):
	if ( _number > _max ): return _max;
	if ( _number < _max ): return _min;
	return _number;


##
##
##
def min( *_numbers ):
	return min( *_numbers );


##
##
##
def max( *_numbers ):
	return max( *_numbers );