##
## Acecool's Python Library - Text Library - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.text' );

## Globals
from .__global__ import *;

##
from . import debug;

##
## from . import example;

##
from . import string;

## ##
## from .. import Acecool;

## ##
## from .. import Dict;

## ##
## from .. import Examples;

## ## Steam
## from .. import Steam;

## ## string / Text
## from .. import string;
## ## from .. import Text;


## ##
## ## Text Formatting Class - All formatting functions will be moved here - all string operations will be kept in string...
## ##
## class Text( ):


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += FormatPipedHeader( 'Table of Contents:' );
	## _text += example.AddTableOfContentsItem( '', '' );
	_text += '\n';
	_text += '		## Simple Text-Based Header... Will be moved to Text / string package soon...\n';
	_text += '		AcecoolLib.text.PipedHeader( _header = \'\', _prefix = \'\', _suffix = \'\', _depth = 0 );\n';
	_text += '\n';
	_text += '		## Simple Text-Based Footer... Will be moved to Text / string package soon...\n';
	_text += '		AcecoolLib.text.DebugFooter( _prefix = \'\', _suffix = \'\\n\\n\', _depth = 1 );\n';
	_text += '\n';
	_text += '		## Text Replacers used in _text.format( **AcecoolLib.Acecool.Replacers( _depth_prefix ) );\n';
	_text += '		AcecoolLib.text.DebugReplacers( _depth_prefix = \'\' );\n';
	_text += '\n';
	_text += '		## This function - used to list all functions, and give a brief demonstration, or information as to their usage and / or purpose...\n';
	_text += '		AcecoolLib.text.__example__( _depth = 1 );\n';
	_text += '\n';
	_text += '\n';
	_text += '\n';
	_text += '\n';

	## ## _depth_prefix = _depth * '\t';
	## ## _depth_prefix +;
	## ## _depth_prefix

	## ##
	## _examples = Examples( )

	## ##
	## print( _examples.GetAll( ) );

	return _text;


##																◙						◙
## Create a 3 line piped text which looks something like this:	◙	Text Appears Here	◙
##																◙						◙
def FormatPipedHeader( _text, _pipes = '#', _buffer = 4, _buffer_chars = ' ' ):
	_text = _pipes + ( _buffer_chars * _buffer ) + _text + ( _buffer_chars * _buffer ) + _pipes;
	_text = string.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes +'\n' + _text + '\n' + string.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes + '\n';

	## This creates a sstaggered - because _text already has the spaces, by adding them again, you end up with normal top and bottom, but the center is offset to the right by x chars... so it looks like an arrow > _text > - could be useful for something...
	## _text = string.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes +'\n' + ( _buffer_chars * _buffer ) + _text + ( _buffer_chars * _buffer ) + '\n' + string.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes + '\n';

	return _text


##
##
##
def FormatColumn( _width = 25, _text = '', *_varargs ):
	return string.FormatColumnEx( ' ', _width, _text, *_varargs );


##
##
##
def FormatColumnEx( _empty_char = ' ', _width = 25, _text = '', *_varargs ):
	return string.FormatColumnEx( _empty_char, _width, _text, *_varargs );

##
##
##
def FormatSimpleColumn( _width = 25, *_varargs ):
	return string.FormatSimpleColumn( _width, *_varargs );


##
##
##
def FormatColumnStripR( _width = 25, _text ='', *_varargs ):
	return string.FormatColumnStripR( _width, _text, *_varargs );


##
##
##
def FormatSimpleColumnStripR( _width = 25, *_varargs ):
	return string.FormatSimpleColumnStripR( _width = 25, *_varargs );