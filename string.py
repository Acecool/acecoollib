##
## Acecool's Python Library - String Library - Josh 'Acecool' Moser
##
##	The String Library is a pseudo extension of str... Because I am unable to properly extend str without renaming the object to something else, which would
## 		also force me to 'create' every string using _text = AcecoolLib.string.String( 'text' );, or even _text = String( 'text' ); instead of _text = '';,
##		means a lot of wasted time spent typing...
##
## 	Because of this, I have decided to create a String Helper function library... This means instead of being able to call the functions using things such as
## 		_text.blah( );, or 'text'.blah( );, I have to use AcecoolLib.string.Blah( 'text' );... Which is a little easier, possibly... I may still change how I
##		am doing this, and do something such as _str( 'text' ) or some other shortly named object to extend str for convenience... Or, maybe, I will see if my
## 		AccessorFunc system can inject properties into str... If so, then I'll be able to extend it easily...
##
## 	Some people don't like the idea of extending default libraries, objects, etc... However, when you include your extension into your project, even if it is on
## 		Sublime Text, it gets its own namespace within the project... ie: The changes do not affect other packages / modules, etc.. Additionally, even if the
## 		extensions did make it beyond the boundaries, none of the default functions are modified... Only new ones are added, all of which are very useful... I do
## 		not see the point in preventing them from being used...
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.string' );

##
import re;

##
from .__global__ import *;

##
from . import debug;
## from .. import Acecool;
from . import dict as Dict;
from . import table as Table;
from . import text as Text;



## Arrays
## from array import Dict;
## import array;


## from SteamException import * as SteamException;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';


	##
	## Truncate Example...
	##
	_col_key					= 20;
	_col_eq						= 10;
	_col_res					= 50;
	_row_eq						= '==';
	_example_text				= 'Testing';

	##;
	_col_a_text					= '10  10  10';
	_col_b_text					= '__|><|';
	_col_c_text					= 'T  h  i  r  t y';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';


	## ##
	## ## Header
	## ##
	## def Header( _header = '', _prefix = '', _suffix = '' ):
	## 	_text = _depth_prefix + '##\n';
	## 	_text += _depth_prefix + '## ' + _header + '\n';
	## 	_text += _depth_prefix + '##\n';
	## 	_text += _depth_prefix + _prefix + String.FormatColumnEx( '-', _col_key + _col_eq + _col_res, '' ) + _suffix + '';

	## 	return _text;


	## ##
	## ## Footer
	## ##
	## def Footer( _prefix = '', _suffix = '\n\n' ):
	## 	_text = _depth_prefix + _prefix + String.FormatColumnEx( '~', _col_key + _col_eq + _col_res, '' ) + _suffix;

	## 	return _text;

	## _text = '';
	_text += '';

	_text += debug.Header( 'Index for Examples', '', '', _depth_header );
	_text += _depth_prefix + '## String.FormatColumn( [ _width, _text ]+ ... )' + '\n';
	_text += _depth_prefix + '## String.FormatSimpleColumn( _width, [ _text ] ... ) ' + '\n';
	_text += _depth_prefix + '## String.FormatColumnEx( _blank_chars, [ _width, _text ]+ ... ) ' + '\n';
	_text += _depth_prefix + '## String.Truncate( _text, _max_len, _suffix = "..", _absolute_max_len = True )' + '\n';
	_text += _depth_prefix + '## DATA_TYPE Output' + '\n';
	_text += _depth_prefix + '## String.reverse( _text )' + '\n';
	_text += _depth_prefix + '## String.repeat( _text, _count = 1 )' + '\n';
	_text += _depth_prefix + '## String.ToString( [ _text ]+ )' + '\n';
	_text += _depth_prefix + '## String.ToStringKeyedVarArgs( [ _text ]+ )' + '\n';
	_text += _depth_prefix + '## String.Safe( _text = '' )' + '\n';
	_text += _depth_prefix + '## String.IndexOf( _needle, _haystack )' + '\n';
	_text += _depth_prefix + '## String.IndexOfExists( _needle, _haystack, _case_sensitive )' + '\n';
	_text += _depth_prefix + '## String.SubStr( _text, _start, _end )' + '\n';
	_text += _depth_prefix + '## String.SubStrEx( _text, _start, _end )' + '\n';
	_text += _depth_prefix + '## String.SubStrNextChars( _text = '', _start = 0, _chars = 1 )' + '\n';
	_text += _depth_prefix + '## String.Strip( _text, _index, _len )' + '\n';
	_text += _depth_prefix + '## String.StripAll( _needle, _haystack, _case_sensitive = True )' + '\n';
	_text += _depth_prefix + '## String.FindAll( _needle, _haystack )' + '\n';
	_text += _depth_prefix + '## String.StripQuotes( _text )' + '\n';
	_text += _depth_prefix + '## String.GetQuotesType( _text )' + '\n';
	_text += _depth_prefix + '## String.HasQuotes( _text )' + '\n';
	_text += _depth_prefix + '## String.StartChars( _text, _count )' + '\n';
	_text += _depth_prefix + '## String.EndChars( _text, _count )' + '\n';
	_text += _depth_prefix + '## String.NotEndChars( _text, _count )' + '\n';
	_text += _depth_prefix + '## String.StartsWith( _needle, _haystack )' + '\n';
	_text += _depth_prefix + '## String.EndsWith( _needle, _haystack )' + '\n';
	_text += _depth_prefix + '## String.FormatColumnStripR( _width = 25, _text ='', *_varargs )' + '\n';
	_text += _depth_prefix + '## String.FormatSimpleColumnStripR( _width = 25, *_varargs )' + '\n';
	_text += _depth_prefix + '## String.GetFileName( _file = '' )' + '\n';
	_text += _depth_prefix + '## String.GetFileExt( _file = '' )' + '\n';
	_text += _depth_prefix + '## String.String.__example__( )' + '\n\n\n';
	## print( '## ' );
	## print( '## ' );
	_text += '';
	_text += '';


	_text += debug.Header( 'Note: All, or most, of the examples below are formatted into columns using:', '', '', _depth_header );
	_text += _depth_prefix + '## Please Also Note that I alternate blank character patterns to show the text is truncated if it reaches the max size... '+ '\n';
	_text += _depth_prefix + '## I am considering adding in a border option, so there ccan always be 1 char space, or you decide, between areas ( meaning subtracting from the allotted length, OR adding an extra char...'+ '\n';
	_text += _depth_prefix + '## I am also going to work on a Layout System so those old Dos Characters ( which made up the nice looking tables in the BIOS, etc... ) can be used as a buffer / organizer with header...'+ '\n';
	_text += _depth_prefix + '## Note: The blank char in the Ex format column function can be used to make interesting patterns as you can use 1 to many chars... Experiment to find what looks best to you for your debugging needs!'+ '\n';
	_text += _depth_prefix + '## '+ '\n';
	_text += _depth_prefix + 'String.FormatColumn( [ _width, _text ] ... )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'ie: String.FormatColumn( 10,	"Test 10 Width",		20,	"Test 20 Width",		30,	"Test 30 Width" )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + FormatColumnEx( _col_a_text, 10,	'' ) + FormatColumnEx( _col_b_text, 20,	'' ) + FormatColumnEx( _col_c_text, 30,	'' )+ '\n';
	_text += _depth_prefix + FormatColumn( 10,	"Test 10 Width",		20, "Test 20 Width",		30,	"Test 30 Width" ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( _col_a_text, 10,	'' ) + FormatColumnEx( _col_b_text, 20,	'' ) + FormatColumnEx( _col_c_text, 30,	'' )+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'String.FormatSimpleColumn( _width, [ _text ] ... )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'ie: String.FormatSimpleColumn( 10, "Test 10", "Test 20", "Test 30" )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + FormatColumnEx( _col_a_text, 10,	'' ) + FormatColumnEx( _col_b_text, 10,	'' ) + FormatColumnEx( _col_c_text, 10,	'' )+ '\n';
	_text += _depth_prefix + FormatSimpleColumn( 10, "Test 10 Width", "Test 20 Width", "Test 30 Width" ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( _col_a_text, 10,	'' ) + FormatColumnEx( _col_b_text, 10,	'' ) + FormatColumnEx( _col_c_text, 10,	'' )+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'String.FormatColumnEx( _blank_chars, [ _width, _text ] ... )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( "-",		60,	"" )'+ '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( " ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( "-",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( " -",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( "- ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
	_text += _depth_prefix + 'ie: String.FormatColumnEx( " - ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + FormatColumnEx( '-',		60,	'' )+ '\n';
	_text += _depth_prefix + FormatColumnEx( '-', 		10,	'' ) + FormatColumnEx( '.', 20,	'' ) + FormatColumnEx( '-', 30,	'' )+ '\n';
	_text += _depth_prefix + FormatColumnEx( ' ',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( '-',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( ' -',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( '- ',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( ' - ',	10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
	_text += _depth_prefix + FormatColumnEx( '-', 10,	'' ) + FormatColumnEx( '.', 20,	'' ) + FormatColumnEx( '-', 30,	'' )+ '\n';
	## print(  );
	_text += debug.Footer( ) + '\n';

	_width = 8;
	_text += debug.Header( 'String.Truncate( <str> _text, <int> _max_len, <str> || <False> _suffix = "..", <bool> _absolute_max_len = True )', '', ' ' + str( _width ), _depth_header );
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 7;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 6;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Testin',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Test..',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 5;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Testi',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Tes..',				_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 4;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Test',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Te..',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 3;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Tes',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'T..',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 2;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'Te',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '..',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 1;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, 'T',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '.',					_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';

	_width = 0;
	_text += debug.Footer( '', ' ' + str( _width ) );
	_text += _depth_prefix + FormatColumn( _col_key, '',						_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '',						_col_eq, _row_eq,			_col_res,	Truncate( _example_text, _width, '..', True ) ) + '\n';
	_text += debug.Footer( '', ' ^ Finished\n\n' ) + '\n';


	_text += debug.Header( 'String.reverse( <str> _text )', '', '', _depth_header );
	_text += _depth_prefix + FormatColumn( _col_key, 'gnitseT',				_col_eq, _row_eq,			_col_res, reverse( _example_text ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res, reverse( reverse( _example_text ) ) ) + '\n';
	_text += debug.Footer( ) + '\n';


	_text += debug.Header( 'String.repeat( <str> _text, <int> _count = 1 )', '', '', _depth_header );
	_text += _depth_prefix + FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res, repeat( _example_text ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TestingTesting',		_col_eq, _row_eq,			_col_res, repeat( _example_text, 2 ) ) + '\n';
	_text += debug.Footer( ) + '\n';

	_text += debug.Header( 'String.FormatColumn( 20, \'TYPE_*\', 50, str( TYPE_* ) ) - Used to display TYPE_ output...', '', '', _depth_header );
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_ANY',				_col_eq, _row_eq,			_col_res,	str( TYPE_ANY ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_INTEGER',			_col_eq, _row_eq,			_col_res,	str( TYPE_INTEGER ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_ENUM',			_col_eq, _row_eq,			_col_res,	str( TYPE_ENUM ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_COMPLEX',			_col_eq, _row_eq,			_col_res,	str( TYPE_COMPLEX ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_FLOAT',			_col_eq, _row_eq,			_col_res,	str( TYPE_FLOAT ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_BOOLEAN',			_col_eq, _row_eq,			_col_res,	str( TYPE_BOOLEAN ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_STRING',			_col_eq, _row_eq,			_col_res,	str( TYPE_STRING ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_TYPE',			_col_eq, _row_eq,			_col_res,	str( TYPE_TYPE ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_TUPLE',			_col_eq, _row_eq,			_col_res,	str( TYPE_TUPLE ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_LIST',			_col_eq, _row_eq,			_col_res,	str( TYPE_LIST ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_SET',				_col_eq, _row_eq,			_col_res,	str( TYPE_SET ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_DICT',			_col_eq, _row_eq,			_col_res,	str( TYPE_DICT ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_FUNCTION',		_col_eq, _row_eq,			_col_res,	str( TYPE_FUNCTION ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_METHOD',			_col_eq, _row_eq,			_col_res,	str( TYPE_METHOD ) ) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, 'TYPE_OBJECT',			_col_eq, _row_eq,			_col_res,	str( TYPE_OBJECT ) ) + '\n';
	_text += debug.Footer( ) + '\n';



	_text += debug.Header( 'String.ToString( _text, [ _text ]* ) - This is used to quickly convert a wide range of elements to string without calling str( ) on all of them..', '', '', _depth_header );
	_text += _depth_prefix + ToString( _col_key, 'Testing',					_col_res, repeat( _example_text ) ) + '\n';
	_text += _depth_prefix + ToString( _col_key, 'TestingTesting',				_col_res, repeat( _example_text, 2 ) ) + '\n';
	_text += debug.Footer( ) + '\n';


	_text += debug.Header( 'String.ToStringKeyedVarArgs( _text, [ _text ]* ) - This is used to create copy / paste code for Dicts, etc.. Or to organize debugging data of a list...', '', '', _depth_header );
	_text += _depth_prefix + ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, repeat( _example_text ) ) + '\n';
	_text += _depth_prefix + ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, repeat( _example_text, 2 ) ) + '\n';
	_text += debug.Footer( ) + '\n';



	_dict = {
		"one":		1,
		"two":		2,
		"three":	3,
		"four":		4,
	};

	_tuple = (
		"one",
		"two",
		"three",
		"four",
	);

	_list = [
		"one",
		"two",
		"three",
		"four",
	];

	_col_key = 50;
	_col_func = 100;
	_col_res = 150;

	_text += debug.Header( 'Dict.SetupQuickLookupTable[ Ex ]( _table [ , _default_value, _use_key_instead_of_value ]* ) - This is used to create an O( 1 ) map!\n' + _depth_header_prefix + '## Note: Non Ex will automatically use the appropriate _use_key_instead_of_value option for _dicts vs _tuples and _lists', '', '', _depth_header );
	_text += _depth_prefix + str( _dict ) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + str( _dict.fromkeys( _dict, True ) ) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + str( _dict.fromkeys( _dict, True ) ) + '\n';
	_text += _depth_prefix + str( _dict.fromkeys( _dict, None ) ) + '\n';
	_text += _depth_prefix + str( _dict.fromkeys( _dict, False ) ) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'Dict.FromValues - Setting each value to be True - First uses Value, second uses Key as identifier... Note: Keys are one / two / three / four -- Values are 1 / 2 / 3 / 4' + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1: KEY = True',							_col_res, str( Dict.FromValues( _dict, True ) )														) + '\n';
	_text += _depth_prefix + '\n\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2: VALUE = True',							_col_res, str( Dict.FromValues( _dict, True, False ) )												) + '\n';
	_text += _depth_prefix + '\n\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3: VALUE = KEY',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _key, False ) )					) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '4: VALUE = VALUE',						_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _value, False ) )					) + '\n';
	_text += _depth_prefix + '\n\n';
	_text += _depth_prefix + FormatColumn( _col_key, '5: KEY = KEY',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _key ) )							) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '6: KEY = VALUE',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _value ) )						) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + 'Dict.SetupQuickLookupTable - 1: Default = Value = type( Value ) ... 2: Returns _key for value ... 3: Returns _value for value' + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.1: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTable( _dict )',																			_col_res, str( Dict.SetupQuickLookupTable( _dict ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.2: VALUE = True',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict )',																		_col_res, str( Dict.SetupQuickLookupTableEx( _dict ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.3: VALUE = str( type( VALUE ) )',		_col_func,	'Dict.SetupQuickLookupTable( _tuple )',																			_col_res, str( Dict.SetupQuickLookupTable( _tuple ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.4: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTable( _list )',																			_col_res, str( Dict.SetupQuickLookupTable( _list ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.5: KEY = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _tuple, True, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _tuple, True, True ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.6: KEY = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _list, True, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _list, True, True ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.7: VALUE = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _tuple, True, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _tuple, True, False ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '1.8: VALUE = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _list, True, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _list, True, False ) )													) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.1: KEY = None',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, None, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _dict, None, True ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.2: KEY = KEY',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ), True )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ), True ) )						) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.3: KEY = type( KEY )',					_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ), True )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ), True ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.4: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ), True )',				_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ), True ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.5: KEY = VALUE',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ), True )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ), True ) )						) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.6: KEY = type( VALUE )',				_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ), True )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ), True ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '2.7: KEY = str( type( VALUE ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ), True )',			_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ), True ) )				) + '\n';
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.1: VALUE = None',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, None, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _dict, None, False ) )													) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.2: VALUE = KEY',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ) )',									_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ) ) )						) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.3: VALUE = type( KEY )',				_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ) )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ) ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.4: VALUE = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ) )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ) ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.5: VALUE = VALUE',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ) )',								_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ) ) )						) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.6: VALUE = type( VALUE )',				_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ) )',						_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ) ) )				) + '\n';
	_text += _depth_prefix + FormatColumn( _col_key, '3.7: VALUE = str( type( VALUE ) )',		_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ) )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ) ) )				) + '\n';

	_text += debug.Footer( ) + '\n';


	## print( String.FormatColumn( 30, '3: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value ) )				) );
	## print( );
	## print( String.FormatColumn( 30, '4: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _key, True ) ) 		) );
	## print( String.FormatColumn( 30, '5: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value, True ) )		) );
	## print( String.FormatColumn( 30, '6: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, True, True ) )										) );
	## print( String.FormatColumn( 30, '7: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, True, False ) )									) );
	## print( );
	## print( String.FormatColumn( 30, '8: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _key, False ) )		) );
	## print( String.FormatColumn( 30, '9: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value, False ) ) 		) );


	return _text;


##
## Converts VarArgs into a comma delimited string ( could also use join )
##
def ToString( *_args ):
	_text = '';

	if ( len( _args ) > 0 ):
		for _arg in _args:
			_text += str( _arg ) + ', ';

	return _text[ : -2 ];


##
## Converts VarArgs into X = Y, bi-delimited string
##
def ToStringKeyedVarArgs( *_args ):
	_text = '';

	if ( len( _args ) > 0 ):
		for _i, _arg in enumerate( _args ):
			_text += ( '"', '' )[ _i % 2 != 0 ] + str( _arg ) + ( '" = ', ', ' )[ _i % 2 != 0 ];

	return _text[ : -2 ];



##
## Expand Vars System
##
def ExpandVariables( _text, _map, _extras = None ):
	## Copy the map if we have any dynamic entries or extras to add...
	_map = Table.MapKeysValuesToExtra( _map, _extras );  # _map.copy( );

	## ##
	## if ( _extras != None ):
	## 	_map.update( _extras );

	##
	return _text.format( **_map );


##
## Expand Vars System - Takes separated / unprepared keys table and values table, combines them into keys = values single map table, and allows adding extras for dynamic options..
##
def ExpandKeyMapVariables( _text, _keys, _values, _extras = None ):
	##
	_map = Table.MapKeysToValues( _keys, _values ); #ENUM_MAP( _keys, _values );

	##
	return ExpandVariables( _text, _map, _extras );


##
## Returns the string in reverse
##
def reverse( _text = '' ):
	return _text[ : : - 1 ];


##
## String.repeat
##
def repeat( _chars, _count = 1 ):
	return _chars * _count;


##
## Returns a safe file-name...
##
def Safe( _text = '' ):
	_text = _text.replace( ':', '_' );
	_text = _text.replace( '\\', '_' );
	_text = _text.replace( '/', '_' );
	_text = _text.replace( ' ', '_' );
	_text = _text.replace( '__', '_' );
	_text = _text.replace( '*', '' );
	_text = _text.replace( '?', '' );
	_text = _text.replace( '|', '' );
	_text = _text.replace( '<', '' );
	_text = _text.replace( '>', '' );

	return _text;


##
## Alias
##
## def contains( )


##
## Returns the index of the _needle word first-char if discovered - otherwise it returns -1
##
def IndexOf( _needle, _haystack ):
	return _haystack.find( _needle ) + 1;


##
## Returns True or False depending if _needle exists in _haystack or not..
##
def IndexOfExists( _needle, _haystack, _case_sensitive = False ):
	if not _case_sensitive:
		return ( IndexOf( _needle, _haystack ) > 0 );
	else:
		return ( IndexOf( _needle.lower( ), _haystack.lower( ) ) > 0 );


##
## Returns a string with only numbers, or blank if no numbers found in supplied string...
##
def Numbers( _text = '' ):
	return ''.join( filter( str.isdigit, _text ) );


##
## Returns a string with only numbers
##
def ExtractNumbers( _text = '' ):
	return re.subn( '\D', '', _text )


##
## Returns a string without any numbers
##
def ExtractNonNumbers( _text = '' ):
	return re.subn( '\d', '', _text )


##
## SubString replacement
##
## Usage:
##		_data = String.SubStr( _text, 0, 10 )
##
def SubStr( _text, _start, _end ):
	return _text[ _start : _end ];


##
## SubString replacement
##
def SubStrEx( _text, _start, _end ):
	return _text[ _start : _end - 1 ];


##
## Returns n chars after the len supplied ( useful for determining next char or chars from a pos, if number of chars is blank then 1 is used... )
##
def SubStrNextChars( _text = '', _start = 0, _chars = 1 ):
	return _text[ _start : _start + _chars ];


##
## Strips out text found at _index
##
def Strip( _text, _index, _len ):
	if ( _index > 0 ):
		return SubStr( _text, 0, _index ) + SubStr( _text, _index + _len, len( _text ) );
	else:
		return SubStr( _text, _len, len( _text ) );


##
## Strips all instances of _needle from _haystack
##
def StripAll( _needle, _haystack, _case_sensitive = True ):
	## Do while... ie run at least once...
	while( True ):
		## Make sure the data exists before running strip and repeating..
		_exists = IndexOfExists( _needle, _haystack, _case_sensitive );

		## It exists, lets do it...
		if ( _exists ):
			## It exists.. Grab the first index of where it is located...
			_index = IndexOf( _needle, _haystack ) - 1;

			## Strip our needle from the haystack
			_haystack = Strip( _haystack, _index, len( _needle ) );
		else:
			## We're done... exit loop...
			break;

	return _haystack;


##
## Truncate characters of a string after _len'nth char, if necessary... If _len is less than 0, don't truncate anything... Note: If you attach a suffix, and you enable absolute max length then the suffix length is subtracted from max length... Note: If the suffix length is longer than the output then no suffix is used...
##
## Usage: Where _text = 'Testing', _width = 4
##		_data = String.Truncate( _text, _width )						== Test
##		_data = String.Truncate( _text, _width, '..', True )			== Te..
##
## Equivalent Alternates: Where _text = 'Testing', _width = 4
##		_data = String.SubStr( _text, 0, _width )						== Test
##		_data = _text[  : _width ]										== Test
## 		_data = ( _text )[  : _width ]									== Test
##
def Truncate( _text, _max_len = -1, _suffix = False, _absolute_max_len = True ):
	## Length of the string we are considering for truncation
	_len			= len( _text );

	## Whether or not we have to truncate
	_truncate		= ( False, True )[ _len > _max_len ];

	## Note: If we don't need to truncate, there's no point in proceeding...
	if ( not _truncate ):
		return _text;

	## The suffix in string form
	_suffix_str		= ( '',  str( _suffix ) )[ _truncate and _suffix != False ];

	## The suffix length
	_len_suffix		= len( _suffix_str );

	## Whether or not we add the suffix
	_add_suffix		= ( False, True )[ _truncate and _suffix != False and _max_len > _len_suffix ];

	## Suffix Offset
	_suffix_offset = _max_len - _len_suffix;
	_suffix_offset	= ( _max_len, _suffix_offset )[ _add_suffix and _absolute_max_len != False and _suffix_offset > 0 ];
	## _suffix_offset	= ( _suffix_offset, 0 )[ _suffix_offset < 0 ];

	## The truncate point.... If not necessary, then length of string.. If necessary then the max length with or without subtracting the suffix length... Note: It may be easier ( less logic cost ) to simply add the suffix to the calculated point, then truncate - if point is negative then the suffix will be destroyed anyway.
	## If we don't need to truncate, then the length is the length of the string.. If we do need to truncate, then the length depends on whether we add the suffix and offset the length of the suffix or not...
	_len_truncate	= ( _len, _max_len )[ _truncate ];
	_len_truncate	= ( _len_truncate, _max_len )[ _len_truncate <= _max_len ];
	## _suffix_offset	= 0;

	## If we add the suffix, add it... Suffix won't be added if the suffix is the same length as the text being output...
	if ( _add_suffix ):
		_text = _text[ 0 : _suffix_offset ] + _suffix_str + _text[ _suffix_offset: ];

	## Return the text after truncating...
	return _text[ : _len_truncate ];


##
## Finds all instances of _needle from _haystack
##
def FindAll( _needle, _haystack ):
	##
	_start = 0;
	_found = [ ];
	_exclude_found = { };
	_regex = re.compile( _needle );

	_exclude = Table.KeySet( 'end', 'for', 'in', 'do', 'then', 'else', 'elseif', 'elif', 'if', 'local', 'return', 'break', 'true', 'false', 'nil', 'function' );

	## Do while... ie run at least once...
	while( True ):
		## Make sure the data exists before running strip and repeating.., len( _haystack ) + 1
		_result = _regex.search( _haystack, _start );

		## It exists, lets do it...
		if ( _result != None ): #and _result.start( ) > 0 and _result.end( ) > 0 ):
			## Update start pos
			_start = _result.end( ) + 1;
			_data = str( _result.group( 1 ).strip( ) );

			##
			if ( _data.find( ':' ) > 0 ):
				_data = _data.split( ':' )[ 0 ];

			## (?!end|then|else|elif|if|local|return|break|true|false)
			## _data.find( ':' ) > 0 or
			## if not ( _data == 'end' or _data == 'then' or _data == 'else' or _data == 'elif' or _data == 'if' or _data == 'local' or _data == 'return' or _data == 'break' or _data == 'true' or _data == 'false' ):
			##
			if not ( _data.find( '..' ) > 0 or _data.endswith( '"' ) or _data.endswith( "'" ) or _data.startswith( '"' ) or _data.startswith( "'" ) or _data.endswith( "(" ) or HasQuotes( _data ) or Table.HasKeySet( _exclude_found, _data ) or Ttable.HasKeySet( _exclude, _data ) ):
				## Add the result...
				_found.append( _data );
				_exclude_found[ _data ] = True;

				## print( str( _data ) )
		else:
			## We're done... exit loop...
			break;

	if ( len( _found ) > 0 ):
		return _found;
	else:
		return None;


##
## Removes Quotes from a string...
##
def StripQuotes( _text ):
	if ( _text.startswith( "'" ) and _text.endswith( "'" ) or _text.startswith( '"' ) and _text.endswith( '"' ) ):
		_text = SubStr( _text, 1, len( _text ) - 1 );

	return _text;


##
## Returns the type of quotes used by a string..
##
def GetQuotesType( _text ):
	if ( not _text or _text == '' ):
		return '';

	if ( _text.startswith( "'" ) and _text.endswith( "'" ) ):
		return "'";

	if ( _text.startswith( '"' ) and _text.endswith( '"' ) ):
		return '"';

	return '';


##
## Returns whether or not a particular string has quotes within it.
##
def HasQuotes( _text ):
	_type = GetQuotesType( _text )

	return Logic.TernaryFunc( _type == '"' or _type == "'", True, False );


##
## Returns the n starting chars from the string
##
def StartChars( _text, _count ):
	return _text[ : _count ];


##
## Returns the n ending chars from the string
##
def EndChars( _text, _count ):
	## One way to do it
	## _len = len( _text );
	## return _text[ _len - _count : ];

	## And another way...
	return _text[ - _count : ];


##
## Returns everything except the last _count chars in the string...
##
def NotEndChars( _text, _count ):
	return _text[ : - _count ];


##
## BUILT-IN - Returns whether or not our string starts with certain text
##
def StartsWith( _needle, _haystack ):
	return _haystack.startswith( _needle );


##
## BUILT-IN - Returns whether or not our string ends with certain text
##
def EndsWith( _needle, _haystack ):
	return _haystack.endswith( _needle );


##
## Helper function which lets you define width for each text, and it repeats it for you so you can repeat text and all columns will be the same width
##
## Purpose:
## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
## Usage:
##		String.FormatColumn( 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
##		String.FormatColumn( 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
##		String.FormatColumn( 25, 'Key', 15, 'Some', 15, 'Another', 50, 'Value' )
##		String.FormatColumn( -5, 'Key', -10, 'Some', -15, 'Another', -20, 'Value' )
##
## Output:
##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
##		Key                      Some           Another        Value                                             <LINE END>
##
## Using Negatives
##		Key     Some          Another               Value                    <LINE END>
##
def FormatColumn( _width = 25, _text = '', *_varargs ):
	return FormatColumnEx( ' ', _width, _text, *_varargs );


##
## Helper function which lets you define width for each text, and it repeats it for you so you can repeat text and all columns will be the same width
##
## Task: Possibly optimize by adding if _width < 1 then simply append option and add abs( _width ) * _empty to the end... this means a lot less logic to process..
##
## Purpose:
## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
## Usage:
##		String.FormatColumnEx( '.', 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
##		String.FormatColumnEx( ' ', 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
##		String.FormatColumnEx( ' ', 25, 'Key', 15, 'Some', 15, 'Another', 50, 'Value' )
##		String.FormatColumn( ' ', -5, 'Key', -10, 'Some', -15, 'Another', -20, 'Value' )
##
## Output:
##		Text / Key...............Some Value.....Another Key....Another Value.....................................<LINE END>
##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
##		Key                      Some           Another        Value                                             <LINE END>
##
## Using Negatives
##		Key     Some          Another               Value                    <LINE END>
##
def FormatColumnEx( _empty_char = ' ', _width = 25, _text = '', *_varargs ):
	## Make sure our text is a string .. and for each tab used, calculate how many spaces should be used with minimum of 1 and maximum of 4 being the range depending which snap-point is used.. Then strip that tab and add spaces in its place..
	_text	= str( _text ).expandtabs( CONST_TAB_WIDTH );

	## Since our Ex function, this, must use a paired-system, we make sure the rounded division is > 0
	_more	= ( round( len( _varargs ) / 2, 0 ) > 0 );

	## If _width is 0, we append the string without doing anything ( but we still process logic - I may make this shorter later by doing that though )... if _width is negative, we fit to the length of text + abs( _width ) spaces added to the end...
	_width = ( len( _text ) + abs( _width ), _width )[ _width >= 0 ];

	## How many repeating chars do we need to create?
	_reps	= ( _width - len( _text ) );

	## Build a string to fill the empty column space with spaces so everything lines up - as long as the right font is used ( where all chars are the same size )
	_empty	= _empty_char * _reps;

	## Now we ensure our text is limited to the _width size - data going over is truncated...TernaryFunc( _reps > 0, _empty, _empty )
	## _data = String.SubStr( _text + ( _empty ), 0, _width );
	## _data = ( _text + ( _empty ) )[  : _width ];
	_data	= Truncate( _text + ( _empty ), _width );

	## If we have more cars
	if ( _more ):
		## Recursive call by shifting our VarArgs left so they populate _width and _text - then add the result to the data var... This only stops when no more paired options are left...
		_data = _data + FormatColumnEx( _empty_char, *_varargs );

	## Return the data..
	return _data;


##
## Helper function which lets you define width once, and it repeats it for you so you can repeat text and all columns will be the same width
##
## Purpose:
## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
##
## Usage:
##		String.FormatSimpleColumn( 15, 'Text / Key', 'Some Value', 'Another Key', 'Another Value' )
##		String.FormatSimpleColumn( 15, 'Key', 'Some', 'Another', 'Value' )
##		String.FormatSimpleColumn( -5, 'Key', 'Some', 'Another', 'Value' )
##
## Output:
##		Text / Key     Some Value     Another Key    Another Value  <LINE END>
##		Key            Some           Another        Value          <LINE END>
##
## Using Negatives
##		Key     Some     Another     Value     <LINE END>
##
def FormatSimpleColumn( _width = 25, *_varargs ):
	## Count how many text elements we have...
	_count = len( _varargs );

	## Set up our return var
	_data = '';

	## If we have at least 1 text element to set-up into a column
	if ( _count > 0 ):
		## Then we loop through each vararg
		for _text in _varargs:
			## Accumulate data by appending each additional _text argument using a call to FormatColumn
			_data = _data + FormatColumn( _width, str( _text ) );

	## Return the data..
	return _data;


##
##
##
def FormatColumnStripR( _width = 25, _text ='', *_varargs ):
	return FormatColumn( _width, _text, *_varargs ).rstrip( );


##
##
##
def FormatSimpleColumnStripR( _width = 25, *_varargs ):
	return FormatSimpleColumn( _width = 25, *_varargs ).rstrip( );


##
## Returns the filename with file extension from path/to/file.ext
## Task: Deprecated - use Text.DebugFile.* or Folder.*
##
def GetFileName( _file = '' ):
	return File.GetName( _file );


##
## Returns the file extension from path/to/file.ext
## Task: Deprecated - use Text.DebugFile.* or Folder.*
##
def GetFileExt( _file = '' ):
	return File.GetExt( _file );