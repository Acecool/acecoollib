##
## Acecool's Python Library - Globals Package - Josh 'Acecool' Moser
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t> AcecoolLib.__global__\n' );


##
##
##
def import_if_exists( _module ):
	## Try to import a module, if it exists - Return False if it doesn't, import if it does and return True.
	try:
		__import__( _module );
	except ImportError:
		return False;

	return True;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents:\n';
	_text += '\n';

	_text += '\n';
	_text += 'Acecool.__global__.__example__( ) ------------------\n';
	_text += '\n';
	## _text += '\n';
	## _text += '\n';
	## _text += '\n';
	## _text += '\n';
	## _text += '\n';
	## _text += '\n';
	## _text += '\n';


	return _text;


## ##
## ##
## ##
## def isstring( _arg ):
## 	return ( type( _arg ) == type( "" ) );


## ##
## ##
## ##
## def isnumber( _arg ):
## 	return ( type( _arg ) == type( 123 ) );




##
## Declarations for Globals, CONSTants, ENUMeration, etc..
##



##
## Some important declarations
##
VALUE_ANY				= None;
VALUE_ALL				= VALUE_ANY;
VALUE_BOOLEANS			= ( True, False );
VALUE_SINGLE_DIGITS		= ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 );

## New-Line Declaration Constants - can use in config if you want..
CONST_NEW_LINE_WINDOWS						= '\r\n';
CONST_NEW_LINE_UNIX							= '\n';
CONST_NEW_LINE_MAC_OS_9						= '\n';

## True / False alternative definitions...
true										= True;
false										= False;
TRUE										= True;
FALSE										= False;

## Data returned when line-number is out of range in certain functions..
CONST_DATA_OUT_OF_RANGE						= 'OUT_OF_RANGE';

## Default used for file-data
CONST_DATA_NONE								= 'DATA_NONE';

## Define the tab jump-points - Pi uses 8, Windows uses 4, etc.. Basically, how many space-chars are used per \t tab char...
CONST_TAB_WIDTH								= 4;

##
## Folder Modes for GetPath...
##

## Default is Suffix Only
PATH_MODE_DEFAULT							= 0;

## Plugin grabs Packages/PluginName/ + suffix
PATH_MODE_PLUGIN							= 1;

## User Plugin grabs Packages/User/PluginName/ + suffix
PATH_MODE_PLUGIN_USER						= 2;

##
PATH_MODE_PLUGIN_RES						= 3;

##
PATH_MODE_PLUGIN_RES_USER					= 4;

##
PATH_MODE_CODEMAP_SYNTAX					= 5;


TYPE_ANY				= None;
TYPE_INTEGER			= int;
TYPE_ENUM				= int;
TYPE_COMPLEX			= complex;
TYPE_FLOAT				= float;
TYPE_BOOLEAN			= type( True );
TYPE_STRING				= type( '' );
TYPE_TYPE				= type;
TYPE_TUPLE				= type( ( ) );
TYPE_LIST				= type( [ ] );
TYPE_SET				= set;
TYPE_DICT				= type( { } );
TYPE_FUNCTION			= type( lambda: print( ) );
TYPE_METHOD				= type( lambda: print( ) );
TYPE_OBJECT				= object;
TYPE_NUMBERS			= ( TYPE_INTEGER, TYPE_FLOAT );
TYPE_NUMBERICAL			= TYPE_NUMBERS;

## Sublime Text 3 API / Library

## TYPE_SUBLIME_VIEW		= ( import_if_exists( 'sublime' ) and import_if_exists( 'sublime_plugin' ) ) and sublime.View or '<class \'sublime.View\'>';


## TYPE_CLASS				= class
## TYPE_INTEGER			= type( 0 )
## TYPE_LONG				= type( long )
## TYPE_FLOAT				= type( 0.0 )
## TYPE_STRING_BASE		= type( basestring )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )




##
##
##
def tostr( *_args ):
	return String.ToString( *_args );



##
## Data-Type Helpers...
##

## Helper, since they all repeat stuff, mostly...
def IsType( _data, _type ): return str( type( _data ) ) == "<class '" + _type + "'>"

##
## def IsModule( _data = None ): return str( type( _data ) ) == "<class 'module'>"
def IsModule( _data = None ): return IsType( _data, 'module' );
def ismodule( _data ): return IsClass( _data );

##
## def IsClass( _data = None ): return str( type( _data ) ) == "<class 'type'>"
def IsClass( _data = None ): return IsType( _data, 'type' ) or IsType( _data, 'object' );
def isclass( _data ): return IsClass( _data );


##
## def IsFunction( _data = None ): return str( type( _data ) ) == "<class 'function'>" or str( type( _data ) ) == "<class 'method'>";
def IsFunction( _data = None ): return IsType( _data, 'function' ) or IsType( _data, 'method' );
def isfunction( _data ): return IsFunction( _data );

## A Boolean is a 1 bit data-type representing True / False or as 1 / 0 in most languages.
def IsBoolean( _data = None ): return type( _data ) is bool;
def isboolean( _data ): return IsBoolean( _data );

## A List is a list created using [ ]s - It can contain a series of values - the index must be whole-numerical
def IsList( _data = None ): return type( _data ) is list;
def islist( _data ): return IsList( _data );

## A Tuple is a list created using ( )s - It is similar to a List but a Tuple is fixed in size once ccreated.
def IsTuple( _data = None ): return type( _data ) is tuple;
def istuple( _data ): return IsTuple( _data );

## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
def IsDict( _data = None ): return type( _data ) is dict;
def isdict( _data ): return IsDict( _data );

## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
def IsTableTypeSet( _data = None ): return type( _data ) is set;
def isTableTypeset( _data ): return IsTableTypeSet( _data );

## To help check for multi-type tables allowed - ie List and Tuple are similar
def IsSimpleTable( _data = None ): return IsTuple( _data ) or IsList( _data );
def issimpletable( _data ): return IsSimpleTable( _data );

## To help check for multi-type tables allowed..
def IsTable( _data = None ): return IsSimpleTable( _data ) or IsDict( _data ) or IsTableTypeSet( _data );
def istable( _data ): return IsTable( _data );

## A String is a text object typically contained and / or created within quotes..
def IsString( _data = None ): return type( _data ) is str;			# ( _typeof == "<class 'string'>" )
def isstring( _data ): return IsString( _data );

## A Complex is a traditional Int as a whole number from 0-255
def IsComplex( _data = None ): return type( _data ) is complex;
def iscomplex( _data ): return IsComplex( _data );

## An Integer, traditionally is a whole number not exceeding 255 - otherwise it typically has a range of 2.7 or so million... +/-
def IsInteger( _data = None ): return type( _data ) is int;
def isinteger( _data ): return IsInteger( _data );

## A long is a number as 123L which can represent octal and hexadecimal values
## def IsLong( _data = None ): return type( _data ) is long;
## def islong( _data ): return islong( _data );

## A Float is half-size Double number with decimal support
def IsFloat( _data = None ): return type( _data ) is float;
def isfloat( _data ): return IsFloat( _data );

## A Number is an integer, float, double, etc..
def IsNumber( _data = None ): return IsComplex( _data ) or IsInteger( _data ) or IsFloat( _data ); # or IsLong( _data )
def isnumber( _data ): return IsNumber( _data );


##
## IsSet is just to see whether or not something is None or not...
##
def IsSet( _data = None, _key = None ):
	## List checking...
	if ( _key != None ):
		## Dictionaries are easy to look at...
		if ( IsDict( _data ) ):
			return TernaryFunc( ( _data.get( _key, False ) == False ), False, True )

		## Lists require the key to be an integer... Tuples are like list but immutable
		elif ( IsList( _data ) or IsTuple( _data ) ):
			## Must be integer otherwise there will be an error so there's no point trying...
			if ( IsInteger( _key ) == False ): return False

			## If the key is out of bounds, then there's an issue...
			if ( len( _data ) > _key ): return False

			## We need to use a try because using simple len doesn't seem to be doing it??
			try:
				if ( IsSet( _data[ _key ] ) ):
					return True
				else:
					return False;
			except IndexError:
				return False;
			except:
				return False;
		elif ( IsString( _data ) ):
			return True;


	## Basic data-type...
	return _data != None;


##
## ## Grab the variable value based on a variable name
## ##
## def GetVariableValueByName( _key, _default_key = None, _default = None ):
## 	## return globals( ).get( _key, locals( ).get( _key, _default ) )
## 	if ( _default_key ):
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default ) );

## 	return globals( ).get( _key, locals( ).get( _key, _default ) );


##
## Grab the variable value based on a variable name
##
## def GetVariableValueByName( _key, _default_key = None, _default = None, _alt_tab = None ):

## 	## return globals( ).get( _key, locals( ).get( _key, _default ) )
## 	if ( _default_key ):
## 		_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ]
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default_override ) );

## 	##
## 	_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ]
## 	return globals( ).get( _key, locals( ).get( _key, _default_override ) );

def GetVariableValueByName( _key, _default_key = None, _default = None, _alt_tab = { } ):
	## return globals( ).get( _key, locals( ).get( _key, _default ) )
	if ( _default_key ):
		_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ len( _alt_tab ) > 0 ]
		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default_override ) );

	##
	_default_override = ( _default, _alt_tab.get( _key, _default ) )[ len( _alt_tab ) > 0 ]
	return globals( ).get( _key, locals( ).get( _key, _default_override ) );
## def GetVariableValueByName( self, _key, _default_key = None, _default = None, _alt_tab = None ):
## 	if ( _default_key ):
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, ( ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) ) )

## 	return globals( ).get( _key, locals( ).get( _key, ( ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) ) )


##
## Ternary Operation in Python is just weird and has problems with stacking.. This fixes that...
##
def TernaryFunc( _statement = True, _true = None, _false = None ):
	return ( _false, _true )[ _statement ];


##
##
##
def tostr( *_args ):
	return String.ToString( *_args );


##
## Data-Type Aliases
##
## isset			= IsSet
## isboolean		= IsBoolean
## IsBool			= IsBoolean
## isbool			= IsBoolean
## islist			= IsList
## istuple			= IsTuple
## isdict			= IsDict
## isstring		= IsString
## iscomplex		= IsComplex
## isinteger		= IsInteger
## IsInt			= IsInteger
## isint			= IsInteger
## ## islong			= IsLong
## isfloat			= IsFloat
## isnumber		= IsNumber

## isfunction		= IsFunction
## isfunc			= IsFunction
## IsFunc			= IsFunction




##
## Enumerator - set the number of values to generate, and optionally the starting number...
##
## @arg: _count:	The number of ENUMs / Variables you create and prefix to the function - excluding the MAPs
## @arg: _start:	The starting number to use.. ( 4, 0 ) would produce: 0, 1, 2, 3 but ( 4, 0, true ) produces: 0, 1, 2, 4
## @arg: _pow:		Set to true to use base 2 for the ENUMeration values - 2^0 is 1 and 2^1 is 2 - you define the starting value and let it go from there with the second arg..
## @arg: _map:		If you want to map the ENUMeration ( ENUM values to be used as table keys for O( 1 ) access ) to functions, values, language, etc... Add a table here..
## @VarArgs:		Repeat _map for as many different maps as you want to make... Each will be its own table using ENUM values as the table key and the supplied values as values..
##
## Logic is as follows:
##	Call enumerator with relevant data... Start number, mode for pow or +1 increment. the number of elements we want.. OR a table which can be counted... We can also add more _args which basically take those counts and lets you build maps... If count is a table then it becomes a map...
##	Establish the start / end numbers and if there is a map to use then we add that to the maps list...
##	We process / create the enums
##	We then use the same enumerated values to generate the rest o the maps...
##
##
##
def ENUM( _count = None, *_maps ):

	## If count isn't set, and we don't have a map to base it off - we don't do anything...
	if ( _count == None and len( _maps ) < 1 ):
		return;


	## Set up a table for the enum values to be stored...
	_enums = [ ];

	## Create a single list of all of the enums...
	_enum_list				= [ ];

	## We set up the global options table to use _count, if it is set, and it is a Dictionary... If it is a number, we have other plans...
	_options				= ( { }, _count )[ _count != None and IsDict( _count ) ];

	## Determine the starting value - Note: It needs to start at 0 or lower for some reason... We seem to have issues using Lists with any non-concurrent number 1 or higher...
	_start 					= _options.get( 'start', 0 );

	## If _count is set and it is a number, then we use _count to assign _cout, otherwise we leave it at None and generate the number manually..
	_count					= _options.get( 'count', ( None, _count )[ _count != None and IsNumber( _count ) ] );

	## Configuration to see whether or not to generate the enumerator list
	_generate_enum_list		= _options.get( 'generate_enum_list', True );

	## Configuration to see whether or not we generate reverse-maps globally...
	_generate_reverse_maps	= _options.get( 'generate_reverse_maps', True );


	## Make sure start is 0 or less than 0...
	if ( _start > 0 ):
		_start = 0;

	## If we have at least 1 map, and the count isn't set or it is set less than the length of the map, we set it...
	if ( len( _maps ) > 0 and _maps[ 0 ] != None and ( _count == None or _count < len( _maps[ 0 ] ) ) ):
		if ( IsDict( _maps[ 0 ] ) ):
			_count = len( _maps[ 0 ].get( 'map', _maps[ len( _maps ) - 1 ] ) );
		else:
			_count = len( _maps[ 0 ] );


	## Process enumerations...
	for _i in range( _start, _count + _start ):
		_enums.append( _i );
		_enum_list.append( _i );

	## The list of enums is added to the start?
	if ( _generate_enum_list ):
		_enums.append( _enum_list );

	## Process any maps if we have them...
	if ( len( _maps ) > 0 ):
		## The adjusted starting point
		_i = _start;

		## For each map we are to build
		for _map in _maps:
			## If the _map is not a List, Tuple or Dictionary then skip this entry..
			if ( not IsTable( _map ) ):
				print( 'ENUM => Error... Map is not a List, Tuple, Set or Dictionary... Skipping this entry.. -- Type: ' + str( type( _map ) ) + ' -- Data: ' + str( _map ) );
				## print( 'ENUM -> _enums Count [ ERROR ]: ' + str( len( _enums ) ) );
				continue;


			## Setup up some special vars swhich may be changed with a Dictionary configuration entry... Config for the Dictionary, _map becomes _config.get( 'map', [ ] ), and _prefixes becomes _config.get( 'prefixes', _prefixes );
			_prefixes					= [ '' ];
			_value_callback				= None;
			_generate_enum_list			= True;
			_generate_reverse_map		= True;

			## Dictionaries are special...
			if ( IsDict( _map ) ):
				## Grab the prefixes...
				_prefixes				= _map.get( 'prefixes', _prefixes );

				## Grab the prefixes...
				_generate_reverse_map	= _map.get( 'generate_reverse_map', True );

				## Grab the prefixes...
				_value_callback			= _map.get( 'value_callback', lambda **_varargs: _varargs.get( 'value', None ) );

				## Grab the map..
				_map					= _map.get( 'map', [ ] );

			## If we allow generating reverse maps globally, or we have the local one set to True we can generate.. but only if the local one isn't False... If the local is false, then that overrides all..
			_generate_rmap	= ( _generate_reverse_map != False and ( _generate_reverse_maps or _generate_reverse_map ) )

			## For each Prefix we have in our list of prefixes...
			for _prefix in _prefixes:
				## The processed map we will insert into the enums list for export...
				_map_save = [ ];

				## The reversed processed map to save..
				_map_save_reversed = { };

				## For each key / value pairing in the map
				for _key, _value in enumerate( _map ):
					## Generate the appropriate index based on the starting index modified by the current counter...
					_map_i = ( _key + _i );

					## Add _map_save[ ADJUSTED_KEY ] = MAPPED_VALUE
					if ( _value_callback != None and callable( _value_callback ) ):
						_value = _value_callback( prefix = _prefix, value = _value );

					## Process the value futher to ensure it is usable.. Unfortunately, due to a bug, I can't use _prefix + _value in Ternary operators because _value can be None, despite that branch not being used when _value is None... This should really be fixed... So I have to force a string to be a string which is a waste to compensate for times when _value isn't used when it is None...
					_entry = ( _value, _prefix + str( _value ) )[ _prefix != '' and _value != None ];

					## Setup the standard direction mapping table... ENUM -> Word
					_map_save.insert( _map_i, _entry );

					## Setup the reversed direction mapping table... Word -> ENUM - We generate it if we can globally, or if we have the local reverse map set to True... But, we ignore this one, even if global is set, if we have the local one set to false...
					if ( _generate_rmap ):
						_map_save_reversed[ _entry ] = _map_i;


				## As we exiit the loop, before we go to the next loop or exit, we add the table to our enums / return table...
				_enums.append( _map_save );

				## Add the reversed map after each map created...
				if ( _generate_rmap ):
					_enums.append( _map_save_reversed );


	## Return the list of enums and maps...
	return _enums;


##
## Takes a table of keys and a table of values and turns them into a single table of keys = values...
##
def ENUM_MAP( _keys, _values ):
	if ( not ( IsTable( _keys ) and IsTable( _values ) and len( _keys ) == len( _values ) ) ):
		return False;

	##
	_map = { };

	##
	for _key, _value in enumerate( _keys ):
		_map[ _value ] = _values[ _key ];

	return _map;


##
##
##
def ENUM_MAP_PREPARE( _map, _extras = None ):
	##
	_map = _map.copy( );

	##
	if ( _extras != None ):
		_map.update( _extras );


	##
	## print( 'ENUM_MAP: ', str( _map ) );

	return _map;


##
## ENUMeration
##

##
ACCESSORFUNC_TYPE_INSTANCE, ACCESSORFUNC_TYPE_PROPERTY, ENUM_LIST_ACESSORFUNC_TYPES, MAP_ACCESSORFUNC_TYPE_NAMES, MAPR_ACCESSORFUNC_TYPE_NAMES = ENUM( None, [ 'Instance', 'Property' ] );



## AccessorFunc ID ENUMs. These are used to identify function short-ids, build function names based on input data, and much more...
ACCESSORFUNC_ID_SETUP_DEFAULTS, ACCESSORFUNC_ID_GET, ACCESSORFUNC_ID_GET_RAW, ACCESSORFUNC_ID_ISSET, ACCESSORFUNC_ID_GET_STR, ACCESSORFUNC_ID_GET_LEN, ACCESSORFUNC_ID_GET_LEN_STR, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, ACCESSORFUNC_ID_GET_PROPERTY, ACCESSORFUNC_ID_GET_ACCESSOR, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, ACCESSORFUNC_ID_GET_GETTER_PREFIX, ACCESSORFUNC_ID_GET_NAME, ACCESSORFUNC_ID_GET_KEY, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, ACCESSORFUNC_ID_GET_DATA_KEY, ACCESSORFUNC_ID_SET, ACCESSORFUNC_ID_ADD, ACCESSORFUNC_ID_POP, ACCESSORFUNC_ID_RESET, ACCESSORFUNC_ID_GET_HELPERS, ACCESSORFUNC_ID_GET_OUTPUT, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, ACCESSORFUNC_ID_INSTANCE, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_FUNC_NAMES_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_GROUPS_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_KEYS_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_NAMES_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_REFERENCES_DATABASE, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, ACCESSORFUNC_ID_INSTANCE__STR__, ENUM_LIST_ACESSORFUNC_IDS, MAP_ACCESSORFUNC_IDS, MAPR_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ON_IDS, MAPR_ACCESSORFUNC_ON_IDS, MAP_ACCESSORFUNC_PRE_IDS, MAPR_ACCESSORFUNC_PRE_IDS, MAP_ACCESSORFUNC_POST_IDS, MAPR_ACCESSORFUNC_POST_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAPR_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE = ENUM( None,
	{
		## Value Callback System - if the defined as a lambda using **_varargs, you may define anything to the values..
		## 'value_vallback':	lambda **_varargs: ...,

		## Process this list 4 times with each output map having a different prefix for the existing words
		'prefixes': [ '', 'on_', 'pre_', 'post_' ],

		## Words list...
		'map': [
			## MAP_ACCESSORFUNC_IDS	- for backwards compatibility until these are removed and the enumeration key / values are used as the only thing..
			'setup_defaults', 'get', 'get_raw', 'isset', 'get_str', 'get_len', 'get_len_str', 'get_default_value', 'get_property', 'get_accessor',
			'get_allowed_types', 'get_allowed_values', 'get_base_allowed_types', 'get_base_allowed_values',
			'has_getter_prefix', 'get_getter_prefix', 'get_name', 'get_key', 'get_accessor_key', 'get_data_key',
			'set', 'add', 'pop', 'reset', 'get_helpers', 'get_output', 'get_getter_output',

			## Single - Unique Parent / Instance Functions ( Only added once, and are not bound by 'name' )...
			'instance', 'instance_reset_accessors_by_name', 'instance_set_accessors_by_name', 'instance_get_accessors_by_name',
			'instance_reset_accessors_by_key', 'instance_set_accessors_by_key', 'instance_get_accessors_by_key',
			'instance_reset_grouped_accessors', 'instance_set_grouped_accessors', 'instance_add_grouped_accessors', 'instance_get_grouped_accessors',
			'instance_get_accessors_database', 'instance_get_accessors_func_names_database', 'instance_get_accessors_groups_database', 'instance_get_accessors_keys_database', 'instance_get_accessors_names_database', 'instance_get_accessors_references_database',
			'instance_add_accessors_database', 'instance_reset_accessors_database',

			## __str__
			'instance__str__'
		]
	},
	[
		## MAP_ACCESSORFUNC_ID_FUNC_NAMES - These are the function names without this. / self. or arguments or parens. - Note: word_* are static words,
		None, '{get}{name}', '{get}{name}{word_raw}', '{word_is}{name}{word_set}', '{get}{name}{word_tostring}', '{get}{name}{word_len}', '{get}{name}{word_len}{word_tostring}', '{get}{name}{word_default}{word_value}', '{get}{name}{word_property}', '{get}{name}{word_accessor}',
		'{get}{name}{word_allowed}{word_types}', '{get}{name}{word_allowed}{word_values}', '{get}{name}{word_base}{word_allowed}{word_types}', '{get}{name}{word_base}{word_allowed}{word_values}',
		'{word_has}{name}{word_getter}{word_prefix}', '{get}{name}{word_getter}{word_prefix}', '{get}{name}{word_name}', '{get}{name}{word_key}', '{get}{name}{word_accessor}{word_key}', '{get}{name}{word_data}{word_key}',
		'{word_set}{name}', '{word_add}{name}', '{word_pop}{name}', '{word_reset}{name}', '{get}{name}{word_helpers}', '{get}{name}{word_output}', '{get}{name}{word_getter}{word_output}',

		## Instance
		None, '{word_reset}{word_accessors}', '{word_set}{word_accessors}', '{word_get}{word_accessors}',
		'{word_reset}{word_accessors}{word_by}{word_key}', '{word_set}{word_accessors}{word_by}{word_key}', '{word_get}{word_accessors}{word_by}{word_key}',
		'{word_reset}{word_grouped}{word_accessors}', '{word_set}{word_grouped}{word_accessors}', '{word_add}{word_grouped}{word_accessors}', '{word_get}{word_grouped}{word_accessors}',
		'{word_get}{word_accessors}{word_db}', '{word_get}{word_accessors}{word_func}{word_names}{word_db}', '{word_get}{word_accessors}{word_groups}{word_db}', '{word_get}{word_accessors}{word_keys}{word_db}', '{word_get}{word_accessors}{word_names}{word_db}', '{word_get}{word_accessors}{word_references}{word_db}',
		'{word_add}{word_accessors}{word_db}', '{word_reset}{word_accessors}{word_db}',

		## __str__
		'{word__str__}'
	],
	{
		## These have too many identical values to be useful as a reverse map, so don't generate one..
		'generate_reverse_map': False,

		## Create a map which shows each argument list for each of the functions
		'map': [
			## MAP_ACCESSORFUNC_ID_FUNC_ARGS - These are the function arguments, if any including defaults without parens
			None, '_default_override = None, _ignore_defaults = False', '', '', 'get_str', '_default_override = None, _ignore_defaults = False', '_default_override = None, _ignore_defaults = False', 'get_default_value', '', '',
			'', '', '', '',
			'', '', '', '', '', '',
			'_value', '_value', '_index = len - 1, _count = 1', '', '', '', '',

			## Instance
			None, '_name [ , _name ] ...', '_name, _value [ , _name, _value ] ...', '_name [ , _name ] ...',
			'_key [ , _key ] ...', '_key, _value [ , _key, _value ] ...', '_key [ , _key ] ...',
			'_group [ , _group ] ...', '_group, _name [ , _name ] ...', '_group, _name [ , _name ] ...', '_group [ , _group ] ...',
			'', 'instance_get_accessors_func_names_database', 'instance_get_accessors_groups_database', 'instance_get_accessors_keys_database', 'instance_get_accessors_names_database', 'instance_get_accessors_references_database',
			'_entry', '',

			## __str__
			'',
		],
	},
	{
		## These have too many identical values to be useful as a reverse map, so don't generate one..
		'generate_reverse_map': False,

		## Create a map which shows the type of instance / object the enumeration ids belong to..
		'map': [
			## MAP_ACCESSORFUNC_ID_FUNC_CALL_NAMES - These are the functions as they'd be if they were called... use {map_*} for names and args to add the arguments and generated name so these are short as: '{word_this}.{map_name}( {map_args} );' - The only thing we really need to do is choose this or self as the only difference meaning we can generate these dynamically... Note: Remove this, generate them dynamically and replace this with self vs this choices...
			## MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE
			None, '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',

			## Instance
			None, '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',

			'{word_this}', '{word_this}',

			## __str__
			'{word_this}',
		],

	},
);



ACCESSORFUNC_WORD_SELF, ACCESSORFUNC_WORD_THIS, ACCESSORFUNC_WORD_FUNC, ACCESSORFUNC_WORD_GROUPS, ACCESSORFUNC_WORD_KEYS, ACCESSORFUNC_WORD_NAMES, ACCESSORFUNC_WORD_REFERENCES, ACCESSORFUNC_WORD_KEY, ACCESSORFUNC_WORD_GET, ACCESSORFUNC_WORD_SET, ACCESSORFUNC_WORD_ADD, ACCESSORFUNC_WORD_POP, ACCESSORFUNC_WORD_RAW, ACCESSORFUNC_WORD_IS, ACCESSORFUNC_WORD_TO_STR, ACCESSORFUNC_WORD_LEN, ACCESSORFUNC_WORD_DEFAULT, ACCESSORFUNC_WORD_VALUE, ACCESSORFUNC_WORD_PROPERTY, ACCESSORFUNC_WORD_ACCESSOR, ACCESSORFUNC_WORD_ACCESSORS, ACCESSORFUNC_WORD_ALLOWED, ACCESSORFUNC_WORD_TYPES, ACCESSORFUNC_WORD_VALUES, ACCESSORFUNC_WORD_BASE, ACCESSORFUNC_WORD_HAS, ACCESSORFUNC_WORD_GETTER, ACCESSORFUNC_WORD_PREFIX, ACCESSORFUNC_WORD_NAME, ACCESSORFUNC_WORD_DATA, ACCESSORFUNC_WORD_RESET, ACCESSORFUNC_WORD_WIKI, ACCESSORFUNC_WORD_OUTPUT, ACCESSORFUNC_WORD_BY, ACCESSORFUNC_WORD_GROUPED, ACCESSORFUNC_WORD_DB, ACCESSORFUNC_WORD_DATABASE, ACCESSORFUNC_WORD_HELPER, ACCESSORFUNC_WORD_HELPERS, ACCESSORFUNC_WORD__STR__, ENUM_LIST_ACCESSOR_FUNC_WORDS, MAP_ACCESSORFUNC_WORDS, MAPR_ACCESSORFUNC_WORDS, MAP_ACCESSORFUNC_WORD_KEYS, MAPR_ACCESSORFUNC_WORD_KEYS = ENUM( None,
	{
		## Change all words to fully lowercase, but only when the prefix is 'word_' so we can generate 2 lists... 1 as is seen, and one where each is lowercase with the word_ prefix...
		'value_callback':	( lambda **_varargs: ( _varargs.get( 'value', None ), str( _varargs.get( 'value', None ) ).lower( ) )[ _varargs.get( 'value', None ) != None and _varargs.get( 'prefix', '' ) != '' ] ),

		## Add word_ prefix to all words
		'prefixes':			( '', 'word_' ),

		## Words List...
		'map': [
			##
			'self',			'this',

			##
			'Func',			'Groups',		'Keys',			'Names',		'References',

			##
			'Key',			'Get',			'Set',			'Add',			'Pop',		'Raw',			'Is',		'ToString',		'Len',			'Default',		'Value',
			'Property',		'Accessor',		'Accessors',	'Allowed',		'Types',	'Values',		'Base',		'Has',			'Getter',		'Prefix',		'Name',
			'Data',			'Reset',		'Wiki',			'Output',		'By',		'Grouped',		'DB',		'Database',		'Helper',		'Helpers',


			##
			'__str__'
		]
	}

	##
	## Note: I'm leaving this here for now to show what the above Dictionary configured map will generate - previously this had to be made by hand... Now it doesn't need to be...
	## [

	## 	##
	## 	'word_self',			'word_this',

	## 	##
	## 	'word_key',				'word_get',				'word_set',				'word_add',			'word_pop',			'word_raw',			'word_is',			'word_tostring',		'word_len',			'word_default',		'word_value',
	## 	'word_property',		'word_accessor',		'word_accessors',		'word_allowed',		'word_types',		'word_values',		'word_base',		'word_has',				'word_getter',		'word_prefix',		'word_name',
	## 	'word_data',			'word_reset',			'word_wiki',			'word_output',		'word_by',			'word_grouped',		'word_db',			'word_database',		'word_helper',		'word_helpers',

	## 	##
	## 	'word__str__'
	## ]
);



##
## Reverse maps...
##

## Create a map which converts word_* as keys in a Dictionary ids to the actual words as values..
MAP_ACCESSORFUNC_KEYS_TO_WORDS = ENUM_MAP( MAP_ACCESSORFUNC_WORD_KEYS, MAP_ACCESSORFUNC_WORDS );

## Create a map which converts enumeration id from all AccessorFunc code-names ( get, get_raw, set, add, etc.. ) and turns them into the words.
## MAP_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE

## Create helper list of all ENUM ACCESSORFUNC_ID_ENUMS
ENUM_LIST_ACCESSORFUNC_ID_ENUMS = ( ACCESSORFUNC_ID_SETUP_DEFAULTS, ACCESSORFUNC_ID_GET, ACCESSORFUNC_ID_GET_RAW, ACCESSORFUNC_ID_ISSET, ACCESSORFUNC_ID_GET_STR, ACCESSORFUNC_ID_GET_LEN, ACCESSORFUNC_ID_GET_LEN_STR, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, ACCESSORFUNC_ID_GET_PROPERTY, ACCESSORFUNC_ID_GET_ACCESSOR, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, ACCESSORFUNC_ID_GET_GETTER_PREFIX, ACCESSORFUNC_ID_GET_NAME, ACCESSORFUNC_ID_GET_KEY, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, ACCESSORFUNC_ID_GET_DATA_KEY, ACCESSORFUNC_ID_SET, ACCESSORFUNC_ID_ADD, ACCESSORFUNC_ID_POP, ACCESSORFUNC_ID_RESET, ACCESSORFUNC_ID_GET_HELPERS, ACCESSORFUNC_ID_GET_OUTPUT, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, ACCESSORFUNC_ID_INSTANCE, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, ACCESSORFUNC_ID_INSTANCE__STR__ );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to get, get_raw, isset, get_str, get_len, get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to on_get, on_get_raw, on_isset, on_get_str, on_get_len, on_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_ON_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to get, pre_get_raw, pre_isset, pre_get_str, pre_get_len, pre_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_PRE_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to post_get, post_get_raw, post_isset, post_get_str, post_get_len, post_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_POST_IDS );


## These are Instance / Property only 2 keys
## ENUM_LIST_ACESSORFUNC_TYPES, MAP_ACCESSORFUNC_TYPE_NAMES, MAPR_ACCESSORFUNC_TYPE_NAMES

## This is for the Word List... self, this, Key, Get, etc... Words is for Key, Get, Set, etc... R is self -> 0, this => 1, etc.. WORD_KEYS is for word_self, word_this, word_key, word_get, etc.. and R is for word_self => 0, word_this => 1, etc...
## ENUM_LIST_ACCESSOR_FUNC_WORDS, MAP_ACCESSORFUNC_WORDS, MAPR_ACCESSORFUNC_WORDS, MAP_ACCESSORFUNC_WORD_KEYS, MAPR_ACCESSORFUNC_WORD_KEYS

## This one is extensive...|| IDs are setup_defaults, get, set, add, etc... && R is get->1, get_raw->2, etc..  || ON_IDs is the same as previous except on_ prefix  && on_get -> 1  || PRE_IDS uses pre_ prefix  && pre_get -> 1 || POST_IDS uses post_ prefix && post_get -> 1 || FUNC_NAMES uses {get}{name} for <Get><Blah>, {word_is}{name}{word_set} for Is<Blah>Set - these need to be formatted &&  Reverse isn't super helpful unelss you use the PRE formatted data to jump to the id.. || FUNC_ARGS is a string list of the arguments for each function && reverse converts back - but a lot of these are empty so this isn't useful either.... || CALL_INSTANCE needs to be formatted but uses {word_this| or {word_self} to show where this is executed... As most of these are identical, conversion won't be very helpful... I'll need to create a Dict setting to prevent reverse maps from being generated ( although maybe I'll revert it back to not generate using normal, and if I want them then use Dict and set it )
## ENUM_LIST_ACESSORFUNC_IDS, MAP_ACCESSORFUNC_IDS, MAPR_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ON_IDS, MAPR_ACCESSORFUNC_ON_IDS, MAP_ACCESSORFUNC_PRE_IDS, MAPR_ACCESSORFUNC_PRE_IDS, MAP_ACCESSORFUNC_POST_IDS, MAPR_ACCESSORFUNC_POST_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAPR_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAPR_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE, MAPR_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE


##
## AccessorFunc Support
##