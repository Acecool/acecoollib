##
## Acecool Python Library - Acecool Helpers - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.Acecool' );

##
## from ..__global__ import *;
## from . import string



##
## Truncate Example...
##
_col_key	= 20;
_col_eq		= 10;
_col_res	= 50;
_row_eq		= '==';
_text		= 'Testing';

##
_col_a_text = '10  10  10';
_col_b_text = '__|><|';
_col_c_text = 'T  h  i  r  t y';


## ##
## ##
## ##
## ## class AcecoolBase( ):
## ## 	pass;
## class Acecool: #( AcecoolBase	):
## 	##
## 	##
## 	##
## 	__name__ = 'Acecool';


##
Settings = {
	## Should we output AccessorFunc Registry information such as function name, call name with args, alias or original, etc...
	## 'output_accessorfunc_register_info':				True,

	## Should we output all file examples?
	## 'output_acecool_lib_examples':						True,

	##
	## 'output_accessorfunc_register_info_alias':			True,

	##
	## 'output_accessorfunc_register_info_instance':		True,

	##
	## 'output_accessorfunc_register_info_original':		True,

	##
	## '':			True,

	##
	## '':			True,
};


##
## Module Example Information!
##
def __example__( _depth = 1 ):

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';




	return _text;


##
## Task: Create Settings system similar to Sublime, but Stand-Alone and with proper inheritance, etc..
##
def GetSetting( _key, _default = None ):
	## Acecool.GetSetting( 'output_accessorfunc_register_info', False )

	##
	_setting = Settings.get( _key, False );
	if ( _setting ):
		return _setting;

	return _default;