##
## Acecool's Python Library - DictEx Library - Josh 'Acecool' Moser
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t- AcecoolLib.dict' );

##
from .__global__ import *;
from . import debug;


## ##
## ## Dictionary Specific Data-Type Helpers
## ##
## ## class DictBase( ):
## ## 	pass;
## class Dict: #( DictBase ):
## 	##
## 	##
## 	##
## 	__name__ = 'Dict';



##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '\n';
	_text += '	Table of Contents: AcecoolLib.dict\n';
	_text += '\n';

	##


	## _text += Text.DebugHeader( 'Dict.(  ) - ...', '', '', _depth_header );
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
	## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
	## _text += Text.DebugFooter( ) + '\n';


	_text += debug.Header( 'Dict.(  ) - ...', '', '', _depth_header );
	_text += _depth_prefix + '\n';
	_text += _depth_prefix + '\n';
	_text += debug.Footer( ) + '\n';


	## _text += Text.DebugHeader( 'Dict.(  ) - ...', '', '', _depth_header );
	## _text += _depth_prefix + '';
	## _text += _depth_prefix + '';
	## _text += Text.DebugFooter( ) + '\n';

	return _text;


##
## Merges 2 Dicts into 1
##
def Merge( _a, _b ):
	_dict = _a.copy( );
	_dict.update( _b );
	return _dict;


##
## Adds an entry to a Dict
##
def Add( _dict = { }, _key = 'Default', _entry = None ):
	##
	setattr( _dict, _key, _entry );

	##
	return _dict;


##
## Returns a keyed entry / value from a Dict
##
def Get( _dict = { }, _key = 'Default', _default = None ):
	##
	return getattr( _dict, _key, _default );


##
## Returns whether or not a key exists with a valid value from a Dict exists
##
def HasKey( _dict = { }, _key = 'Default' ):
	_has = True if Get( _dict, _key, None ) != None else False;
	return _has;


##
## Returns whether or not an value from a Dict exists
##
def Len( _dict = { } ):
	return len( _dict );


##
## Returns whether or not an value from a Dict exists
##
def Reset( _dict = { } ):
	return Clear( _dict );


##
## Returns whether or not an value from a Dict exists
##
def Clear( _dict = { } ):
	return _dict.clear( );


##
## Removes, and Returns, the last entry added to the dict!
##
def Pop( _dict ):
	return _dict.popitem( );


##
## Removes, and Returns, the entry corresponding to the key within the dict, if not found, the _default value is returned!
##
def GetPop( _dict, _key, _default = None ):
	return _dict.pop( _key, _default );


##
## Inserts an entry to a Dict without a key, uses numerical keys...
##
def Insert( _dict = { }, _entry = None ):
	##
	setattr( _dict, len( _dict ), _entry );

	##
	return _dict;


##
## This simply creates a new Dict from a list of keys, setting the value to what we specify for ALL of them...
##
def FromKeys( _dict = { }, _value = True ):
	return _dict.fromkeys( _dict, _value );


##
## Same as SetupQuickLookupTable, except instead of setting the value to the data-type, it sets it to True...
## Note: This uses the key = True instead of value = True... In a lot of cases, with other lists to convert, we need the value and not the key...
##
def FromValues( _dict = { }, _default_value = None, _use_keys = True ):
	##
	_dict_new = { };

	##
	for _key, _value in _dict.items( ):
		##
		if ( _use_keys ):
			_type_key = str( type( _key ) ) if _default_value == None else _default_value;
			if ( callable( _type_key ) ):
				_type_key = _type_key( _dict, _key, _value );

			_dict_new[ str( _key ) ] = _type_key;
		else:
			_type_val = str( type( _value ) ) if _default_value == None else _default_value;
			if ( callable( _type_val ) ):
				_type_val = _type_val( _dict, _key, _value );

			_dict_new[ str( _value ) ] = _type_val;

	##
	return _dict_new;


##
## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
##
def SetupQuickLookupTable( _data = None ):
	return SetupQuickLookupTableEx( _data, ( lambda _dict, _key, _value: str( type( _key ) ) ) )


##
## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
##
def SetupQuickLookupTableEx( _data = None, _default_value = True, _use_keys = None ):
	## If we have no data, return None...
	if ( _data == None ):
		return None;

	## Helper
	_type = type( _data );

	## If the data isn't None, we need to determine what it is - single element or table... So if not Dict, Tuple, List then we only add 1 object to Dict and return - else we go through each element..
	if ( _type != type( { } ) and _type != type( ( ) ) and _type != type( [ ] ) ):
		return { str( _data ): str( _type ) };

	## Now, we have established it is a Dict, Tuple or List, process all of the values into keys = True...
	_dict = { };
	_iter = None;

	##;
	_is_dict = ( _type == type( { } ) )
	_is_list = ( _type == type( [ ] ) or _type == type( ( ) ) );

	## Data-Table Iteration Method..
	if ( _is_dict ):
		_iter = _data.items( );

	##
	if ( _is_list ):
		_iter = enumerate( _data );

	## For each element, process it...
	for _key, _value in _iter:
		## If List / Tuple, we focus on the value, not the numerical key, as the identifer - ie ( 'x', 'y', 'z' ) would be more useful as x y z instead of 0, 1, 2...
		_value_result = _value;

		## If Dict, the default quick-search method we want won't be the value, it'll be the key...
		if ( _use_keys == True or ( _is_dict and _use_keys == None ) ):
			_value_result = _key;

		_default_value_result = _default_value;
		if ( callable( _default_value ) ):
			_default_value_result = _default_value( _data, _key, _value );

		_dict[ str( _value_result ) ] = _default_value_result;

	## return the Dict..
	return _dict;