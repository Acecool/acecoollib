##
## Acecool's Python Library - Vector Object - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.datatypes.vector' );

##
from ..__global__ import *;

##
from .. import debug;
from .pos import Pos;
from .. import text as Text;
from .. import util;
from ..example import GetExample;

##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Vector );

	return _text;


##
## Vector Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class VectorBase( Pos ):
	pass;
class Vector( VectorBase ):
	##
	__name__					= 'Vector';

	## One method to serialize the data - Note: Here I use this.Get<Name>ToString( ) instead of str( this.<key> to show that you can implement it how you want - you can use the built in helpers or not.... it is up to you.. If you don't want to use them, you can set the key to None in the setup_override panel to remove only specific functions, or override the setup dict and only include the functions you specifically want )
	__serialize					= util.accessor.AccessorFuncBase( parent = VectorBase,	key = 'serialize',		name = 'Serialize',			default = 'Vector( 0.0, 0.0, 0.0 );',		getter_prefix = '',				documentation = 'Serialize Data',												allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: 'Vector( ' + this.GetXToString( ) + ', ' + this.GetYToString( ) + ', ' + this.GetZToString( ) + ' );' ) }				);
	__normalize					= util.accessor.AccessorFuncBase( parent = VectorBase,	key = 'normalize',		name = 'Normalize',			default = None,								getter_prefix = '',				documentation = 'Normalizes the Vector Values and returns them as a copy',		allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: this.CalcNormalize( ) ) }															);
	__normalized				= util.accessor.AccessorFuncBase( parent = VectorBase,	key = 'normalized',		name = 'Normalized',		default = None,								getter_prefix = 'Get',			documentation = 'Returns a Normalized Vector',									allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: Vector( *this.Normalize( ) ) ) }															);

	## Not necessary because of parenting - it means instead of using self.__x/y/z I have to use self.GetX/Y/ZAccessor( ) when I want to access the AccessorFuncBase Object... This adds Vector Functions but still uses the 3 previous locations for storage of information created by Pos2D ( x and y ), and Pos ( 3D which uses x and y from Pos2D and creates z )
	## __x = Pos.__x;
	## __y = Pos.__y;
	## __z = Pos.__z;
	## __x							= AccessorFuncBase( parent = VectorBase,	key = 'x',				name = 'X',					default = 0.0,								getter_prefix = 'Get',			documentation = 'X',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	## __y							= AccessorFuncBase( parent = VectorBase,	key = 'y',				name = 'Y',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Y',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	## __z							= AccessorFuncBase( parent = VectorBase,	key = 'z',				name = 'Z',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Z',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);


	## ##
	## ## Note: This isn't necessary because of Pos parent class...
	## ##
	## def __init__( self, _x = 0.0, _y = 0.0, _z = 0.0 ):
	## 	## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
	## 	self.x = _x;
	## 	self.y = _y;
	## 	self.z = _z;


	##
	## Calculates the magnitude of the vector... A vector is a direction in 3D space, when we calculate the magnitude we can use it to travel in that direction 1 unit at a time instead of n units... This means if the Vector is pointing forward in relation to a player and we SetPlayerPos( GetPlayerPos( ) + NormalizedVector( ) * _units ) we move the player n units forward...
	##
	def CalcNormalize( self ):
		## Create local copies of the variables so we don't change the data...
		_x, _y, _z = self.x, self.y, self.z

		## Calculate the length
		_len = math.sqrt( ( _x * _x ) + ( _y * _y ) + ( _z * _z ) );

		## Now, normalize the values
		_x /= _len;
		_y /= _len;
		_z /= _len;

		## Return those values...
		return _x, _y, _z;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_vector = Vector( );
		_vector.x = -1.0486;
		_vector.y = 0.00134785;
		_vector.z = 0.9768;

		_text = '';
		_text += Text.DebugHeader( 'Data for _vector = Vector( -1.0486, 0.00134785, 0.9768 )', '{comment_note}Pos2DBase -> Pos2D -> PosBase -> Pos -> VectorBase -> Vector{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.Serialize( ): ', 50, str( _vector.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.x: ', 50, str( _vector.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.y: ', 50, str( _vector.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.z: ', 50, str( _vector.z ) ) + '\n';
		_text += '\n';

		## Vector( *_vector.Normalize( ) ).Serialize( )
		_text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.Normalize( ): ', 85, str( _vector.Normalize( ) ) ) + 'This copies the values from the vector and returns the noramlized data.. It doesn\'t change the vector..\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.GetNormalized( ): ', 85, str( _vector.GetNormalized( ) ) ) + '\n\n\t\t\t^ This creates a new Vector with the Normalized data.. It doesn\'t change the vector..\n\t\t\tIt could be added as a Node to this Vector object, and when the Vector changes, using on_set callback, the Vector could be Normalized and saved into the Node - This is what the Versioning system does - when the version is updated, it is unpacked and processed ( removes decimals and treats it as a number but allows buffers to be added ).\n\t\t\tAlternatively, to make it more efficient, the Normalized Node could be populated only when Normalize or GetNormalized is called - then that data is micro-cached and a toggled-control would be enabled ( which would be reset when any Vector element [ x, y, z ] is updated ) so, if enabled then it would use the cached data and if disabled it means the data has changed and the normalized data is processed again and re-stored..\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__serialize: ', 300, str( _vector.__serialize ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__x: ', 300, str( _vector.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__y: ', 300, str( _vector.__y ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__z: ', 300, str( _vector.__z ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetXAccessor( ): ', 250, str( _vector.GetXAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetYAccessor( ): ', 250, str( _vector.GetYAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetZAccessor( ): ', 250, str( _vector.GetZAccessor( ) ) ) + '\n';
		_text += '\n\t\t\tNote: I changed VectorBase Parent to Pos so x, y and z did not need to be duplicated - this means __x, __y, __z are inaccessible but we can still use Get<Name>Accessor( ) for the same result.. It also means I do not need to duplicate __str__ because they all use self.Serialize( )\n';


		return _text + '\n' + Text.DebugFooter( ) + '\n\n';