##
## Acecool's Python Library - Position Object - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.objects.pos' );

##

## Pos2D
from ..__global__ import *;

##
from .. import debug;
from .pos2d import Pos2D;
from .. import text as Text;
from .. import util;
from ..example import GetExample;

##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Pos );

	return _text;


##
## Position 3D Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class PosBase( Pos2D ):
	pass;
class Pos( PosBase ):
	##
	__name__					= 'Pos';

	## Note: This may not work properly because this extends Pos2D - __ prefixed methods can't be overrided by child-classes, so it may not work with other data so I'll need to verify... This is the behavior I want for adding these accessors so the accessors can't be overwritten, but for child-classes I think it wouldn't be a bad idea to allow updated functions...
	__serialize					= util.accessor.AccessorFuncBase( parent = PosBase,		key = 'serialize',		name = 'Serialize',			default = 'Pos( 0.0, 0.0, 0.0 );',			getter_prefix = '',				documentation = 'Serialize Data',				allowed_types = ( TYPE_STRING ),							allowed_values = ( VALUE_ANY ),				setup = { 'get_accessor': True, 'get': ( lambda this: 'Pos( ' + str( this.x ) + ', ' + str( this.y ) + ', ' + str( this.z ) + ' );' ) }							);
	## __serialize					= util.accessor.AccessorFuncBase( parent = PosBase,		key = 'serialize',		name = 'Serialize',			default = 'Pos( 0.0, 0.0, 0.0 );',			getter_prefix = '',				documentation = 'Serialize Data',				allowed_types = ( TYPE_STRING ),							allowed_values = ( VALUE_ANY ),				setup = { 'get_accessor': True, 'get': ( lambda this: 'Pos( ' + this.GetXToString( ) + ', ' + this.GetYToString( ) + ', ' + this.GetZToString( ) + ' );' ) }							);
	__z							= util.accessor.AccessorFuncBase( parent = PosBase,		key = 'z',				name = 'Z',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Z',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY )		);
	## __x = Pos2Base.__x
	## __y = Pos2Base.__y


	##
	##
	##
	def __init__( self, _x = 0.0, _y = 0.0, _z = 0.0 ):
		## Call the super?
		super( ).__init__( _x, _y );

		## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
		## self.x = _x;
		## self.y = _y;
		self.z = _z;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_pos = Pos( );
		_pos.x = 12.2478;
		_pos.y = 4321.12;
		_pos.z = 9867.393;

		_text = '';
		_text += Text.DebugHeader( 'Data for _pos = Pos( 12.2478, 4321.12, 9867.393 )', '{comment_hierarchy}Pos2DBase -> Pos2D -> PosBase -> Pos{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.Serialize( ): ', 50, str( _pos.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.x: ', 50, str( _pos.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.y: ', 50, str( _pos.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.z: ', 50, str( _pos.z ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__serialize: ', 250, str( _pos.__serialize ) ) + '\n';

		## Note: This will also return an error because I created Serialize with a setup table override - meaning the only function which is created is the getter... If I want to allow this, then I need to unlock it... which I did by adding 'get_accessor': True to the setup table... Soon this table will use the enumerators, although I'll probably still allow the text elements... Note: Get prefix isn't use, this affects all helpers - I may change it back to where it only alters the main Get/Set etc.. functions and keep the others so Get<Name>Accessor( ) instead of <Name>Accessor( ), etc... Or I'll change a few things to remov ethe Get prefix for certain systems.
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.Serialize( ): ', 250, str( _pos.SerializeAccessor( ) ) ) + '\n';

		## Note: because __ is effectively 'internal' designation in Python, and since we haven't devided it in Pos - even though we extend to a class that has it - we don't have access to it through these means... I may need to look into a way to reinitialize it when a class is initialized or something... We can still use Get<Name>Accessor( ) which is effectively identical to __<key> and, if you look at the identifier provided by the output, they are identical to x / y for Pos@D with x / y for Pos because those are re-used.... This is intended. But not being able to use __<key> is a drawback I will look into.
		## Task: Look into way to unlock __<key> usage for child-classes which parent a class which makes use of them... Or trigger a way to add the __<key> to the current class without having ro re-add the same line as the previous class...
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _pos.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _pos.__y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.GetXAccessor( ): ', 250, str( _pos.GetXAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.GetYAccessor( ): ', 250, str( _pos.GetYAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _pos.GetXProperty( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _pos.GetYProperty( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__z: ', 250, str( _pos.__z ) ) + '\n';


		return _text + '\n' + Text.DebugFooter( ) + '\n\n';