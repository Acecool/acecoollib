##
## Acecool's Python Library - 2D Position Object - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.datatypes.pos2d' );

##

##
from ..__global__ import *;

##
from .. import debug;
from .. import Acecool
## from .Acecool import *
## from .Acecool import * as Acecool

##
from .. import text as Text;

## AccessorFunc
## from .AccessorFunc import *;
from .. import util;
## import ..util.accessor
from ..example import GetExample;

##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Post2D );

	return _text;


##
## Position 2D Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class Pos2DBase( ):
	pass;
class Pos2D( Pos2DBase ):
	##
	__name__					= 'Pos2D';

	## This is blank - so there is this.Get, this.IsSet, etc... ( getter_prefix must be set otherwise empty function-names may cause an issue before I block them )
	__this_object				= util.accessor.AccessorFuncBase( parent = Pos2DBase,		key = 'this_object',	name = '',				default = 0.0,						getter_prefix = 'Get',		documentation = 'Returns this object octets',	allowed_types = ( TYPE_ANY ),								allowed_values = ( VALUE_ANY ),				setup = {
		'get':		( lambda this: [ this.x, this.y ] ),
		'set':		( lambda this, _x = 0.0, _y = 0.0: this.__init__( _x, _y ) ),
		'get_str': 	( lambda this: 'Pos2D( ' + str( this.x ) + ', ' + str( this.y ) + ' );' )
	} );

	## Serialize turns this object into a human readable format - Note: There will be a serialize function in AccessorFunc in the future where you can easily define separators, prefix, suffix, etc..
	__serialize					= util.accessor.AccessorFuncBase( parent = Pos2DBase,		key = 'serialize',		name = 'Serialize',		default = 'Pos2D( 0.0, 0.0 );',		getter_prefix = '',			documentation = 'Serialize Data',				allowed_types = ( TYPE_STRING ),							allowed_values = ( VALUE_ANY ),				setup = { 'get_accessor': True, 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: this.GetToString( ) ) }															);

	## This is the actual data for this object - a 2D position needs X and Y axis.
	__x							= util.accessor.AccessorFuncBase( parent = Pos2DBase,		key = 'x',				name = 'X',				default = 0.0,						getter_prefix = 'Get',		documentation = 'X',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY ),		setup = { 'get': True, 'get_str': True, 'get_accessor': True, 'set': True }		);
	__y							= util.accessor.AccessorFuncBase( parent = Pos2DBase,		key = 'y',				name = 'Y',				default = 0.0,						getter_prefix = 'Get',		documentation = 'Y',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY ),		setup = { 'get': True, 'get_str': True, 'get_accessor': True, 'set': True }		);


	##
	##
	##
	def __init__( self, _x = 0.0, _y = 0.0 ):
		## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
		self.x = _x;
		self.y = _y;


	##
	##
	##
	def __str__( self ):
		return self.Serialize( );


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_pos = Pos2D( );
		_pos.x = 1.01;
		_pos.y = 2.02345;

		_text = '';
		_text += Text.DebugHeader( 'Data for _pos2d = Pos2D( 1.01, 2.02345 )', '{comment_hierarchy}Pos2DBase -> Pos2D{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.Serialize( ): ', 50, str( _pos.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.x: ', 50, str( _pos.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.y: ', 50, str( _pos.y ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__serialize: ', 250, str( _pos.__serialize ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__x: ', 250, str( _pos.__x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__y: ', 250, str( _pos.__y ) ) + '\n';


		return _text + '\n' + Text.DebugFooter( ) + '\n\n';