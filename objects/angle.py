##
## Acecool's Python Library - Angle Object - Josh 'Acecool' Moser
##
## Note:
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##
##
##
##
## ----------------------------------------------------------------------------------------------------------------------------------------------------------------- ##
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

##
print( '\t\t- AcecoolLib.objects.angle' );

##

## Pos
## from . import Pos;
from ..__global__ import *;

##
from .. import debug;
from .. import text as Text;
from .. import util;
from ..example import GetExample;


##
## Module Example Information!
##
def __example__( _depth = 1 ):
	##
	_depth_header			= _depth;
	_depth					= _depth + 1;
	_depth_prefix			= _depth * '\t';
	_depth_header_prefix	= _depth_header * '\t';

	##
	_text = '';

	## ##
	## _text = '\n';
	## _text += '	Table of Contents:\n';
	## _text += '\n';

	##
	_text += GetExample( Angle );

	return _text;


##
## Angle Base Class - Note: I could parent the same way Vector does and change the getattr magic function to alter x / y / z to p / y / r for pitch, yaw and roll without re-declaring anything...
##
class AngleBase( ):
	pass;
class Angle( AngleBase ):
	##
	__name__					= 'Angle';

	## Note, I'm using self.p / y / r for to string data instead of functions because if I rename the property from x, y, z dynamically without re-declaring then it means I'd either need to rename all of the functions too, or just re-declare, or simply use self.p / y / r instead, everywhere...
	## Task: Add system to rename functions in this regard to allow prevention of adding so much duplicate code...
	__serialize					= util.accessor.AccessorFuncBase( parent = AngleBase,	key = 'serialize',									name = 'Serialize',											default = 'Angle( 0.0, 0.0, 0.0 );',		getter_prefix = '',				documentation = 'Serialize Data',			allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: 'Angle( ' + str( this.p ) + ', ' + str( this.y ) + ', ' + str( this.r ) + ' );' ) }				);

	## Note: I could set up pitch, yaw, roll with Get / Set redirecting to p / y / r.... This would make __pitch, __yaw, and __roll available... But I don't want to override pitch / yaw / roll / _pitch / _yaw / _roll created by these 3 aliases... So I'll likely just need to add the alias system for names too.. Honestly, I should change the defaults to Pitch / Yaw / Roll and add P / Y / R as the aliases..
	__p							= util.accessor.AccessorFuncBase( parent = AngleBase,	key = 'p',				keys = [ 'pitch' ],			name = 'Pitch',				names = [ 'P' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Pitch',					allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	__y							= util.accessor.AccessorFuncBase( parent = AngleBase,	key = 'y',				keys = [ 'yaw' ],			name = 'Yaw',				names = [ 'Y' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Yaw',						allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	__r							= util.accessor.AccessorFuncBase( parent = AngleBase,	key = 'r',				keys = [ 'roll' ],			name = 'Roll',				names = [ 'R' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Roll',						allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);

	## ##
	## ##
	## ##
	## def __getattr__( self, _key, _default = None ):
	## 	if ( _key == 'x' ): _key = 'p';
	## 	if ( _key == 'z' ): _key = 'r';

	## 	##
	## 	return self.__dict__[ _key ];


	##
	## This isn't necessary... As the defaults are already 0.0, using the getter or the property will return that value... This is a convenience function to allow assigning all values at once...
	##
	def __init__( self, _pitch = 0.0, _yaw = 0.0, _roll = 0.0 ):
		## Update all of the properties - Note: I could use self.SetPitch( _pitch ), self._p = _pitch, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetPitch( ... ) is actually called when self.p / self.pitch is reassigned with _pitch...
		self.p = _pitch;
		self.y = _yaw;
		self.r = _roll;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_ang = Angle( );
		_ang.x = 12.2478;
		_ang.y = 4321.12;
		_ang.z = 9867.393;

		_text = '';
		_text += Text.DebugHeader( 'Data for _ang = Angle( 12.2478, 4321.12, 9867.393 )', '{comment_hierarchy}Pos2DBase -> Pos2D -> PosBase -> Pos -> AngleBase -> Angle{depth_comment}{depth_note}This example is interesting because Angle is a class where p / y / r is the primary key, but the aliases pitch / yaw / roll have been added and reference the primary keys meaning you can use self.p = 123.45 or self.pitch = 123.45{depth_comment_tab}When attempting to reassign data for the Primary Key, or using the Primary Setter ( Set<Name> ), the Primary Property Setter assigns the data via the Raw Property Setter, if the data-type / value system doesn\'t block it.\n\t##\tWhen attempting to reassign using an alias, only the Raw Property Setter is used which also has protection against bad data, but it redirects to _<key>..{depth_comment}{depth_note}Each alias key added has 2 properties used - the primary property for self.<alias> and the raw property for self._<alias> whereas with the Primary Key, we only need to set a property to self.<key>{depth_comment}{depth_example}self.p{tab3}=={tab2}Property{depth_example}self.pitch{tab2}=={tab2}Property{depth_comment}{depth_example}self._p{tab3}=={tab2}Value{depth_example}self._pitch{tab2}=={tab2}RawProperty{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.Serialize( ): ', 50, str( _ang.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.p: ', 50, str( _ang.p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.y: ', 50, str( _ang.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.r: ', 50, str( _ang.r ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.pitch: ', 50, str( _ang.pitch ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.yaw: ', 50, str( _ang.yaw ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.roll: ', 50, str( _ang.roll ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__serialize: ', 250, str( _ang.__serialize ) ) + '\n';

		## Note: This will also return an error because I created Serialize with a setup table override - meaning the only function which is created is the getter... If I want to allow this, then I need to unlock it... which I did by adding 'get_accessor': True to the setup table... Soon this table will use the enumerators, although I'll probably still allow the text elements... Note: Get prefix isn't use, this affects all helpers - I may change it back to where it only alters the main Get/Set etc.. functions and keep the others so Get<Name>Accessor( ) instead of <Name>Accessor( ), etc... Or I'll change a few things to remov ethe Get prefix for certain systems.
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.Serialize( ): ', 250, str( _ang.SerializeAccessor( ) ) ) + '\n';

		## Note: because __ is effectively 'internal' designation in Python, and since we haven't devided it in Pos - even though we extend to a class that has it - we don't have access to it through these means... I may need to look into a way to reinitialize it when a class is initialized or something... We can still use Get<Name>Accessor( ) which is effectively identical to __<key> and, if you look at the identifier provided by the output, they are identical to x / y for Pos@D with x / y for Pos because those are re-used.... This is intended. But not being able to use __<key> is a drawback I will look into.
		## Task: Look into way to unlock __<key> usage for child-classes which parent a class which makes use of them... Or trigger a way to add the __<key> to the current class without having ro re-add the same line as the previous class...
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _ang.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _ang.__y ) ) + '\n';
		_ang.roll = 123;
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__p: ', 250, str( _ang.__p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._pitch: ', 250, str( _ang._pitch ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._p: ', 250, str( _ang._p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._yaw: ', 250, str( _ang._yaw ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._y: ', 250, str( _ang._y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._roll: ', 250, str( _ang._roll ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._r: ', 250, str( _ang._r ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.GetYAccessor( ): ', 250, str( _ang.GetYAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__x: ', 300, str( _ang.GetXProperty( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__y: ', 300, str( _ang.GetYProperty( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__r: ', 250, str( _ang.__r ) ) + '\n';


		return _text + '\n' + Text.DebugFooter( ) + '\n\n';